/* Copyright (c) 2011, Martin Kopta <martin@kopta.eu>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#ifndef __general_h_4738928493528934712894_
#define __general_h_4738928493528934712894_

#include <iostream>
#include <string>

using std::cout;
using std::cerr;
using std::endl;
using std::string;

#define EDNAI_VERSION "0.0.0"
#define DEFAULT_MEMORY 1000 /* MB */
#define DEFAULT_TMPDIR "/tmp"
#define DEFAULT_PREFLEN 3
#define DEFAULT_MAXSTRINGSIZE_MULTIPLIER 0.1
#define DEFAULT_MEMFORINBUFFERS_MULTIPLER 0.7
#define DEFAULT_MEMFOROUTTREE 10 /* MB */

#ifndef TIMESTAMP
#define TIMESTAMP __TIMESTAMP__
#endif

#ifdef DEBUG
#define DEBUG 1
#define PRINT_DEBUG(s) cerr << __FILE__ << \
  " [" << __LINE__ << "]: " << s << endl
#else
#define DEBUG 0
#define PRINT_DEBUG(s)
#endif

#define PRINT_ERROR(s) cerr << "ERROR: " << s << endl
#define PRINT_WARNING(s) cerr << "WARNING: " << s << endl

#endif /* __general_h_4738928493528934712894_ */
