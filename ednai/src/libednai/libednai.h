/* Copyright (c) 2011, Martin Kopta <martin@kopta.eu>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#ifndef __libednai_h_324378889426348971239__
#define __libednai_h_324378889426348971239__

#include <string>
#include <iostream>

namespace libednai {

using std::string;
using std::cout;
using std::cerr;
using std::endl;

#ifdef DEBUG
#define DEBUG 1
#define PRINT_DEBUG(s) cerr << __FILE__ << \
  " [" << __LINE__ << "]: " << s << endl
#else
#define DEBUG 0
#define PRINT_DEBUG(s)
#endif

#define PRINT_ERROR(s) cerr << "ERROR: " << s << endl
#define PRINT_WARNING(s) cerr << "WARNING: " << s << endl

#define LIBEDNAI_VERSION "0.0.0"

/* driver facade (front controller); abstract class */
class driver {
  public:
    static int create_index(string inputfile, string outputdir, string
        tmpdir, int memory /* MB */);
};

class dummy_driver: public driver {
  public:
    static int create_index(string inputfile, string outputdir,
        string tmpdir, int memory /* MB */);
};

class trellis_driver: public driver {
  public:
    static int create_index(string inputfile, string outputdir,
        string tmpdir, int memory /* MB */, int preflen);
};

class trellis_plus_driver: public driver {
  public:
    static int create_index(string inputfile, string outputdir,
        string tmpdir, int memory /* MB */);
};

class digest_driver: public driver {
  public:
    static int create_index(string inputfile, string outputdir,
        string tmpdir, int memory /* MB */);
};

class b2st_driver: public driver {
  public:
    static int create_index(string inputfile, string outputdir,
        string tmpdir, int memory /* MB */, int maxstringsize,
        int memforinbuffers /*MB*/, int memforouttree /*MB*/);
};

} /* namespace libednai */

#endif /* __libednai_h_324378889426348971239__ */
