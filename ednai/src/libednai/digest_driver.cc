/* Copyright (c) 2011, Martin Kopta <martin@kopta.eu>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include "./libednai.h"
#include "./digest/digest.h"

namespace libednai {
using namespace libednai_digest;

int digest_driver::create_index(string inputfile, string outputdir,
  string tmpdir, int memory /* MB */) {

  int retval = 0;
  string prefix("digest");
  /* constants taken from original implementation TREE_FROM_FASTA.sh */
  int numerationstart = 0;
  int maxnumberofsequences = 10000;
  int maxlinelength = 1000000; /* FASTA file line lengths */
  int withMapping = 1;
  int memforinputbuffers = 300000000;
  int maxsubtree = memory * 1024; /*kB*/

  cout << "+---------------------------------------+" << endl;
  cout << "| Creating index using DiGeST algorithm |" << endl;
  cout << "+---------------------------------------+" << endl;

  /* FASTA to text */
  cout << "* Phase 1: converting FASTA input to text" << endl;
  retval = fastatotext_main(inputfile.c_str(), outputdir.c_str(),
      tmpdir.c_str(), prefix.c_str(), numerationstart,
      maxnumberofsequences, maxlinelength);
  if (retval) {
    PRINT_ERROR("Phase 1 failed. Exiting.");
    return 1;
  }
  cout << "[Phase 1 done]" << endl;

  /* Text to DNA */ 
  cout << "* Phase 2: converting text to DNA" << endl;
  retval = texttodna_main(outputdir.c_str(), tmpdir.c_str(), prefix.c_str(),
      withMapping);
  if (retval) {
    PRINT_ERROR("Phase 2 failed. Exiting.");
    return 1;
  }
  cout << "[Phase 2 done]" << endl;

  /* encoding */
  cout << "* Phase 3: encoding" << endl;
  retval = encodeFolder_main(outputdir.c_str(), tmpdir.c_str(), prefix.c_str());
  if (retval) {
    PRINT_ERROR("Phase 3 failed. Exiting.");
    return 1;
  }
  cout << "[Phase 3 done]" << endl;

  /* sorting partitions */
  cout << "* Phase 4: sorting partitions" << endl;
  retval = sorting_main(outputdir.c_str(), tmpdir.c_str(),
      prefix.c_str());
  if (retval) {
    PRINT_ERROR("Phase 4 failed. Exiting.");
    return 1;
  }
  cout << "[Phase 4 done]" << endl;

  /* merging */
  cout << "* Phase 5: merging to suffix tree" << endl;
  retval = mergeToSuffixTree_main(outputdir.c_str(), tmpdir.c_str(),
      prefix.c_str(), memforinputbuffers, maxsubtree);
  if (retval) {
    PRINT_ERROR("Phase 5 failed. Exiting.");
    return 1;
  }
  cout << "[Phase 5 done]" << endl;

  return 0;
}

} /* namespace libednai */
