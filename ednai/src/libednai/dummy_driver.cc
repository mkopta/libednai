/* Copyright (c) 2011, Martin Kopta <martin@kopta.eu>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include "./libednai.h"
#include "./dummy/dummy.h"

namespace libednai {
using namespace libednai_dummy;

int dummy_driver::create_index(string inputfile, string outputdir,
  string tmpdir, int memory /* MB */) {
  cout << "+--------------------------------------+" << endl;
  cout << "| Creating index using dummy algorithm |" << endl;
  cout << "+--------------------------------------+" << endl;

  return dummy_index(inputfile.c_str(), outputdir.c_str(), tmpdir.c_str(),
      memory /*MB*/);
}

} /* namespace libednai */
