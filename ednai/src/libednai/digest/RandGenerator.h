#ifndef RANDGENERATOR_H
#define RANDGENERATOR_H

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

namespace libednai_digest {

int getRandomNumber(int sigma);
void getRandomSequence(char *alphabet, int sigma, int N, char *pattern);
unsigned int getSeed( int N);

} /* namespace libednai_digest */

#endif /* RANDGENERATOR_H */
