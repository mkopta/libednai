#include <stdio.h>
#include <stdlib.h>
#include "SuffixTree.h"
#include "RandGenerator.h"
#include "ExactSearch.h"

namespace libednai_digest {

extern int maxOccurences;
extern DividerElement* dividers;
extern unsigned int totalDividers;
extern int found;

int getSequence(char *fileName, int pos, int patternLength, char *buffer)
{
	FILE *inputFile;
	int res;
	
	if(!(inputFile= fopen ( fileName , "rb" )))
	{
		printf("Could not open input DNA file \"%s\" \n", fileName);
		return 1;
	}
	
	fseek(inputFile, pos, SEEK_CUR);
	
	res=fread(buffer,sizeof(char),patternLength,inputFile);
	if(res!=patternLength)
	{
		printf("Error reading input DNA file \"%s\" from position %d \n", fileName,pos);
		
		return 1;
	}
	fclose(inputFile);
	return 0;
}

int testST(int patternLength, char *inputfilePrefix, char *treefileprefix, int minInputLength, 
		   int numberOfInputFiles, int maxTestIterations)
{
	FILE *dividersFile;

	
	
	int j;
	int res;
	
	
	char inputFileName[MAX_PATH_LENGTH];
	char dividersFileName[MAX_PATH_LENGTH];
	char *pattern=(char*) calloc (patternLength, sizeof(char));
	char *encodedDNA=(char*) calloc (2*patternLength, sizeof(char));
	char *buffer=(char*) calloc (patternLength, sizeof(char));
	ResultRecord *occurences=(ResultRecord*) calloc (maxOccurences, sizeof(ResultRecord));
	
	
	
	//2. Load dividers
	
	sprintf(dividersFileName,"%s_dividers",inputfilePrefix);
	
	if(!(dividersFile= fopen ( dividersFileName , "rb" )))
	{
		printf("Could not open dividers DNA file \"%s\" \n", dividersFileName);
		return 1;
	}
	
	//determine number of dividers
	fseek (dividersFile, 0, SEEK_END);
	totalDividers=ftell (dividersFile)/sizeof(DividerElement);
	rewind(dividersFile);
	
	dividers=(DividerElement*) calloc (totalDividers, sizeof(DividerElement));
	
	if(fread (dividers,sizeof(DividerElement),totalDividers,dividersFile)!=totalDividers)
	{
		printf("Error reading tree dividers file \"%s\" \n", dividersFileName);
		return 1;
	}

	fclose(dividersFile);
	
	for(j=0;j<maxTestIterations;j++)
	{
		int randstart=getRandomNumber(minInputLength-patternLength-1); //17
		if(randstart<0)
			randstart=0;
		int randFile=getRandomNumber(numberOfInputFiles); //1
		printf("Looking for pattern starting at position %d in file %d\n",randstart,randFile);
		sprintf(inputFileName,"%s_%d", inputfilePrefix,randFile);
		
		
		res=getSequence(inputFileName,randstart,patternLength,pattern);
		if(res==0)
		{
			int count=0;
			found=0;
			findOccurence(randstart,randFile,treefileprefix,inputfilePrefix,pattern,
					patternLength,occurences,&count,encodedDNA, buffer);
			if(!found)
			{
				printf("XXXXXXXXXXXXXX The %i-th pattern %s not found\n", j, pattern);
				return 1;	
			}
			else
				printf("OK\n");
		}		
	
	}
	
	return 0;
}

int testSuffixTree_main(int argc, char *argv[]) 
{
	char inputfilenameprefix [MAX_PATH_LENGTH];
	char treeprefix [MAX_PATH_LENGTH];
	int patternLength;
	int minInputLength;
	
	int numOfInputfiles;
	int iterations;
	int info[3];
	FILE *infofile;
	char infofilename[MAX_PATH_LENGTH];

	if(argc<5)
	{
		printf("To run: ./searchTest <treefolder> <fileprefix> <patternLength> <maxTestIterations>\n");
		return 1;
	}

	sprintf(inputfilenameprefix,"%s%s", argv[1], argv[2]);
	sprintf(treeprefix,"%s%s_tree", argv[1], argv[2]);

	sprintf(infofilename,"%s%s_input_info", argv[1],argv[2]);
	//1. read info to compute min substript,max subscript and maxfile size
	if(!(infofile= fopen ( infofilename , "rb" )))
	{
		printf("Could not open input info file %s for reading \n",infofilename);
		return 1;
	}	
	
	if(fread(info, sizeof(int), 3, infofile)!=3)
	{
		printf("Error reading input info \n");
		return 1;
	}
	fclose(infofile);

	minInputLength =info[1];	
	patternLength =atoi(argv[3]);		
	maxOccurences=1;
	numOfInputfiles=info[0];
	iterations=atoi(argv[4]);
	
	return testST(patternLength, inputfilenameprefix, treeprefix, minInputLength, numOfInputfiles,iterations);
}

} /* namespace libednai_digest */
