#ifndef BITS_H
#define BITS_H

#include <stdio.h>

namespace libednai_digest {

int getBit(unsigned int *sequence,int pos);
void setBit(unsigned int *sequence,int pos);
void printBitSequence(unsigned int *bitseq);

extern int numBitsInLong;
extern unsigned int masks_array32[];

} /* namespace libednai_digest */

#endif /* BITS_H */
