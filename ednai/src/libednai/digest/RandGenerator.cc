#include "RandGenerator.h"

namespace libednai_digest {

int getRandomNumber(int sigma)
/* returns random number in range of 0 to sigma - not inclusive */
{
   
    int n=rand();   
    return (n%sigma);
}

unsigned int getSeed( int N)
/* returns random number in range of 0 to N - not inclusive */
{
   
    int i=rand()/(int)(((unsigned)RAND_MAX + 1) / N);	
    return (i);
}

void getRandomSequence(char *alphabet, int sigma, int N, char *pattern)
{
	int i=0;
	
	for(;i<N;i++)
	{
		
		pattern[i]=alphabet[getRandomNumber(sigma)];
	}
	pattern[i]='\0';	
}

} /* namespace libednai_digest */
