#include <stdio.h>
#include <stdlib.h>

namespace libednai_digest {

int getNumberOfChunks(char *fileNumbersFileName)
{
	FILE *fileNumbersFile;
	int arraylength;

	if(!(fileNumbersFile= fopen ( fileNumbersFileName , "rb" )))
	{
		printf("Could not open file numbers file \"%s\" \n", fileNumbersFileName);
		exit(1);
	}


	fseek (fileNumbersFile , 0 , SEEK_END);
	arraylength = (ftell (fileNumbersFile))/sizeof(int);
	
	fclose(fileNumbersFile);
	return arraylength;

}

void readPartitionInfo(char *lengthsFileName,char *filenumbersFileName,char *binarylengthsFileName,
					   int *lengths, int *filenumbers,int *binarylengths, int numberofChunks)
{
	
	FILE *lengthsFile;
	FILE *filenumbersFile;
	FILE *binarylengthsFile;
	int readLongs;

	if(!(lengthsFile= fopen ( lengthsFileName , "rb" )))
	{
		printf("Could not open lengths file \"%s\" \n", lengthsFileName);
		exit(1);
	}
	
	readLongs=fread (lengths,sizeof(int),numberofChunks,lengthsFile);

	if (readLongs != numberofChunks) 
	{
		printf ("Reading lengths of partitions - error \n");
		exit (2);
	}
	
	if(!(binarylengthsFile= fopen ( binarylengthsFileName , "rb" )))
	{
		printf("Could not open lengths file \"%s\" \n", binarylengthsFileName);
		exit(1);
	}
	
	readLongs=fread (binarylengths,sizeof(int),numberofChunks,binarylengthsFile);

	if (readLongs != numberofChunks) 
	{
		printf ("Reading binary lengths of partitions - error \n");
		exit (2);
	}	

	if(!(filenumbersFile= fopen ( filenumbersFileName , "rb" )))
	{
		printf("Could not open file numbers file \"%s\" \n", filenumbersFileName);
		exit(3);
	}

	readLongs=fread (filenumbers,sizeof(int),numberofChunks,filenumbersFile);

	if (readLongs != numberofChunks ) 
	{
		printf ("Reading file numbers of partitions error \n");
		exit (4);
	}	

	fclose(lengthsFile);
	fclose(filenumbersFile);
	fclose(binarylengthsFile);
}

} /* namespace libednai_digest */
