#ifndef ENCODING_H
#define ENCODING_H

namespace libednai_digest {

int getLongBinaryEncoding(unsigned int *buffer, int *dnanumeric,int length, int *lengthInLongs,
									 int withterminationchar);

int get1234Encoding(int *buffer,char *dna,int length,int withTerminationChar);

} /* namespace libednai_digest */

#endif /* ENCODING_H */
