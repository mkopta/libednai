namespace libednai_digest {

#define MIN(a, b) ((a)<=(b) ? (a) : (b))
#define MAX(a, b) ((a)>=(b) ? (a) : (b))

#define MAX_PATH_LENGTH 200
#define TAIL_OVERLAP 1000

typedef struct Suffix
{
	int fileNumber;
	int startPos;
	unsigned int bitsPrefix1;
	unsigned int bitsPrefix2;
}Suffix;

typedef struct MergedSuffix
{
	int fileNumber;
	int startPos;	
}MergedSuffix;

void suffixsort(int *x, int *p, int n, int k, int l);
int buildSuffixArrayLarsson(int *buff,int *intBuffer,unsigned int *binaryinputseq,
							 int seq_len,	int binary_seq_length, int output_len,
							 FILE *outputFile, int fileNumber);


typedef struct HeapNode
{
	int startPos;
	unsigned int bitsPrefix1;
	unsigned int bitsPrefix2;
	int fileNumber;	
}HeapNode;

typedef struct STNode
{	
	int startPos;
	int fileNumber;
	int length;  
	int children[2]; 	
	int nextLeaf;	
}STNode;

typedef struct PathElement
{
	STNode *node;
	int length;
}PathElement;

typedef struct DividerElement
{
	unsigned int maxBitSequence;
	unsigned int minBitSequence;
	int fileNumber;
	int length;
}DividerElement;

//in reader.c
void fillInitialBuffers();
void refillBuffer(int bufferNumber);

//in mergeST.c mergeSA.c
void initializeMerge();
int mergeSuffixArrays();
unsigned int getBitsPrefix(int fileNumber,int startPos);

} /* namespace libednai_digest */
