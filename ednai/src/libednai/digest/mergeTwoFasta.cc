#include <stdio.h>
#include <stdlib.h>

namespace libednai_digest {

int mergeTwoFasta(char * fistfilename, char * secondfilename)
{
	FILE *firstfile;
	FILE* secondfile;
	
	if(!(firstfile= fopen ( fistfilename , "ab" )))
	{
		printf("Could not open first DNA file %s for append \n",fistfilename);
		return 1;
	}
	
	fseek (firstfile, 0, SEEK_END);
	long size=ftell (firstfile);
	printf("appending to file %s of length %ld\n",fistfilename,size);
	
	if(!(secondfile= fopen ( secondfilename , "rb" )))
	{
		printf("Could not open second DNA file %s for reading \n",secondfilename);
		return 1;
	}
	
	fseek (secondfile, 0, SEEK_END);
	size=ftell (secondfile);
	rewind(secondfile);
	char *buffer=(char*) calloc (size, sizeof(char));
	long toread=size;
	long result;
	
	result = fread (buffer,sizeof(char),toread,secondfile);
	if(result!=toread)
	{
		printf("reading second file error\n");
		return 1;
	}
	result = fwrite (buffer,sizeof(char),toread,firstfile);
	if(result!=toread)
	{
		printf("writing to first file error\n");
		return 1;
	}
	fclose(firstfile);
	fclose(secondfile);
	return 0;
}

int main(int argc, char *argv[])
{

	if(argc<3)
	{
		printf("To run: preprocessingDNA <firstfilename> <secondfilename>\n");
		return 1;
	}


	char *firstfilename =argv[1];
	char *secondfilename =argv[2];
	//long ChunkSize =atol(argv[3]);
	
	//int currentFileID=atoi(argv[4]);
	
	//char smallNumericFileNamePrefix[MAX_PATH_LENGTH];
	//char smallBinaryFileNamePrefix[MAX_PATH_LENGTH];
	//char * hg="hg";
	//sprintf(smallNumericFileNamePrefix,"%s_smallnumeric", hg);
	//sprintf(smallBinaryFileNamePrefix,"%s_smallbinary", hg);
	

	/*if(preprocess(inputfilename,ChunkSize,currentFileID,
		smallNumericFileNamePrefix,smallBinaryFileNamePrefix))
		return 1;*/
	//writeLengthsForHG();
	//textToDNA("/Data/zebrafish_chromosomes_raw/chr25.txt","/Data/fish_chromosomes/f25");
	//mergeChromosomes(28,"/Data/bird_chromosomes/chiken","/Data/inputsoutputs/chiken_lowcase");
	//chromosomesToACGT("/Data/bird_chromosomes/b", "/Data/bird_chromosomes/chiken", 
	//		1, 28);
	mergeTwoFasta(firstfilename, secondfilename);
//"/Data/inputsoutputs/HG18/HG18.fasta", "/Data/inputsoutputs/HG18/FASTA/chr15.fa");
	
	return 0;
}

} /* namespace libednai_digest */
