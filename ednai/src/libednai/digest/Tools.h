namespace libednai_digest {

#define maxFileName 200
#define maxStringName 10000
#define debug 1
#define MAX_PATH_LENGTH 200

#define MIN(a, b) ((a)<=(b) ? (a) : (b))

typedef struct FileInfo
{
	char fileName[maxFileName];
	int length;
	char stringName[maxStringName];
}FileInfo;

typedef struct FileData
{
	char FileName[MAX_PATH_LENGTH];
	int fileSize;
	int lengthToIndex;
	int startInMergedFile;
	int startInOriginalFile;
}FileData;


int fastaToTextFiles(char *fastaFileName, FileInfo *files, 
		char *outputFilePrefix, int numerationStart,int *stringsCounter);

} /* namespace libednai_digest */
