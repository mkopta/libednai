namespace libednai_digest {

int getNumberOfChunks(char *fileNumbersFileName);
void readPartitionInfo(char *lengthsFileName,char *filenumbersFileName,char *binarylengthsFileName,
					   int *lengths, int *filenumbers,int *binarylengths, int numberofChunks);

} /* namespace libednai_digest */
