/*
 ============================================================================
 Name        : searchExactPattern.c
 Author      : M.Barsky
 Version     :
 Copyright   : Your copyright notice
 Description : Blind Search for exact pattern in suffix tree
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include "SuffixTree.h"
#include "ExactSearch.h"
#include "RandGenerator.h"

namespace libednai_digest {

extern int maxOccurences;
extern DividerElement* dividers;
extern unsigned int totalDividers;

int searchPerformance_main(int argc, char *argv[]) 
{

	int i,j,k;
	char inputfilenameprefix [MAX_PATH_LENGTH];
	char dividersFileName[MAX_PATH_LENGTH];
	char treeprefix [MAX_PATH_LENGTH];
	char *pattern;
	int patternLength;
	ResultRecord *occurences;
	int count;
	char *encodedDNA;
	char *buffer;
	FILE *dividersFile;
	char alphabet[4];
	int iterations;
	
	if(argc<7)
	{
		printf("To run: ./searchPerformance <treefolder> <treefileprefix>  "
				"<patternlength> <maxNumberOfOccurencesToDisplay> <iterations> <seed>\n");
		return 1;
	}
	patternLength =atoi(argv[3]);		
	maxOccurences=atoi(argv[4]);
	iterations=atoi(argv[5]);	
	alphabet[0]='a';
	alphabet[1]='c';
	alphabet[2]='g';
	alphabet[3]='t';
	pattern=(char*) calloc (patternLength+1, sizeof(char));
	
	sprintf(inputfilenameprefix,"%s%s", argv[1], argv[2]);
	sprintf(treeprefix,"%s%s_tree", argv[1], argv[2]);
		
	
	occurences=(ResultRecord*) calloc (maxOccurences, sizeof(ResultRecord));
	encodedDNA=(char*) calloc (2*patternLength, sizeof(char));
	buffer=(char*) calloc (patternLength, sizeof(char));
	unsigned int seed=(unsigned int)(atoi(argv[6]));

  //2. Load dividers
	
	sprintf(dividersFileName,"%s_dividers",inputfilenameprefix);
	
	if(!(dividersFile= fopen ( dividersFileName , "rb" )))
	{
		printf("Could not open dividers DNA file \"%s\" \n", dividersFileName);
		return 1;
	}
	
	//determine number of dividers
	fseek (dividersFile, 0, SEEK_END);
	totalDividers=ftell (dividersFile)/sizeof(DividerElement);
	rewind(dividersFile);
	
	dividers=(DividerElement*) calloc (totalDividers, sizeof(DividerElement));
	
	if(fread (dividers,sizeof(DividerElement),totalDividers,dividersFile)!=totalDividers)
	{
		printf("Error reading tree dividers file \"%s\" \n", dividersFileName);
		return 1;
	}

	fclose(dividersFile);
	
	for (i=0;i<iterations;i++)
	{
		srand(seed);
		count=0;
		for(j=0;j<patternLength;j++)
		{
			k=getRandomNumber(maxOccurences);
			pattern[j]=alphabet[k%4];
		}
		pattern [j]='\0';
		if(findAllOccurences(treeprefix,inputfilenameprefix,pattern,
				patternLength,occurences,&count,encodedDNA,buffer))
				return 1;	
	
		printf("Pattern %s occurs %d times:\n",pattern,count);
	}
	return 0;
}

} /* namespace libednai_digest */
