/* Copyright (c) 2011, Martin Kopta <martin@kopta.eu>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#ifndef __digest_h_89337612437892355263479__
#define __digest_h_89337612437892355263479__

namespace libednai_digest {

int fastatotext_main(const char *inputfile, const char *outputdir, const char
    *tmpdir, const char *prefix, int numerationstart, int maxnumberofsequences,
    int maxlinelength);

int texttodna_main(const char *outputdir, const char *tmpdir, const char
    *prefix, int withMapping);

int encodeFolder_main(const char *outputdir, const char *tmpdir,
    const char *prefix);

int sorting_main(const char *outputdir, const char *tmpdir, const char
    *prefix);

int mergeToSuffixTree_main(const char *outputdir, const char *tmpdir,
    const char *prefix, int memforinputbuffers, int maxsubtree);

} /* namespace libednai_digest */

#endif /* __digest_h_89337612437892355263479__ */
