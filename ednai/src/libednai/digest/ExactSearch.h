namespace libednai_digest {

typedef struct ResultRecord
{
	int startPos;
	int fileNumber;	
}ResultRecord;


int findAllOccurences(char *treeprefix, char *inputfilenameprefix, char *pattern,
		int patternLength, ResultRecord *occurences, int *count, char *pattern01, char *buffer);
void collectAllLeaves(STNode *tree, int parentindex, int *counter, ResultRecord *occurences, 
		int patternLength, int depth );
void findFirstLeaf(STNode *tree, int parentindex, int *fileNumber, int *startPos, 
		int *depth, int patternLength);
int blindSearch(char *pattern01,int patternLength,  STNode *tree, int *depth);
int traverse(STNode *tree, int nodePos, STNode *currNode, char *pattern01, 
		int patternLength, int currPatternPos, int *depth);
unsigned int getBitPrefix(char *pattern01, int patternLength);
int locateTree(DividerElement *dividers,  unsigned int bitPrefix);
char * get01Encoding(char *dnatosearch, int length, char *encodedDNA);
int findOccurence(int startPos, int fileNumber, char *treeprefix, char *inputfilenameprefix, char *pattern,
		int patternLength, ResultRecord *occurences, int *count, char *pattern01, char *buffer);
void findLeaf(int startPos, int fileNumber, STNode *tree, int parentindex, int *counter, ResultRecord *occurences, 
		int patternLength, int depth );
int testST(int patternLength, char *inputfilePrefix, char *treefileprefix, 
		   int minInputLength, int numberOfInputFiles, int maxTestIterations);

} /* namespace libednai_digest */
