#include "Preprocessing.h"

namespace libednai_b2st {

/*This module takes as an input fasta file 
and it outputs a set of plain text files of DNA alphabet for further processing by BBST and DiGeST,
and the input info files

Parameters:
<inputfolder> - folder of the input fasta file, including last delimiter
<inputfilename> - name of the input fasta file 
<tempfolder> - folder where intermediate results will be written, it can be deleted after processing is complete
<outputfileprefix> - prefix of the output and intermediate files
<outputfolder> - folder where the resulting suffix tree will be written
<numerationstart>- start of subscript in the output files names, default=0, in case more than 1 fasta file
<maxnumberofsequences> - max number of sequences in fasta file, default=10000
<maxlinelengthlength> - max length of the line which can be aencountered in fasta file, default=100000
<maxMemoryForInput> - this is the amount of memory (in bytes) dedicated to hold input
**************
  Important notice: since this implementation is intended for a single machine, the ratio of 
  total input size N to the available memory M cannot be large,
  since the execution time is kN, where k=N/M
**************
*/

int preprocessing_main(const char *inputfile, const char *prefix,
    const char *outputdir, const char *tmpdir, int memory /* MB */)
{
	int max_number_of_sequences;
	int max_line;
	int numerationStart;
	PreprocessingState state;
	char inputfilename [MAX_PATH_LENGTH];	
	FILE *outputFile;
	int i,written;
	//int maxMemoryForInputString; //max size of string
	int maxPartitionSize; 
	char *inputBuffer;
	char *outputBuffer;
	int *numericOutput; 
	unsigned int *binaryOutput;

	//1. set parameters
	sprintf(inputfilename,"%s", inputfile);
	sprintf(state.tempfileprefix,"%s/%s", tmpdir, prefix);
	sprintf(state.outputfileprefix,"%s/%s", outputdir, prefix);
	sprintf(state.inputstringsinfofilename,"%s%s", state.outputfileprefix,STRINGS_INFO);
	sprintf(state.infofilename,"%s%s", state.outputfileprefix,INPUT_INFO);
	sprintf(state.partitionsinfofilename,"%s%s", state.outputfileprefix,PARTITIONS_INFO);

	state.inputInfo[0].totalStrings=0;
	state.inputInfo[0].totalPartitions=0;

	state.inputInfo[0].maxLength=0;
	state.inputInfo[0].minLength=0;

	state.inputInfo[0].maxStringsPerPartition=0;


  numerationStart = 0; /* do all of the input */
	
  max_number_of_sequences = 10000; /* FIXME */
  max_line = 1000000; /* FIXME */
	maxPartitionSize = (memory * 1024 * 1024) / 2;
	state.maxStringLength = 100000000; /* FIXME */
	state.inputStrings=(PartitionMember*) calloc (max_number_of_sequences, sizeof(PartitionMember));
	state.partitions=(PartitionInfo*) calloc (max_number_of_sequences, sizeof(PartitionInfo));
 
	printf("Max string length=%d chars and max partition size =%d chars\n",state.maxStringLength,maxPartitionSize);
	printf("Now breaking input fasta file into separate files which can be grouped into partitions of specified size. Please wait ...\n ");
	if(fastaToTextFiles(inputfilename, &state, numerationStart, max_line, maxPartitionSize )) //2*maxMem since each part=mem/2 and compressed 4 times with encoding
		return 1;

	printf("**********Finished converting fasta to separate files:*********\n\n");
	if(debug)
	{		
		printInfo(&(state.inputInfo[0]),state.inputStrings,state.partitions);
	}
	else
	{
		printf("There are total %d input strings and %d partitions \n",state.inputInfo[0].totalStrings,state.inputInfo[0].totalPartitions);
	}

	//2. Remove non-a,c,g,t characters
	printf("Now stripping out the non-DNA characters. Please wait ...\n");
	inputBuffer=(char*) calloc (state.inputInfo[0].maxLength, sizeof(char));
	outputBuffer=(char*) calloc (state.inputInfo[0].maxLength, sizeof(char));

	state.inputInfo[0].maxLength=0;
	for(i=0;i<state.inputInfo[0].totalStrings;i++)
	{	
		if(textToDNA(i,inputBuffer,outputBuffer, &state))
			return 1;	
	}
	
	free(outputBuffer);

	if(debug)
	{
		printf("***********After removing non-DNA symbols:\n");
		printInfo(&(state.inputInfo[0]),state.inputStrings,state.partitions);
	}
	
	printf("Now encoding into a 2-bits per character alphabet, to keep more input in memory\n");
	//3. Crerate binary representation of DNA - 2 bits per char
	//and numeric representation needed for Larsson suffix sorting
	numericOutput=(int*) calloc (state.inputInfo[0].maxLength, sizeof(int));
	if (numericOutput == NULL) 
	{
		printf ("2. Memory error\n"); 
		return 1;
	}

	binaryOutput=(unsigned int*) calloc (state.inputInfo[0].maxLength/4+1, sizeof(unsigned int));
	if (binaryOutput == NULL) 
	{
		printf ("3. Memory error\n"); 
		return 1;
	}

	if(preprocessFolder(inputBuffer,numericOutput, 
					 binaryOutput, &state))
		return 1;
	
	free(inputBuffer);
	free(numericOutput);
	free(binaryOutput);
printf("\n***********FINAL INPUT INFO*********\n");
	if(debug)
	{	
		printInfo(&(state.inputInfo[0]),state.inputStrings,state.partitions);
	}
	else
	{
		printShortInfo(&(state.inputInfo[0]),state.inputStrings,state.partitions);
	}


	//4. write input infos
	outputFile = fopen(state.infofilename, "wb");
	if(outputFile==NULL) 
	{
		printf("Error: can't create file %s for writing input info.\n",state.infofilename);
		return 2;
	}	

	written=fwrite(state.inputInfo, sizeof(InputInfo),1, outputFile);
	if(written!=1)
	{
		printf ("Error writing input info\n"); 
		return 1;
	}
	fclose(outputFile);

	outputFile = fopen(state.inputstringsinfofilename, "wb");
	if(outputFile==NULL) 
	{
		printf("Error: can't create file %s for writing strings info.\n",state.inputstringsinfofilename);
		return 2;
	}	

	written=fwrite(state.inputStrings, sizeof(PartitionMember),state.inputInfo[0].totalStrings, outputFile);
	if(written!=state.inputInfo[0].totalStrings)
	{
		printf ("Error writing substrings info\n"); 
		return 1;
	}
	fclose(outputFile);
	free(state.inputStrings);

	outputFile = fopen(state.partitionsinfofilename, "wb");
	if(outputFile==NULL) 
	{
		printf("Error: can't create file %s for writing strings info.\n",state.partitionsinfofilename);
		return 2;
	}	

	written=fwrite(state.partitions, sizeof(PartitionInfo),state.inputInfo[0].totalPartitions, outputFile);
	if(written!=state.inputInfo[0].totalPartitions)
	{
		printf ("Error writing partitions info\n"); 
		return 1;
	}
	fclose(outputFile);
	free(state.partitions);
	return 0;
	
}

void printInfo(InputInfo *inputInfo, PartitionMember *strings, PartitionInfo *partitions)
{
	int i,j;
	printf("There are total %d input strings and %d partitions \n",inputInfo[0].totalStrings,inputInfo[0].totalPartitions);
	printf("Input strings are of size from %d to %d,\n and max strings per partition is %d\n\n",
		inputInfo[0].minLength,inputInfo[0].maxLength,inputInfo[0].maxStringsPerPartition);
	for(i=0;i<inputInfo[0].totalPartitions;i++)
	{
		printf("Partition %d is of length %d consists of the following %d strings:\n",
			i,partitions[i].partitionLength,partitions[i].numberOfStrings);
		for(j=partitions[i].firstSubscript;
		j<partitions[i].firstSubscript+partitions[i].numberOfStrings;j++)
		{
			PartitionMember *currString=&(strings[j]);
			printf("String: key=%s subscript %d of length %d belongs to partition %d\n start pos in partition %d occupies %d unsigned ints\n\n",
				currString->fasta_key, j, currString->stringLength, currString->partitionNumber,currString->startPosInsidePartition, currString->binaryLength);
		}
		printf("***********\n\n");
	}
}

void printShortInfo(InputInfo *inputInfo, PartitionMember *strings, PartitionInfo *partitions)
{
	int i;
int maxPartition=0, minPartition=0;
	printf("There are total %d input strings and %d partitions \n",inputInfo[0].totalStrings,inputInfo[0].totalPartitions);
	printf("Input strings are of size from %d to %d,\n and max strings per partition is %d\n\n",
		inputInfo[0].minLength,inputInfo[0].maxLength,inputInfo[0].maxStringsPerPartition);
	for(i=0;i<inputInfo[0].totalPartitions;i++)
	{
		if(partitions[i].partitionLength>maxPartition)
			maxPartition=partitions[i].partitionLength;
		if(partitions[i].partitionLength<minPartition || minPartition==0)
			minPartition=partitions[i].partitionLength;			
	}
	printf("Partitions are of size from %d to %d\n",
		minPartition,maxPartition);
	printf("***********\n\n");
}

} /* namespace libednai_b2st */
