/*
 ============================================================================
 Name        : texttodna.c
 Author      : M.Barsky
 Description : Removing from files in the directory characters except a,c,g,t
 (with mapping- texttodna with mapping to actual positions)
 ============================================================================
 */

#include "Preprocessing.h"

namespace libednai_b2st {

int minActualLength;


int convertToDNAAlphabet( char *inputBuffer,int rawLength,
		char *outputBuffer, int *actualLength)
{
	
	int counter=0;
	int i;	
		
	for(i=0;i<rawLength;i++)
	{
		char curr=inputBuffer[i];
		switch (curr)
		{
		case 'a': case 'A':				
				outputBuffer[counter++]='a'; 
							
				break;
		case 'c': case 'C':				
				outputBuffer[counter++]='c'; 
							
				break;
		case 'g': case 'G':				
				outputBuffer[counter++]='g'; 
								
				break;
		case 't': case 'T':
				outputBuffer[counter++]='t';				
				break;			
		default:				
				break;
		}
		
	}	
	*actualLength=counter;
	return 0;
}	


int textToDNA ( int stringNumber, char *inputBuffer, 
			   char *outputBuffer, PreprocessingState *state)
{
	char textFileName[MAX_PATH_LENGTH];
	char DNAFileName[MAX_PATH_LENGTH];
	FILE *outputFile;
	FILE *inputFile;
	
	int result;
	int validLength=0;

	sprintf(textFileName,"%s_%d%s",state->tempfileprefix,stringNumber,TXT_FILE);
	sprintf(DNAFileName,"%s_%d",state->outputfileprefix,stringNumber);

	if(!(outputFile= fopen ( DNAFileName , "wb" )))
	{
		printf("Could not open output DNA file %s  for writing \n",DNAFileName);
		return 1;
	}	
	
	if(!(inputFile= fopen ( textFileName , "rb" )))
	{
		printf("Could not open input text file %s for reading \n",textFileName);
		return 1;
	}
	
	
	result = fread (inputBuffer,sizeof(char),state->inputStrings[stringNumber].stringLength,inputFile);
	if(result!=state->inputStrings[stringNumber].stringLength)
	{
		printf("error reading data from file %s \n",textFileName);
		return 1;
	}	
	
	
	if(convertToDNAAlphabet(inputBuffer,state->inputStrings[stringNumber].stringLength, 
		outputBuffer, &validLength))
		return 1;
	if(validLength<state->inputInfo[0].minLength)
		state->inputInfo[0].minLength=validLength;
	if(validLength>state->inputInfo[0].maxLength)
		state->inputInfo[0].maxLength=validLength;

	result=fwrite(outputBuffer, sizeof(char), validLength, outputFile);	
	if(result!=validLength)
	{
		printf("not all data from file %s was written\n",textFileName);
		return 1;
	}	
	printf("Removed %d non-DNA characters from the %d-th (out of %d) DNA file \n",(state->inputStrings[stringNumber].stringLength-validLength),
 (stringNumber+1),state->inputInfo[0].totalStrings);
	fclose(outputFile);
	fclose(inputFile);	
	
	state->partitions[state->inputStrings[stringNumber].partitionNumber].partitionLength-=(state->inputStrings[stringNumber].stringLength-validLength);
	state->inputStrings[stringNumber].stringLength=validLength;
	if(stringNumber==
		state->partitions[state->inputStrings[stringNumber].partitionNumber].firstSubscript) //first string of partition
	{
		state->inputStrings[stringNumber].startPosInsidePartition=0;
	}
	else
	{
		state->inputStrings[stringNumber].startPosInsidePartition=
			state->inputStrings[stringNumber-1].startPosInsidePartition+state->inputStrings[stringNumber-1].stringLength;
	}
	return 0;
}

} /* namespace libednai_b2st */
