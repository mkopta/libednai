#include "SuffixSorting.h"

namespace libednai_b2st {

int fillInitialBuffers(PairwiseSortingState *state)
{
	int result;
	int i;
	char currentInputFileName[MAX_PATH_LENGTH];
	FILE *suffixArrayFile;
	int first=state->currentFirstPartition;
	int second=state->currentSecondPartition;

	for (i = state->partitions[first].firstSubscript; i < state->partitions[first].firstSubscript+state->partitions[first].numberOfStrings; i++) 
	{
		state->buffersLengths[i]=MIN(state->outputBufferMax,state->strings[i].stringLength);	
	}		

	for (i = state->partitions[second].firstSubscript; i < state->partitions[second].firstSubscript+state->partitions[second].numberOfStrings; i++) 
	{
		state->buffersLengths[i]=MIN(state->outputBufferMax,state->strings[i].stringLength);	
	}	



	//2. Fill buffers from 0 to numberOfChunks	for first and second partitions
	for (i = state->partitions[second].firstSubscript; i < state->partitions[second].firstSubscript+state->partitions[second].numberOfStrings; i++) 
	{
		if(state->buffersLengths[i]>=1)  //CHECK
		{
			sprintf(currentInputFileName,"%s_%d", state->stringSuffixArrayFilePrefix,i);
			if(!(suffixArrayFile=fopen(currentInputFileName,"rb")))
			{
				printf("Cannot open input suffix array File %s for merge\n",currentInputFileName);
				return 1;
			}
	
			result = fread (state->inputSuffixArrays[i],sizeof(Suffix),state->buffersLengths[i],suffixArrayFile);
			if ((int)result != state->buffersLengths[i]) 
			{
				printf ("Reading into buffer %d error\n",i);
				return 1;
			}

			state->buffersPointers[i]=0;
			state->currStartPositionsInSufArrayFile[i]=state->buffersLengths[i];
			if(state->currStartPositionsInSufArrayFile[i]>=state->strings[i].stringLength)
			{
				state->currStartPositionsInSufArrayFile[i]=-1;
			}
			fclose(suffixArrayFile);
		}
		else
		{
			state->currStartPositionsInSufArrayFile[i]=-1;
			state->buffersLengths[i]=-1;			
		}
	}

	for (i = state->partitions[first].firstSubscript; i < state->partitions[first].firstSubscript+state->partitions[first].numberOfStrings; i++) 
	{
		if(state->buffersLengths[i]>=1)  //CHECK
		{
			sprintf(currentInputFileName,"%s_%d", state->stringSuffixArrayFilePrefix,i);
			if(!(suffixArrayFile=fopen(currentInputFileName,"rb")))
			{
				printf("Cannot open input suffix array File %s for merge\n",currentInputFileName);
				return 1;
			}
	
			result = fread (state->inputSuffixArrays[i],sizeof(Suffix),state->buffersLengths[i],suffixArrayFile);
			if ((int)result != state->buffersLengths[i]) 
			{
				printf ("Reading into buffer %d error\n",i);
				return 1;
			}

			state->buffersPointers[i]=0;
			state->currStartPositionsInSufArrayFile[i]=state->buffersLengths[i];
			if(state->currStartPositionsInSufArrayFile[i]>=state->strings[i].stringLength)
			{
				state->currStartPositionsInSufArrayFile[i]=-1;
			}
			fclose(suffixArrayFile);
		}
		else
		{
			state->currStartPositionsInSufArrayFile[i]=-1;
			state->buffersLengths[i]=-1;			
		}
	}
	return 0;
}

void fseekXXL(FILE *file, int pos)
{
	int currPos=pos;
	while(currPos>MAX_MOVE)
	{
		fseek(file,MAX_MOVE*sizeof(Suffix),SEEK_CUR);
		currPos-=MAX_MOVE;
	}
	fseek(file,currPos*sizeof(Suffix),SEEK_CUR);

}

int refillBuffer(PairwiseSortingState *state, int bufferNumber)
{
	int bufferSize,result;
	char currentInputFileName[MAX_PATH_LENGTH];
	FILE *suffixArrayFile;

	if(state->currStartPositionsInSufArrayFile[bufferNumber]==-1 || 
		(state->currStartPositionsInSufArrayFile[bufferNumber])>=state->strings[bufferNumber].stringLength)
	{
		state->buffersLengths[bufferNumber]=-1; //finished this chunk
		state->currStartPositionsInSufArrayFile[bufferNumber]=-1;		
		return 0;
	}

	bufferSize=MIN(state->strings[bufferNumber].stringLength-(state->currStartPositionsInSufArrayFile[bufferNumber]),
		state->inputBufferMax);
	

	state->buffersLengths[bufferNumber]=bufferSize;
	state->buffersPointers[bufferNumber]=0;
	
	sprintf(currentInputFileName,"%s_%d", state->stringSuffixArrayFilePrefix,bufferNumber);
	if(!(suffixArrayFile=fopen(currentInputFileName,"rb")))
	{
		printf("Cannot open input File %s for reading!\n",currentInputFileName);
		return 1;
	}

	fseekXXL(suffixArrayFile,state->currStartPositionsInSufArrayFile[bufferNumber]);
	result=fread (state->inputSuffixArrays[bufferNumber],sizeof(Suffix),bufferSize,suffixArrayFile);
	if(result!=bufferSize)
	{
		printf("error in refilling buffer %d: wanted to read %d and in fact read %d\n",bufferNumber,bufferSize,result);
		printf("Curr start position in suf array file is %d, buffer size is %d, suffix array file length is %d\n",
			state->currStartPositionsInSufArrayFile[bufferNumber],bufferSize,state->strings[bufferNumber].stringLength);
		return 1;
	}

	fclose(suffixArrayFile);

	state->currStartPositionsInSufArrayFile[bufferNumber]+=bufferSize; //move pointer	
	if(	(state->currStartPositionsInSufArrayFile[bufferNumber])>=state->strings[bufferNumber].stringLength)
	{
		state->currStartPositionsInSufArrayFile[bufferNumber]=-1; //no next processing		
	}
	return 0;
}

} /* namespace libednai_b2st */
