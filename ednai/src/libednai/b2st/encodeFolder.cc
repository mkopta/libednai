#include "Preprocessing.h"
#include "Bits.h"

#define MAX_NUMBER_OF_CHUNKS 100
#define TAIL_OVERLAP 1000

namespace libednai_b2st {

int get1234Encoding(int *buffer,char *dna,int length,int withTerminationChar)
{	
	
	int i;
	char curr;

	for(i=0;i<length;i++)
	{
		curr=dna[i];

		switch (curr)
		{
		case 'a': case 'A':
				buffer[i]=1; 				
				break;
		case 'c': case 'C':
				buffer[i]=2;				
				break;
		case 'g': case 'G':
				buffer[i]=3;				
				break;
		case 't': case 'T':
				buffer[i]=4; 				
				break;
			default:
				buffer[i]=1; 				
				break;
		}
		
	}
	if(withTerminationChar)
		buffer[i-1]=0; 

	return 0;
}

int getLongBinaryEncoding(unsigned int *buffer, int *dnanumeric,int length, int *lengthInLongs,
									 int withterminationchar)
{
	int numberofdnacharsin1long=16;
		
	int i,j,m;
	unsigned int *bitsSequence=&buffer[0];
	*bitsSequence=0L;

	for(i=0,j=0,m=0;i<length;i++)
	{
		int currInt=dnanumeric[i];
		if(i>0 && i%numberofdnacharsin1long==0)
		{		
			j++;			
			bitsSequence=&buffer[j];
			*bitsSequence=0L;			
			m=0;
		}		
		
		
		if(currInt==2) //01
		{			
			setBit(bitsSequence,m+1);			
		}
		if(currInt==3) //10
		{			
			setBit(bitsSequence,m);			
		}
		if(currInt==4)  //11
		{
			setBit(bitsSequence,m);
			setBit(bitsSequence,m+1);	
		}
		m=m+2;		
	}
	
	*lengthInLongs=j+1;
	return 0;
}

int preprocessFolder(char *inputbuffer,int *numericOutput, 
					 unsigned int *binaryOutput, PreprocessingState *state)
{
	
	FILE *inputFile;
	int result;
	int lengthInLongs=0;

	FILE *outputFile;
	

	char DNAFileName[MAX_PATH_LENGTH];

	char currnumericfilename [MAX_PATH_LENGTH];
	char currbinaryfilename [MAX_PATH_LENGTH];

	char smallNumericFileNamePrefix[MAX_PATH_LENGTH];
	char smallBinaryFileNamePrefix[MAX_PATH_LENGTH];

	int i;	

	sprintf(smallNumericFileNamePrefix,"%s%s",state->tempfileprefix,NUMERIC_STRING);
	sprintf(smallBinaryFileNamePrefix,"%s%s",state->tempfileprefix,BINARY_STRING);
	

	for(i=0;i<state->inputInfo[0].totalStrings;i++)
	{
		sprintf(DNAFileName,"%s_%i",state->outputfileprefix,i);
		if(!(inputFile= fopen ( DNAFileName , "rb" )))
		{
			printf("Could not open input DNA file \"%s\" \n", DNAFileName);
			return 1;
		}
		
		//printf("reading file %i of length %d\n",i,state->inputStrings[i].stringLength);		

		result=fread(inputbuffer,sizeof(char),state->inputStrings[i].stringLength,inputFile);
		if(result!=state->inputStrings[i].stringLength)
		{
			printf("Error reading input DNA file \"%s\" \n", DNAFileName);
			return 1;
		}
	

		
		state->inputStrings[i].binaryLength=2*state->inputStrings[i].stringLength;		
	
		printf("Encoding into binary alphabet the %i-th file of total size %d\n",(i+1),state->inputStrings[i].stringLength);
		
		sprintf(currnumericfilename,"%s_%i", smallNumericFileNamePrefix,i);
		sprintf(currbinaryfilename,"%s_%i", smallBinaryFileNamePrefix,i);

		outputFile = fopen(currnumericfilename, "wb");
		if(outputFile==NULL) 
		{
			printf("Error: can't create temporary file %s for writing encoded DNA.\n",currnumericfilename);
			return 2;
		}

		if(get1234Encoding(numericOutput,inputbuffer,state->inputStrings[i].stringLength,0))
			return 1;

		result=fwrite(numericOutput, sizeof(int), state->inputStrings[i].stringLength, outputFile);
		if(result!=state->inputStrings[i].stringLength)
		{
			printf("Write error: not all numeric encoding was written.\n");
			return 1;
		}		
		
		fclose(outputFile);
		
		outputFile = fopen(currbinaryfilename, "wb");
		if(outputFile==NULL) 
		{
			printf("Error: can't create temporary file for writing binary encoded DNA.\n");
			return 2;
		}	
			
		if(getLongBinaryEncoding(binaryOutput,numericOutput,state->inputStrings[i].stringLength,
			&lengthInLongs,0))
			return 1;
	
		result=fwrite(binaryOutput, sizeof(unsigned int), lengthInLongs, outputFile);
		if(result!=(lengthInLongs))
		{
			printf("Write error: not all binary encoded DNA was written.\n");
			return 1;
		}
		
		state->inputStrings[i].binaryLength=lengthInLongs;
		fclose(outputFile);
		fclose(inputFile);
	}


	
	return 0;
}

} /* namespace libednai_b2st */
