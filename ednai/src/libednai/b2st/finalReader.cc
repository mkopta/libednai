#include "SuffixTree.h"

namespace libednai_b2st {

int fillInitialBuffersFinalMerge(FinalMergeState *state)
{
	
	int result;
	int i, j;
	char currentInputFileName[MAX_PATH_LENGTH];
	FILE * suffixArrayFile;

	//allocate suffix array buffers
	for (i = 0; i < state->inputInfo[0].totalPartitions; i++) 
	{
		state->buffersLengths[i]=MIN(state->inputBufferMax,state->partitions[i].partitionLength);		
	}		
	

	state->inputSABuffers=(SimpleSuffix **) malloc ( sizeof(SimpleSuffix *)*state->inputInfo[0].totalPartitions);

	if(state->inputSABuffers==NULL)
	{
		printf("Memory error. Cannot allocate memory for input suffix array buffers.\n");
		return 1;
	}

	//1. first read


	//2. Fill buffers from 1 to numberOfChunks	
	for(i=0;i<state->inputInfo[0].totalPartitions; i++) 
	{
		if(state->buffersLengths[i]>0)  //CHECK
		{
			state->inputSABuffers[i]=(SimpleSuffix*) malloc (sizeof(SimpleSuffix)*state->buffersLengths[i]);	
			
			sprintf(currentInputFileName,"%s_%d", state->partitionSuffixArrayFilePrefix,i);
			if(!(suffixArrayFile=fopen(currentInputFileName,"rb")))
			{
				printf("Cannot open input FILE %s for reading\n",currentInputFileName);
				return 1;
			}
	
			result = fread (state->inputSABuffers[i],sizeof(SimpleSuffix),state->buffersLengths[i],
				suffixArrayFile);
			if ((int)result != state->buffersLengths[i]) 
			{
			  printf ("Reading into buffer %d error\n",i);
			  return 1;
			}

			state->buffersPointers[i]=0;
			state->startPosInSAFile[i]=state->buffersLengths[i];
			if(state->startPosInSAFile[i]>=state->partitions[i].partitionLength)
			{
				state->startPosInSAFile[i]=-1;
			}
			fclose(suffixArrayFile);
		}
		else
		{
			state->startPosInSAFile[i]=-1;
			state->buffersLengths[i]=-1;
			
		}
	}

	
	//fill order arrays buffers
	//size of each buffer
	for (i = 0; i < state->inputInfo[0].totalPartitions-1; i++) 
	{
		for(j=i+1;j<state->inputInfo[0].totalPartitions;j++)
			state->orderBuffersLengths[i][j]=MIN(state->inputBufferMax,
			(state->orderArraysLengths[i][j]));			
	}		

	//allocate order array buffers	
	state->inputOrderArrayBuffers=(OrderArrayElement ***) malloc ( sizeof(OrderArrayElement **)*
		state->inputInfo[0].totalPartitions);
	
	for(i=0;i<state->inputInfo[0].totalPartitions;i++)
	{
		state->inputOrderArrayBuffers[i]=(OrderArrayElement **) malloc ( sizeof(OrderArrayElement *)
			*state->inputInfo[0].totalPartitions);
	
		if(state->inputSABuffers[i]==NULL)
		{
			printf("Memory error. Cannot allocate memory for input order array buffers.\n");
			return 1;
		}
	}


	//2. Fill buffers from 0 to numberOfChunks	
	for(i=0;i<state->inputInfo[0].totalPartitions; i++) 
	{
		for(j=i+1;j<state->inputInfo[0].totalPartitions;j++)
		{
			if(state->orderBuffersLengths[i][j]>0)  //CHECK
			{
				state->inputOrderArrayBuffers[i][j]=(OrderArrayElement *) malloc ( sizeof(OrderArrayElement )
					*state->orderBuffersLengths[i][j]);
				
				sprintf(currentInputFileName,"%s_%d_%d", state->orderArrayFilePrefix,i,j);
				if(!(suffixArrayFile=fopen(currentInputFileName,"rb")))
				{
					printf("Cannot open input order array File %s for reading\n",currentInputFileName);
					return 1;
				}
		
				result = fread (state->inputOrderArrayBuffers[i][j],sizeof(OrderArrayElement),
					state->orderBuffersLengths[i][j],	suffixArrayFile);
				
				if ((int)result != state->orderBuffersLengths[i][j]) 
				{
					printf ("Reading into buffer [%d] [%d] error\n",i,j);
					return 1;
				}

				state->orderBuffersPointers[i][j]=0;
				state->startPosInOrderArrayFile[i][j]=state->orderBuffersLengths[i][j];
				if(state->startPosInOrderArrayFile[i][j]>=state->orderArraysLengths[i][j])
				{
					state->startPosInOrderArrayFile[i][j]=-1;
				}
				fclose(suffixArrayFile);
			}
			else
			{
				state->startPosInOrderArrayFile[i][j]=-1;
				state->orderBuffersLengths[i][j]=-1;
				
			}
		}
	}

	return 0;
	
}


int refillBufferFinal(int bufferNumber, FinalMergeState *state)
{
	int bufferSize;
	int result;
	
	char currentInputFileName[MAX_PATH_LENGTH];
	FILE *suffixArrayFile;

	if(state->startPosInSAFile[bufferNumber]==-1 || 
		(state->startPosInSAFile[bufferNumber])>=state->partitions[bufferNumber].partitionLength)
	{
		state->buffersLengths[bufferNumber]=-1; //finished this chunk
		state->startPosInSAFile[bufferNumber]=-1;		
		return 0;
	}

	bufferSize=MIN(state->partitions[bufferNumber].partitionLength-state->startPosInSAFile[bufferNumber],
		state->inputBufferMax);
	
	state->buffersLengths[bufferNumber]=bufferSize;
	state->buffersPointers[bufferNumber]=0;
	
	sprintf(currentInputFileName,"%s_%d", state->partitionSuffixArrayFilePrefix, bufferNumber);
	if(!(suffixArrayFile=fopen(currentInputFileName,"rb")))
	{
		printf("Cannot open input File '%s' for reading\n",currentInputFileName);
		return 1;
	}

	fseekXXLFinal(suffixArrayFile,state->startPosInSAFile[bufferNumber]);
	
	result=fread (state->inputSABuffers[bufferNumber],sizeof(SimpleSuffix),bufferSize,suffixArrayFile);
	if(result!=bufferSize)
	{
		printf("error in refilling buffer %d: wanted to read %d and in fact read %d\n",bufferNumber,bufferSize,result);
		printf("Curr start position in suf array file is %d, buffer size is %d, suffix array file length is %d\n",
			state->startPosInSAFile[bufferNumber],bufferSize,state->partitions[bufferNumber].partitionLength);
		return 1;
	}

	fclose(suffixArrayFile);

	state->startPosInSAFile[bufferNumber]+=bufferSize; //move pointer	
	if(	(state->startPosInSAFile[bufferNumber])>=state->partitions[bufferNumber].partitionLength)
	{
		state->startPosInSAFile[bufferNumber]=-1; //no next processing
		
	}

	return 0;
}

int refillPairwiseBufferFinal(int first, int second, FinalMergeState *state)
{
	int bufferSize,result;
	
	char currentInputFileName[MAX_PATH_LENGTH];
	FILE *orderArrayFile;

	if(state->startPosInOrderArrayFile[first][second]==-1 || 
		(state->startPosInOrderArrayFile[first][second])>=state->orderArraysLengths[first][second])
	{
		state->orderBuffersLengths[first][second]=-1; //finished this order array
		state->startPosInOrderArrayFile[first][second]=-1;		
		return 0;
	}

	bufferSize=MIN(state->orderArraysLengths[first][second]-state->startPosInOrderArrayFile[first][second],
		state->inputBufferMax);
	
	state->orderBuffersLengths[first][second]=bufferSize;
	state->orderBuffersPointers[first][second]=0;
	
	sprintf(currentInputFileName,"%s_%d_%d", state->orderArrayFilePrefix,first,second);
	if(!(orderArrayFile=fopen(currentInputFileName,"rb")))
	{
		printf("Cannot open order array File %s for reading\n",currentInputFileName);
		return 1;
	}

	fseekXXLOrderArray(orderArrayFile,state->startPosInOrderArrayFile[first][second]);
	
	result=fread (state->inputOrderArrayBuffers[first][second],sizeof(OrderArrayElement),bufferSize,
		orderArrayFile);
	if(result!=bufferSize)
	{
		printf("error in refilling order buffer [%d][%d]: wanted to read %d and in fact read %d\n",
			first, second,bufferSize,result);
		printf("Curr start position in order array file is %d, buffer size is %d, order array file length is %d\n",
			state->startPosInOrderArrayFile[first][second],bufferSize,state->orderArraysLengths[first][second]);
		return 1;
	}

	fclose(orderArrayFile);

	state->startPosInOrderArrayFile[first][second]+=bufferSize; //move pointer	
	if(	(state->startPosInOrderArrayFile[first][second])>=state->orderArraysLengths[first][second])
	{
		state->startPosInOrderArrayFile[first][second]=-1; //no next processing
		
	}

	return 0;
}

void fseekXXLOrderArray(FILE *file, int pos)
{
	int currPos=pos;
	while(currPos>MAX_MOVE)
	{
		fseek(file,MAX_MOVE*sizeof(OrderArrayElement),SEEK_CUR);
		currPos-=MAX_MOVE;
	}
	fseek(file,currPos*sizeof(OrderArrayElement),SEEK_CUR);

}
void fseekXXLFinal(FILE *file, int pos)
{
	int currPos=pos;
	while(currPos>MAX_MOVE)
	{
		fseek(file,MAX_MOVE*sizeof(SimpleSuffix),SEEK_CUR);
		currPos-=MAX_MOVE;
	}
	fseek(file,currPos*sizeof(SimpleSuffix),SEEK_CUR);

}

} /* namespace libednai_b2st */
