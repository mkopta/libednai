#include "SuffixSorting.h"
#include "Bits.h"

namespace libednai_b2st {

char currOutputFileName[MAX_PATH_LENGTH];

extern int *totallengthsdoubled;

extern unsigned int masks_array32[32];
extern int numBitsInLong; //is 32 bits, we treat the input sequence as doubled in size
extern int shiftDivision; //is log lengthOfLong - in order to divide by lengthOfLong

int shiftDivision=5; //log 32
extern FILE *debugFile;

void printOutputSABuffer(SimpleSuffix *suffixArray,int size, int partitionID)
{
	int i;
	fprintf(debugFile,"Content of buffer for suffix array for partition %d:\n",partitionID);
	for(i=0;i<size;i++)
	{
		SimpleSuffix *curr=&suffixArray[i];
		fprintf(debugFile,"start=%d file=%d bitprefix=",curr->startPos, curr->fileNumber);
		fprintBitSequence(debugFile, & curr->bitPrefix);
		fprintf(debugFile,"\n");
	}
	fprintf(debugFile,"----------\n");
}

void printOrderBuffer(OrderArrayElement *orderArray,int size, 
			int partition1, int partition2)
{
	int i;
	fprintf(debugFile,"Content of buffer for order array for partitions %d and %d:\n",partition1,partition2);

	for(i=0;i<size;i++)
	{
		OrderArrayElement* curr=&orderArray[i];
		fprintf(debugFile,"sign=%d LCP=%d firstBit=%d\n", curr->sign, curr->LCP, curr->firstBitAfterLCP);
	}
	fprintf(debugFile,"----------\n");
}

unsigned int getBitsPrefix(PairwiseSortingState *state, int fileNumber,int startPos)
{
	//IMPORTANT - startPos in the doubled sequence of 0 and 1
	
	int longPos;
	int tmp;
	short startBitInLong;
	unsigned int bitSequence=0;
	unsigned int first=0;
	unsigned int second=0;
	
	int stringlengthdoubled=state->strings[fileNumber].stringLength<<1;

	if(startPos>=stringlengthdoubled)
	{		
		printf("Invalid position %d in the binary encoded file %d of doubled length %d\n",startPos,fileNumber,stringlengthdoubled);
		exit(1);
	}	
	
	longPos=startPos>>shiftDivision;
	tmp=(longPos<<shiftDivision);
	startBitInLong=(short)(startPos-tmp);

	first=state->inputInBinary[fileNumber][ longPos];

	if(!startBitInLong)
		return first;

	first<<=startBitInLong;

	if(startPos+(numBitsInLong-startBitInLong)>=stringlengthdoubled)
		return first;
	
	bitSequence |=first;

	second=state->inputInBinary[fileNumber][ longPos+1];

	second>>=(numBitsInLong-startBitInLong);
	bitSequence |=second;
	
	return bitSequence;	

}

int getLCP(PairwiseSortingState *state, HeapNode *prev, HeapNode *next,short *firstbitafterlcp)
{
	int b;
	int iteration=1;
	
	unsigned int diff;

	unsigned int firstone=(unsigned int) (1 << (numBitsInLong-1));//number 1000000...
	
	int currLCP=0;

	int start1=(*prev).startPos;
	int start2=(*next).startPos;

	int file1=(*prev).fileNumber;
	int file2=(*next).fileNumber;


	int len1=(state->strings[file1].stringLength<<1)-start1;
	int len2=(state->strings[file2].stringLength<<1)-start2;	
	
	unsigned int bitPrefix1=(*prev).bitsPrefix1;
	unsigned int bitPrefix2=(*next).bitsPrefix1;	
	iteration++;

	diff=(bitPrefix1)^(bitPrefix2);

	while(diff==0)
	{
		if(len1<=numBitsInLong)
		{
			if(len2==len1)
				*firstbitafterlcp=EMPTY_BIT;
			else if(len2>len1)
			{
				if(len1==numBitsInLong)
				{
					start2+=numBitsInLong;
					bitPrefix2=getBitsPrefix(state, file2,start2);
					*firstbitafterlcp=((bitPrefix2 & masks_array32[numBitsInLong-0-1])!=0);
				}
				else
					*firstbitafterlcp=((bitPrefix2 & masks_array32[numBitsInLong-len1-1])!=0);
			}
			else
			{
				printBitSequence(&bitPrefix1);
				printBitSequence(&bitPrefix2);
				printf("LCP computation error 1\n");
				return -1;
			}
			return (currLCP+len1);
		}

		currLCP+=numBitsInLong;

		len1-=numBitsInLong;
		len2-=numBitsInLong;

		start1+=numBitsInLong;
		start2+=numBitsInLong;

		if(iteration==2)
		{
			 bitPrefix1=(*prev).bitsPrefix2;
			 bitPrefix2=(*next).bitsPrefix2;
			 iteration++;
		}
		else
		{
			bitPrefix1=getBitsPrefix(state, file1,start1);
			bitPrefix2=getBitsPrefix(state, file2,start2);
		}

	

		diff=(bitPrefix1)^(bitPrefix2);
	}

//prev or shorter or is smaller - therefore should be 1 in next where 0 in prev
	for(b=0;diff!=0 && b<len1;diff=diff<<1,b++)
	{		
		//if it is still inside this loop, then LCP is in the middle of both bit strings
		if(diff &(firstone))
		{
			*firstbitafterlcp=((bitPrefix2 & masks_array32[numBitsInLong-b-1])!=0);
			return (currLCP+b);
		}			
	}
		
		
	//if it is here, then it exited the for loop since remaininglength1=0
	//b-1 was the last position where they were equal, after b bits, there is an empty string in 
	//the prev suffix, and there is some bit in the next suffix
	if(len1<numBitsInLong)
	{
		if(len2==len1)
			*firstbitafterlcp=EMPTY_BIT;
		else if(len2>len1)
			*firstbitafterlcp=((bitPrefix2 & masks_array32[numBitsInLong-b-1])!=0);
		else
		{
			printBitSequence(&bitPrefix1);
			printBitSequence(&bitPrefix2);
			printf("LCP computation error 2\n");
			return -1;
		}
		return (currLCP+b);
	}
	printf("LCP computation error 3\n");
	return -1;
}


//>0 -		first>second 
//<0 -	first<second
//0 -		equal 
int compare(PairwiseSortingState *state, HeapNode *first, HeapNode *second)
{

	int iteration=1;

	int start1=(*first).startPos; //already doubled
	int start2=(*second).startPos;

	int file1=(*first).fileNumber;
	int file2=(*second).fileNumber;

	
	int len1=(state->strings[file1].stringLength<<1)-start1;
	int len2=(state->strings[file2].stringLength<<1)-start2;

	unsigned int bitPrefix1=(*first).bitsPrefix1;
	unsigned int bitPrefix2=(*second).bitsPrefix1;

	iteration++;
	
//	if(file1==file2)
//		return -1; //since they are sorted in the suffix array of the same file, first is smaller
	
	while(bitPrefix1==bitPrefix2)
	{
		if(len1<=numBitsInLong || len2<=numBitsInLong)
		{
			if(len1>len2)//first is longer
			{ 
			
				return 1;
			}
			if(len1<len2)
				return -1; //second is longer
			return 0;
		}	
		
		len1-=numBitsInLong;
		len2-=numBitsInLong;

		start1+=numBitsInLong;
		start2+=numBitsInLong;

		if(iteration==2)
		{
			bitPrefix1=(*first).bitsPrefix2;
			bitPrefix2=(*second).bitsPrefix2;
			iteration++;
		}
		else
		{
			bitPrefix1=getBitsPrefix(state,file1,start1);
			bitPrefix2=getBitsPrefix(state,file2,start2);
		}
	}

	if(bitPrefix1>bitPrefix2)
	{
	
		return 1;
	}

	if(bitPrefix1<bitPrefix2)
		return -1;	
	
	//absolutely equal suffixes
	return 0;
}



Suffix *getNextStartPos(PairwiseSortingState *state, int bufferNumber)
{
	int bufPointer=state->buffersPointers[bufferNumber];
	if(state->buffersLengths[bufferNumber]<1)
		return NULL;

	if(state->buffersPointers[bufferNumber]<state->buffersLengths[bufferNumber])
	{		
		(state->buffersPointers[bufferNumber])++;
		return &(state->inputSuffixArrays[bufferNumber][bufPointer]);
	}
	else
	{
		refillBuffer(state,bufferNumber);			
		return getNextStartPos(state, bufferNumber);
	}
	return NULL;
}

void addNewSuffix(PairwiseSortingState *state, Suffix *suffix, int chunkID)
{
//IMPORTANT - suffixStart in the doubled sequence of 0 and 1
	HeapNode newNode;
	int resComp,i,j, inserted=0;
	int suffixStart=((*suffix).startPos)<<1;  //multiple of 2^1

	
	if (state->heapCounter == state->currNumberOfStringsToMerge) 
	{
		printf( "ERROR: heap is full\n");
		exit(1);
	}

	newNode.startPos=suffixStart;	
	newNode.fileNumber=chunkID;
	newNode.bitsPrefix1=(*suffix).bitsPrefix1;
	newNode.bitsPrefix2=(*suffix).bitsPrefix2;
  

	for(i=0;i<state->heapCounter && !inserted;i++)
	{
		resComp=compare(state, &(state->heap[i]),&newNode);
		if(resComp>0 || (resComp==0 && state->heap[i].fileNumber>newNode.fileNumber))
		{
			inserted=1;
			for(j=state->heapCounter;j>i;j--)
				state->heap[j]=state->heap[j-1];
			state->heap[i]=newNode;
		}
	}

	if(!inserted)
	{
		state->heap[i]=newNode;
	}

	(state->heapCounter)++;	
}


int initializeMerge(PairwiseSortingState *state)
{	
	int i;
	int first=state->currentFirstPartition;
	int second=state->currentSecondPartition;
	

	//add first element of each buffer -if there is at least one
	for(i=state->partitions[first].firstSubscript;i<state->partitions[first].firstSubscript+
		state->partitions[first].numberOfStrings;i++)
	{
		if(state->buffersLengths[i]>0)
		{
			Suffix *zeroSuffixStart=getNextStartPos(state, i);			
			addNewSuffix(state, zeroSuffixStart, i);
			
		}
	}
	
	for(i=state->partitions[second].firstSubscript;i<state->partitions[second].firstSubscript+
		state->partitions[second].numberOfStrings;i++)
	{
		if(state->buffersLengths[i]>0)
		{
			Suffix *zeroSuffixStart=getNextStartPos(state, i);			
			addNewSuffix(state, zeroSuffixStart, i);
			
		}
	}	
	state->lastTransferred.fileNumber=-1;	
	return 0;
}

HeapNode getSmallestHeapNode(PairwiseSortingState *state)
{
HeapNode result;
	int i;

	if (state->heapCounter == 0) 
	{
		printf( "ERROR: heap is empty\n");
		exit(1);
	}
	result= state->heap[0];   /* to be returned */
	
	(state->heapCounter)--;
	for(i=0;i<state->heapCounter;i++)
		state->heap[i]=state->heap[i+1];	
	return result;
}


//the content of buffers - sa buffer and order array buffer - is added to the end of the 
//corresponding pairwise file
//we need to know if only the first partition is added to the suffix array or both - which happens for 
void restartOutputBuffers(PairwiseSortingState *state) //empty when full
{	
	int written;
	
	if(debug)
	{
		fprintf(debugFile,"Output for pairwise arrays of partition %d and %d:\n",
			state->currentFirstPartition,state->currentSecondPartition);
		printOutputSABuffer(state->outputSuffixArray,state->outputSA1Counter, 
			state->currentFirstPartition);
		printOrderBuffer(state->outputOrderArray,state->outputOrderArrayCounter, 
			state->currentFirstPartition, state->currentSecondPartition);
		if(state->currentFirstPartition==state->inputInfo[0].totalPartitions-2 
			&& state->currentSecondPartition==state->inputInfo[0].totalPartitions-1)
		{
			printOutputSABuffer(state->outputSuffixArray2,state->outputSA2Counter, state->currentSecondPartition);
		}
		fprintf(debugFile,"**********\n\n");
	}
	if(state->writeSA1)
	{
		written=fwrite(state->outputSuffixArray, sizeof (SimpleSuffix), state->outputSA1Counter, state->currSAOutputFileFirst);
		if(written!=state->outputSA1Counter)
		{
			printf("not all output sa entries were written for SA 1\n");
			exit(1);
		}
		state->outputSA1Counter=0;
	}
	


	if(state->writeSA2)
	{
		written=fwrite(state->outputSuffixArray2, sizeof (SimpleSuffix), state->outputSA2Counter, 
			state->currSAOutputFileSecond);
		if(written!=state->outputSA2Counter)
		{
			printf("not all output sa entries were written for SA 2\n");
			exit(1);
		}	
	
		state->outputSA2Counter=0;
	}

	written=fwrite(state->outputOrderArray, sizeof (OrderArrayElement), 
		state->outputOrderArrayCounter, state->currOrderArrayOutputFile);
//printf("written %d SA entries\n",written);
	if(written!=state->outputOrderArrayCounter)
	{
		printf("not all output order array entries were written\n");
		exit(1);
	}	
	
	state->outputOrderArrayCounter=0;	
}


void addSuffixToOutputBuffers(PairwiseSortingState *state, 
							  HeapNode next) //here we transform to the partition start, and partition number instead of file number
{
	char partitionBit;
	short firstbitafterlcp=0;	
	int lcpWithPrevSibling;
	int partitionNumber;
	int suffixStart=(next).startPos;
	int fileNumber=next.fileNumber;

	
	unsigned int bitPrefix=getBitsPrefix(state,fileNumber,suffixStart);
	
	partitionNumber=state->strings[fileNumber].partitionNumber;

	

	
	if(partitionNumber==state->currentFirstPartition)
	{
		if( state->writeSA1)
		{
			state->outputSuffixArray[state->outputSA1Counter].bitPrefix=bitPrefix;
			state->outputSuffixArray[state->outputSA1Counter].startPos=suffixStart;
			state->outputSuffixArray[state->outputSA1Counter].fileNumber=fileNumber;
			(state->outputSA1Counter++);
		
		}
		partitionBit=-1;
	}
	else if(partitionNumber==state->currentSecondPartition)
	{
		if( state->writeSA2)
		{
			state->outputSuffixArray2[state->outputSA2Counter].bitPrefix=bitPrefix;
			state->outputSuffixArray2[state->outputSA2Counter].startPos=suffixStart;
			state->outputSuffixArray2[state->outputSA2Counter].fileNumber=fileNumber;
			(state->outputSA2Counter)++;
		}
		
		partitionBit=1;
	}
	else
	{
		printf("Logic error: suffix does not belong to partitions being processed\n");
		exit(1);
	}

	//Case 0A. first time insertion into an empty suffix array and order array
	if(state->lastTransferred.fileNumber==-1 )
	{	
		
		state->outputOrderArray[state->outputOrderArrayCounter].firstBitAfterLCP=(char)getBit(&bitPrefix,0);
		state->outputOrderArray[state->outputOrderArrayCounter].sign=partitionBit;
		state->outputOrderArray[state->outputOrderArrayCounter].LCP=0;
		(state->outputOrderArrayCounter)++;		
		return;
	}
		
	lcpWithPrevSibling=getLCP(state,&(state->lastTransferred),&next,&firstbitafterlcp);	
	

	state->outputOrderArray[state->outputOrderArrayCounter].firstBitAfterLCP=(char)firstbitafterlcp;
	state->outputOrderArray[state->outputOrderArrayCounter].sign=partitionBit;
	state->outputOrderArray[state->outputOrderArrayCounter].LCP=lcpWithPrevSibling;

	(state->outputOrderArrayCounter)++;
		
}


int  mergeSuffixArrays(PairwiseSortingState *state)
{
	Suffix *nextSuffix;
	printf("Now processing pair of partitions %d and %d. This may take some time so please wait...\n",
		state->currentFirstPartition, state->currentSecondPartition);
	while (state->heapCounter>0 )
	{		
		HeapNode smallest=getSmallestHeapNode(state);
		
		addSuffixToOutputBuffers(state,smallest);

	
		
		nextSuffix=getNextStartPos(state, smallest.fileNumber);
		
		
		if(nextSuffix!=NULL)
		{
			addNewSuffix(state, nextSuffix, smallest.fileNumber);			
		}	
		
		state->lastTransferred=smallest;

		if(state->outputOrderArrayCounter>=state->outputBufferMax )
		{
			restartOutputBuffers(state);			
		}				
		
	}

	
	if(state->outputOrderArrayCounter>0)
	{		
		restartOutputBuffers(state);		
	}
	if(debug)
		fprintf(debugFile,"Created suffix array and order arrays for partitions %d and %d >>>>>>\n\n",
		state->currentFirstPartition, state->currentSecondPartition);
	else
		printf("Created suffix array and order arrays for partitions %d and %d\n\n",
		state->currentFirstPartition, state->currentSecondPartition);

	return 0;
}

} /* namespace libednai_b2st */
