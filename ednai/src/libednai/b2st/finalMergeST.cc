#include "SuffixTree.h"
#include "Bits.h"

namespace libednai_b2st {

extern FILE *debugFile;

STNode *getNewOutputTreeNode(FinalMergeState *state, int *newID)
{
	STNode *ret=&state->outputBuffer[state->outputNodesCounter];	
	(*ret).children[0]=0;
	(*ret).children[1]=0;
	(*ret).nextLeaf=0;
	*newID=state->outputNodesCounter;
	(state->outputNodesCounter)++;
	return ret;
}


int initializeFinalMerge(FinalMergeState *state)
{
	int newID;
	int i;

	state->outputNodesCounter=0;
	state->lastAccessedFirstIndex=-1;
	state->lastAccessedSecondIndex=-1;
	state->dontChangelastAccessed=0;

	state->heapCounter=0;
	state->outputRoot=getNewOutputTreeNode(state,&newID);

	state->lastPath[state->pathCounter].node=state->outputRoot;
	state->lastPath[state->pathCounter].length=0;

	(state->pathCounter)++;

	state->lastTransferred.fileNumber=-1;	
	state->prevLCP=-1;
	//add first element of each sa buffer to the heap -if there is at least one
	for(i=0;i<state->inputInfo[0].totalPartitions;i++)
	{
		if(state->buffersLengths[i]>0)
		{
			SimpleSuffix *zeroSuffixStart=getNextSuffix(state,i);			
			if(addNextSuffixToHeap(state, zeroSuffixStart))
				return 1;			
		}
	}	


	state->outputTreeFileNumberCounter=0;
	return 0;
}

SimpleSuffix *getNextSuffix(FinalMergeState *state,int partitionNumber)
{
	
	if(state->buffersLengths[partitionNumber]<1)
		return NULL;

	if(state->buffersPointers[partitionNumber]<0)
	{
		printf("OOO\n");
		exit(1);
	}
	if(state->buffersPointers[partitionNumber]<state->buffersLengths[partitionNumber])
	{
		(state->buffersPointers[partitionNumber])++;
		return &(state->inputSABuffers[partitionNumber][state->buffersPointers[partitionNumber]-1]);
	}
	else
	{
		refillBufferFinal(partitionNumber, state);			
		return getNextSuffix(state,partitionNumber);
	}
}

int addNextSuffixToHeap(FinalMergeState *state, SimpleSuffix *suffix)
{	
	//IMPORTANT - suffixStart in the doubled sequence of 0 and 1
	SimpleHeapNode newNode;
	int newPartitionID;
	int currPartitionID;
	int i,j;
	int inserted=0;
	
	int suffixStart=((*suffix).startPos);  //multiple of 2^1 ALREADY DOUBLED AFTER INTERMEDIATE MERGE
	


	if (state->heapCounter == state->inputInfo[0].totalPartitions) 
	{
		printf( "ERROR: heap is full\n");
		return 1;
	}

	newNode.startPos=suffixStart;	
	newNode.fileNumber=(*suffix).fileNumber;
	newNode.bitPrefix=(*suffix).bitPrefix;

	newPartitionID=state->strings[newNode.fileNumber].partitionNumber;

	for(i=0;i<state->heapCounter && !inserted;i++)
	{
		currPartitionID=state->strings[state->heap[i].fileNumber].partitionNumber;
		if(compare(currPartitionID,newPartitionID, state)>0)
		{
			//move everything to the end of an array
			for(j=state->heapCounter;j>i;j--)
			{
				state->heap[j]=state->heap[j-1];
			}

			state->heap[i]=newNode;	
			
			(state->heapCounter)++;

			inserted=1;
		}
	}
	if(!inserted) //insert in the last slot
	{
		state->heap[state->heapCounter]=newNode;
		
	
		(state->heapCounter)++;
	}
	return 0;
}

//1 -		first(smaller)>second 
//-1 -	first<second
//0 -		ERROR 
int compare (int firstFile,  int secondFile,  FinalMergeState *state)
{
//	if(firstFile==secondFile)
//	{
//		return -1;
//	}
	if(firstFile<secondFile)
	{
		return state->inputOrderArrayBuffers[firstFile][secondFile][state->orderBuffersPointers[firstFile][secondFile]].sign;
	}

	else  //firstFile>secondFile
	{
		return - state->inputOrderArrayBuffers[secondFile][firstFile][state->orderBuffersPointers[secondFile][firstFile]].sign;
	}

	return 0; //error
}


int  mergeSuffixArraysIntoFinalTree(FinalMergeState *state)
{
	SimpleSuffix *nextSuffix;


	while (state->heapCounter>0 )
	{		
		SimpleHeapNode smallest=getSmallestSimpleHeapNode(state);
		if(debug)
		{
			printf("start=%d, file=%d\n",smallest.startPos, smallest.fileNumber);
		}
	
	
		if(smallest.fileNumber<0)
			return 1;	


		if(addToFinalOutputTree(smallest, state))
			return 1;
		
		nextSuffix=getNextSuffix(state, state->strings[smallest.fileNumber].partitionNumber);
		
	
		
		if(state->lastTransferred.fileNumber==-1)
		{
				if(state->strings[smallest.fileNumber].partitionNumber==0)
				{
					state->lastAccessedFirstIndex=0;
					state->lastAccessedSecondIndex=1;
				}
				else
				{
					state->lastAccessedFirstIndex=0;
					state->lastAccessedSecondIndex=state->strings[smallest.fileNumber].partitionNumber;
				}
		}
		else
		{
			if(state->strings[state->lastTransferred.fileNumber].partitionNumber>state->strings[smallest.fileNumber].partitionNumber)
			{
				state->lastAccessedFirstIndex=state->strings[smallest.fileNumber].partitionNumber;
				state->lastAccessedSecondIndex=state->strings[state->lastTransferred.fileNumber].partitionNumber;
			}
			else if (state->strings[state->lastTransferred.fileNumber].partitionNumber < state->strings[smallest.fileNumber].partitionNumber)
			{
				state->lastAccessedFirstIndex=state->strings[state->lastTransferred.fileNumber].partitionNumber;
				state->lastAccessedSecondIndex=state->strings[smallest.fileNumber].partitionNumber ;		
			}
			else
			{
			}
		}
		state->lastTransferred=smallest;

		if(moveOrderArrayPointers(state, state->strings[smallest.fileNumber].partitionNumber))
			return 1;

		if(nextSuffix!=NULL)
		{
			if(addNextSuffixToHeap(state, nextSuffix))
				return 1;
		}	
		if(state->outputNodesCounter>=(state->outputBufferMax-4))
		{
			
			if(restartFinalOutputTree(state))
				return 1;
			if(((state->outputTreeFileNumberCounter) % 1000)==0)	
				printf("Created %d subtrees\n", state->outputTreeFileNumberCounter);
		}
		
	}

	
	if(state->outputNodesCounter>0)
	{		
		if(restartFinalOutputTree(state))
			return 1;
	}
printf("***********Created TOTAL %d subtrees. END*************\n", state->outputTreeFileNumberCounter);
	return 0;
}
//printf("Writing subtree %d\n", state->outputTreeFileNumberCounter);
int addToFinalOutputTree(SimpleHeapNode next, FinalMergeState *state)
{	
	char firstbitafterlcp;
	int newID;
	STNode *outputnode, *prevLeaf,  *nextLeaf,  *currParent,  *newLeaf, *replacementNode, *existingNextLeaf;
	
//	char firstbitafterlcp=0;
	int newLeafID=0;
	int lcpWithPrevSibling;
	int nextLeafID=0;
	int parentDepth=0;

	
	//int suffixStart=next.startPos;
	int suffixStart=next.startPos; //relative to the entire partition
	int fileNumber=next.fileNumber; //partition number
	int totalStringLength=2*state->strings[fileNumber].stringLength; //tbc bits
	
	
	int prevPartition;
	int currPartition;


	//Case 0A. first time insertion into an empty tree
	if(state->lastTransferred.fileNumber==-1 || state->outputNodesCounter==1) //only root node exists so far
	{
		//initialize divider
		
		state->currentDivider[0].treeFileNameSubscript=state->outputTreeFileNumberCounter;		
		firstbitafterlcp=getBit(&(next.bitPrefix),0);
		
		outputnode=getNewOutputTreeNode(state, &newID);
		(*outputnode).startPos=(next).startPos;
		(*outputnode).fileNumber=fileNumber;
		(*outputnode).length=totalStringLength-(*outputnode).startPos;
		
		state->outputRoot->children[(int)firstbitafterlcp]=newID;
		
		state->lastPath[state->pathCounter].node=outputnode;
		state->lastPath[state->pathCounter].length=(*outputnode).length;

		(state->pathCounter)++;
		state->prevLCP=0;
	
		return 0;
	}

	prevPartition=state->strings[state->lastTransferred.fileNumber].partitionNumber;
	currPartition=state->strings[next.fileNumber].partitionNumber;

	if(prevPartition==currPartition)
	{
		lcpWithPrevSibling=getLCP(state,state->lastAccessedFirstIndex,state->lastAccessedSecondIndex, &firstbitafterlcp);
	}
	else
	{
		lcpWithPrevSibling=getLCP(state, prevPartition,currPartition, &firstbitafterlcp);
	}

	if(lcpWithPrevSibling==-1)
	{
		printf("Invalid LCP when transferring next suffix to the output tree\n");
		return 1;
	}	
	
	if(lcpWithPrevSibling<0)
	{
		printf("Invalid LCP when transferring next suffix to the output tree\n");
		return 1;
	}

	//Case 0B. new child out of root
	if(lcpWithPrevSibling==0 )
	{		
		outputnode=getNewOutputTreeNode(state,&newID);
		(*outputnode).startPos=(next).startPos;
		(*outputnode).fileNumber=fileNumber;
		(*outputnode).length=totalStringLength-(next).startPos;
		if(state->outputRoot->children[(int)firstbitafterlcp]>0)
		{
			printf("Logic error while adding suffix starting at %d in file %d. Child %d of the root node already exists\n",suffixStart, fileNumber,firstbitafterlcp);
			return 1;
		}
		state->outputRoot->children[(int)firstbitafterlcp]=newID;
		
		
		state->lastPath[0].node=state->outputRoot;
		state->lastPath[0].length=0;
		
		state->pathCounter=1;
		state->lastPath[state->pathCounter].node=outputnode;
		state->lastPath[state->pathCounter].length=(*outputnode).length;

		(state->pathCounter)++;
		state->prevLCP=0;
		return 0;
	}


	//if we need to split up on the border path, we first set the correct end of the path
	if(lcpWithPrevSibling<state->lastPath[state->pathCounter-1].length)
		moveUpPath(lcpWithPrevSibling, state);


	//after this operation, we have 2 cases:
	//Case 1. lcp=depth of the node at the end of lastPath

	//sub-case 1. lcp runs till the end of the current string -> create next leaf
	//sub-case 2. lcp runs till the end of the previous suffix and then continues into a child node
	//			-> this child node should not exist, otherwise lcp would be at least 1 bit bigger

	if(lcpWithPrevSibling==state->lastPath[state->pathCounter-1].length)
	{
		//sub-case 1
		if(lcpWithPrevSibling==totalStringLength-suffixStart) 
		{
			//checking correctness
			prevLeaf=state->lastPath[state->pathCounter-1].node;
			if(state->lastPath[state->pathCounter-1].length!=lcpWithPrevSibling)
			{
				printf("Logic error while adding next suffix with LCP equals remaining suffix length\n");
				return 1;
			}			
							
			nextLeaf=getNewOutputTreeNode(state,&nextLeafID);
			(*nextLeaf).fileNumber=fileNumber;
			(*nextLeaf).startPos=suffixStart;
			(*nextLeaf).length=-1;	//till the end of the string of file fileNumber		
			
			if((*prevLeaf).nextLeaf)
			{
				existingNextLeaf=&(state->outputBuffer[(*prevLeaf).nextLeaf]);
				while(existingNextLeaf->nextLeaf)
				{
					existingNextLeaf=&(state->outputBuffer[(*existingNextLeaf).nextLeaf]);
				}
				(*existingNextLeaf).nextLeaf=nextLeafID;
			}
			
			else
				(*prevLeaf).nextLeaf=nextLeafID;
			
			state->prevLCP=lcpWithPrevSibling;

			state->lastPath[state->pathCounter-1].node=prevLeaf; //last path has the same length
			return 0;
		}

		//sub-case 2 new lcp=prev lcp but not till the end of file fileNumber
		currParent=state->lastPath[state->pathCounter-1].node;
		
	//	if(isALeaf(currParent))
	//		(*currParent).emptyNode=1;

		if(state->pathCounter>1)
			parentDepth=state->lastPath[state->pathCounter-2].length;  //to add to the start pos of a new neaf
		else 
			parentDepth=0;
		
		if((*currParent).children[0]==0)
		{
			nextLeaf=getNewOutputTreeNode(state,&nextLeafID);
			(*nextLeaf).fileNumber=(*currParent).fileNumber;
			(*nextLeaf).startPos=(*currParent).startPos-parentDepth;
			(*nextLeaf).length=-1;

			if((*currParent).nextLeaf)
			{
				existingNextLeaf=&(state->outputBuffer[(*currParent).nextLeaf]);
				while(existingNextLeaf->nextLeaf)
				{
					existingNextLeaf=&(state->outputBuffer[(*existingNextLeaf).nextLeaf]);
				}
				(*existingNextLeaf).nextLeaf=nextLeafID;
			}
			else	
				(*currParent).nextLeaf=nextLeafID;
		}

		newLeaf=getNewOutputTreeNode(state,&newLeafID);
		(*newLeaf).startPos=suffixStart+lcpWithPrevSibling;
		(*newLeaf).fileNumber=fileNumber;
		(*newLeaf).length=totalStringLength-(*newLeaf).startPos;

		//additional check for correctness
		if((*currParent).children[(int)firstbitafterlcp]>0)
		{
			printf("Logical error. Trying to add a new child: suffix starting at %d in file %d \n where the child already exists.\n",
				suffixStart,fileNumber);
			return 1;
		} //TBC

		(*currParent).children[(int)firstbitafterlcp]=newLeafID;

		//update path and prevLCP
		state->prevLCP=lcpWithPrevSibling;

		state->lastPath[state->pathCounter].node=newLeaf;
		state->lastPath[state->pathCounter].length=lcpWithPrevSibling+(*newLeaf).length;
		(state->pathCounter)++;

		return 0;
	}

	//Case 2. lcp<depth of the node at the last path - we split this node
	if(lcpWithPrevSibling<state->lastPath[state->pathCounter-1].length) //additional check
	{
		if(lcpWithPrevSibling<=state->lastPath[state->pathCounter-2].length) //additional check should be bigger
		{
			printf("logic error - adding suffix in the middle of an edge, but parent is below the child\n");
			return 1;
		}

		newID=0;
		replacementNode=getNewOutputTreeNode(state, &newID);
		
		//independent if there is leaf or internal node at the end of the path, 
		//it will become the parent of a split
	
		currParent=state->lastPath[state->pathCounter-1].node;  	//copy content of last node in the lastPath to replacementNode
		*replacementNode=*currParent;		

		(*replacementNode).length=(state->lastPath[state->pathCounter-1].length-lcpWithPrevSibling);
		(*replacementNode).startPos=(*currParent).startPos+((*currParent).length-(*replacementNode).length);	

		//we use currParent as the split point on the edge
		//(*currParent).emptyNode=0;
		(*currParent).nextLeaf=0;		
			
		(*currParent).length=(*currParent).length-(*replacementNode).length;		
			
		state->lastPath[state->pathCounter-1].length-=(*replacementNode).length;
			
		(*currParent).children[0]=newID;  
			
		//if we split in the middle of the node, nexttolcp is 1 - child[1]

		//add new leaf
		newLeafID=0;	
		newLeaf=getNewOutputTreeNode(state,&newLeafID);
		(*newLeaf).startPos=suffixStart+lcpWithPrevSibling;
		(*newLeaf).fileNumber=fileNumber;
		(*newLeaf).length=totalStringLength-(*newLeaf).startPos;
		
		if(firstbitafterlcp!=1)
		{
			printf("Logic error - split in the middle of an edge - and the new child is a %d-child\n",firstbitafterlcp);
			return 1;
		} //TBC back
		(*currParent).children[1]=newLeafID;
		
		state->lastPath[state->pathCounter-1].node=currParent;

		state->lastPath[state->pathCounter].node=newLeaf;
		state->lastPath[state->pathCounter].length=lcpWithPrevSibling+(*newLeaf).length;
		(state->pathCounter)++;
	
		state->prevLCP=lcpWithPrevSibling;
		return 0;
	}

	//additional check
	printf("logic error - adding suffix to the tree - not any of the cases???\n");
	return 1; //TBC back
	//return 0;


	
}

SimpleHeapNode getSmallestSimpleHeapNode(FinalMergeState *state)
{
	SimpleHeapNode result;
	int i;
	//int compRes;
result= state->heap[0];   /* to be returned */
	if (state->heapCounter == 0) 
	{
		printf( "ERROR: heap is empty\n");
		result.fileNumber=-1;
		return result;
	}

	

	
//move an array to the beginning

	for(i=1;i<state->heapCounter;i++)
	{
		state->heap[i-1]=state->heap[i];
	}

	(state->heapCounter)--;
	return result;
}

int restartFinalOutputTree(FinalMergeState *state)
{	
	int written;
	int outputRootID=0;
	char currOutputFileName[MAX_PATH_LENGTH];
	FILE *outputFile;

	state->currentDivider[0].totalNodes=state->outputNodesCounter;
	state->currentDivider[0].maxBitSequence=state->lastTransferred.bitPrefix;
	
	if(fwrite(state->currentDivider, sizeof (DividerElement), 1, state->dividersFile)!=1)
	{
		printf("could not write current divider element\n");
		return 1;
	}	
	
	sprintf(currOutputFileName,"%s_%i", state->outputTreePrefix,state->currentDivider[0].treeFileNameSubscript);
	
	//Open output file for writing merged suffixes
	if(!(outputFile=fopen(currOutputFileName,"wb")))
	{
		printf("Cannot open file  %s for writing tree\n",currOutputFileName);
		return 1;
	}

	written=fwrite(state->outputBuffer, sizeof (STNode), state->outputNodesCounter, outputFile);
	if(written!=state->outputNodesCounter)
	{
		printf("not all output tree nodes were written\n");
		return 1;
	}	
	
	fclose(outputFile);		

	(state->outputTreeFileNumberCounter)++;

	
	state->outputNodesCounter=0;
	
	state->outputRoot=getNewOutputTreeNode(state,&outputRootID);	
	state->lastTransferred.fileNumber=-1;
	state->pathCounter=0;

	state->lastPath[state->pathCounter].node=state->outputRoot;
	state->lastPath[state->pathCounter].length=0;
	(state->pathCounter)++;


	return 0;
}

void moveUpPath(int lcp, FinalMergeState *state)
{
	if(state->lastPath[state->pathCounter-1].length>lcp)
	{
		if(state->pathCounter>=2)
		{
			if(state->lastPath[state->pathCounter-2].length<lcp) //in the middle of the edge between last and 1 before last
				return; 
			if(state->lastPath[state->pathCounter-2].length==lcp)
			{
				(state->pathCounter)--;
				return;
			}

			if(state->lastPath[state->pathCounter-2].length>lcp)
			{
				(state->pathCounter)--;
				moveUpPath(lcp, state);
			}
		}
		else
		{
			printf("logic error - moving up in the tree consisting of 1 level\n");
			exit(1);
		}		
	}
}

int getLCP(FinalMergeState *state, int first, int second,char *firstbitafterlcp)
{
	OrderArrayElement *current;	

	if(first==-1 || second==-1)
	{
		printf("Error accessing order array\n");
		exit(1);
	}

	
	if(first<second)
	{
		if(state->orderBuffersLengths[first][second]==-1)
		{
			printf("Trying to access empty run [%d] [%d]\n", first, second);
			return -1;
		}
		current=&(state->inputOrderArrayBuffers[first][second][state->orderBuffersPointers[first][second]]);
		*firstbitafterlcp=current->firstBitAfterLCP;
	/*	if(current->sign<0)
		{
			printf("Invalid entry in order array 1: array[%d][%d]\n", first, second);
			return -1;
		}*/
		return current->LCP;		
	}
	
	if(first>second)
	{
		if(state->orderBuffersLengths[second][first]==-1)
		{
			printf("Trying to access empty run [%d] [%d]\n",  second, first);
			return -1;
		}
		current=&(state->inputOrderArrayBuffers[second][first][state->orderBuffersPointers[second][first]]);
		*firstbitafterlcp=current->firstBitAfterLCP;
		/*if(current->sign>0)
		{
			printf("Invalid entry in order array 2: array[%d][%d] \n",second, first);
			return -1;
		}*/
		return current->LCP;		
	}

	//both suffixes are from the same partition - what to do?
	//we can use the entry from an array which was accessed before

	printf("Invalid partition indexes [%d][%d] for LCP\n",first, second);

	return -1;
}

int moveOrderArrayPointers(FinalMergeState *state, int partitionNumber)
{
	int i;

	//i<partitionNumber
	for(i=0;i<partitionNumber;i++)
	{
		(state->orderBuffersPointers[i][partitionNumber])++;
		
		if(state->orderBuffersPointers[i][partitionNumber]>=state->orderBuffersLengths[i][partitionNumber])
		{
			if(refillPairwiseBufferFinal(i,partitionNumber,state))
				return 1;
		}		
	}

	//partitionNumber<i
	for(i=partitionNumber+1;i<state->inputInfo[0].totalPartitions;i++)
	{
		(state->orderBuffersPointers[partitionNumber][i])++;
		if(state->orderBuffersPointers[partitionNumber][i]>=state->orderBuffersLengths[partitionNumber][i])
		{
			if(refillPairwiseBufferFinal(partitionNumber, i,state))
				return 1;
		}	
	}
	return 0;
}


/*void printNode(STNode *node)
{
	printf("NODE startPos=%d, file=%d, length=%d, child[0]=%d, child[1]=%d,  nextleaf=%d\n",
		node->startPos,node->fileNumber,node->length,node->children[0],node->children[1],node->nextLeaf);
}

void printLastPath(HeapNode *last, int lcpwithnext)
{
	int i;
	unsigned int tbr_bitprefix=getBitsPrefix(last->fileNumber,last->startPos+lcpwithnext-1);
	printf("Last path after insertion of suffix startPos=%d file=%d of length %d\n",
		last->startPos,last->fileNumber,totallengthsdoubled[last->fileNumber]-last->startPos);
	printBitSequence(&tbr_bitprefix);
	for(i=0;i<pathCounter;i++)
	{
		printf("level %d node: \n",i );
		printNode(lastPath[i].node);
		printf(" distance from root is %d\n",lastPath[i].length);
	}
}*/


} /* namespace libednai_b2st */
