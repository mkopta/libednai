#ifndef BITS_H
#define BITS_H

#include <stdio.h>

namespace libednai_b2st {

int getBit(unsigned int *sequence,int pos);
void setBit(unsigned int *sequence,int pos);
void printBitSequence(unsigned int *bitseq);
void fprintBitSequence(FILE *file, unsigned int *bitseq);
extern int numBitsInLong;
extern unsigned int masks_array32[];

} /* namespace libednai_b2st */

#endif /* BITS_H */
