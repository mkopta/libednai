#include "Common.h"

namespace libednai_b2st {

#define MAX_STRING_LENGTH 100000000 //cannot be more, even if partition length is bigger, since otherwise, larsson in mem fails
#define MAX_SEQ_NAME_LENGTH 100
#define debug 0

typedef struct PreprocessingState
{
	InputInfo inputInfo[1];
	int maxStringLength;
	char tempfileprefix [MAX_PATH_LENGTH];
	char outputfileprefix [MAX_PATH_LENGTH];
	char infofilename [MAX_PATH_LENGTH];
	char inputstringsinfofilename [MAX_PATH_LENGTH];
	char partitionsinfofilename [MAX_PATH_LENGTH];
	PartitionInfo *partitions;
	PartitionMember *inputStrings;
}PreprocessingState;

int fastaToTextFiles(char *fastaFileName, PreprocessingState *state, 
					 int numerationStart, int max_line, int maxPartitionSize);

int textToDNA (int stringNumber, char *inputBuffer, 
			   char *outputBuffer, PreprocessingState *state);

int getLongBinaryEncoding(unsigned int *buffer, int *dnanumeric,int length, int *lengthInLongs,
									 int withterminationchar);

int get1234Encoding(int *buffer,char *dna,int length,int withTerminationChar);

int preprocessFolder(char *inputbuffer,int *numericOutput, 
					 unsigned int *binaryOutput, PreprocessingState *state);

void printInfo(InputInfo *inputInfo, PartitionMember *strings, PartitionInfo *partitions);
void printShortInfo(InputInfo *inputInfo, PartitionMember *strings, PartitionInfo *partitions);

} /* namespace libednai_b2st */
