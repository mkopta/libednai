#include "SuffixSorting.h"

namespace libednai_b2st {

/*
This module produces an input for BBST merge:

It takes as an input suffix arrays for each partition, and it creates a suffix array for each partition and 
order array for each partition pair, which will be accessed sequentially during finasl merge

  Input: suffix arrays and the input files of 2 partitions in binary held in RAM
  Output: Suffix arrays of each partition, and order arrays of each partition pair
Parameters:
<outputfilefolder> - folder for the final output trees, where the input info is stored
<tempfilefolder> - folder with the input suffix arrays and used 
for output intermediate partition suffix arrays and order arrays
<inputfileprefix> - the file prefix for all intermediate files
<memforinputbuffers> - the amount of memory (in bytes) which can be dedicated for 
input and output buffers, equals the amount of total memory minus double size of the largest partition 
*/


int pairwise_sorting_main(const char *outputdir, const char *tmpdir,
    const char *prefix, int memforinputbuffers)
{	
	int i,j,s, result;
	char inputFilePrefix [MAX_PATH_LENGTH];	
	char infoFileName [MAX_PATH_LENGTH];
	char stringsFileName [MAX_PATH_LENGTH];
	char partitionsFileName [MAX_PATH_LENGTH];
	char currInputFileName [MAX_PATH_LENGTH];
	FILE *inputFile;
	PairwiseSortingState state;
	int memForInputBuffers;

	
	if(debug)
	{
		debugFile=fopen("PAIRWISE_SORTING_DEBUG.txt","w");
	}
	sprintf(inputFilePrefix,"%s/%s", tmpdir, prefix);	
	sprintf(infoFileName,"%s/%s%s", outputdir, prefix, INPUT_INFO);
	sprintf(stringsFileName,"%s/%s%s", outputdir, prefix, STRINGS_INFO);
	sprintf(partitionsFileName,"%s/%s%s", outputdir, prefix, PARTITIONS_INFO);


	sprintf(state.inputFilePrefixForNumeric,"%s%s", inputFilePrefix, NUMERIC_STRING);
	sprintf(state.inputFilePrefixForBinary,"%s%s", inputFilePrefix, BINARY_STRING);

	sprintf(state.stringSuffixArrayFilePrefix,"%s%s", inputFilePrefix, STRING_SUFFIX_ARRAY);
	
	sprintf(state.partitionSuffixArrayFilePrefix,"%s%s", inputFilePrefix, PARTITION_SUFFIX_ARRAY);
	sprintf(state.orderArrayFilePrefix,"%s%s", inputFilePrefix, ORDER_ARRAY);

	memForInputBuffers = memforinputbuffers;

	//1. read info to compute min substript,max subscript and maxfile size
	if(!(inputFile= fopen ( infoFileName , "rb" )))
	{
		printf("Could not open input info file %s for reading \n",infoFileName);
		return 1;
	}	
	if(fread(state.inputInfo, sizeof(InputInfo), 1, inputFile)!=1)
	{
		printf("Error reading input info file\n");
		return 1;
	}	
	fclose(inputFile);
	
	state.strings=(PartitionMember*) calloc (state.inputInfo[0].totalStrings, sizeof(PartitionMember)); 
	state.partitions=(PartitionInfo*) calloc (state.inputInfo[0].totalPartitions, sizeof(PartitionInfo)); 
	
	if(!(inputFile= fopen ( stringsFileName , "rb" )))
	{
		printf("Could not open string info file %s for reading \n",stringsFileName);
		return 1;
	}	
	if((int)fread(state.strings, sizeof(PartitionMember), state.inputInfo[0].totalStrings, inputFile)!=
		state.inputInfo[0].totalStrings)
	{
		printf("Error reading string info file\n");
		return 1;
	}	
	fclose(inputFile);
	
	if(!(inputFile= fopen ( partitionsFileName , "rb" )))
	{
		printf("Could not open partitions info file %s for reading \n",partitionsFileName);
		return 1;
	}	
	if((int)fread(state.partitions, sizeof(PartitionInfo), state.inputInfo[0].totalPartitions, inputFile)!=
		state.inputInfo[0].totalPartitions)
	{
		printf("Error reading partitions info file\n");
		return 1;
	}	
	fclose(inputFile);





	//4. Prepare for pairwise partition sorting
	state.outputBufferMax=(memForInputBuffers/sizeof(SimpleSuffix))/30;
/*	if((state.outputBufferMax*sizeof(SimpleSuffix))<1000) //less than 1 MB per output buffer
	{
		printf("The calculated size of the output buffer (%d elements) is too small. \n",state.outputBufferMax);
		printf("Please increase memory budget for this dataset, and try again\n");
		return 1;
	}
	
*/
	memForInputBuffers=memForInputBuffers - 3*state.outputBufferMax*sizeof(SimpleSuffix);
	state.inputBufferMax=(memForInputBuffers/(2*state.inputInfo[0].maxStringsPerPartition))/sizeof(Suffix);
/*	if((state.inputBufferMax*sizeof(Suffix))<1000) //less than 1 MB per each input buffer buffer
	{
		printf("The calculated size of the input buffer (%d elements) is too small.",state.inputBufferMax);
		printf(" There are max %d total input buffers \n",2*state.inputInfo[0].maxStringsPerPartition);
		printf("Please increase memory budget for this dataset, and try again\n");
		return 1;
	}*/
	printf("\n************Started paiwise merge (Digest algorithm).****\n");
	printf("Input buffer per string =%lu bytes, output buffer = %lu bytes, max number of input buffers=%d\n",
		state.inputBufferMax*sizeof(SimpleSuffix),state.outputBufferMax*sizeof(Suffix)*3,(2*state.inputInfo[0].maxStringsPerPartition));

	//allocate output buffers
	state.outputSuffixArray=(SimpleSuffix*) calloc (state.outputBufferMax, sizeof(SimpleSuffix));
	state.outputSuffixArray2=(SimpleSuffix*) calloc (state.outputBufferMax, sizeof(SimpleSuffix));
	state.outputOrderArray=(OrderArrayElement*) calloc (state.outputBufferMax, sizeof(OrderArrayElement)); 

	//allocate input buffers array
	state.inputSuffixArrays=(Suffix**) calloc (state.inputInfo[0].totalStrings, sizeof(Suffix*));
	
	//allocate pointers to be used for all merged buffers
	state.currStartPositionsInSufArrayFile=(int*) calloc (state.inputInfo[0].totalStrings, sizeof(int));//for the next read, -1 if no more reads
	state.buffersLengths=(int*) calloc (state.inputInfo[0].totalStrings, sizeof(int));  //the current lengths of the in-memory buffers
	state.buffersPointers=(int*) calloc (state.inputInfo[0].totalStrings, sizeof(int)); //from 0 to bufferslengths[i]

	
	state.inputInBinary=(unsigned int **)calloc (state.inputInfo[0].totalStrings,sizeof(unsigned int*));

	for(i=0; i<state.inputInfo[0].totalPartitions-1;i++)
	{
		//1. read corresponding input strings for partition i, and allocate input buffers
		for(s=state.partitions[i].firstSubscript;
				s<state.partitions[i].firstSubscript+state.partitions[i].numberOfStrings;s++)
		{
			//allocate
			state.inputInBinary[s]=(unsigned int *)calloc (state.strings[s].binaryLength,sizeof(unsigned int));
			//open file
			sprintf(currInputFileName,"%s_%i", state.inputFilePrefixForBinary,s);

			if(!(inputFile=fopen(currInputFileName,"rb")))
			{
				printf("Cannot open input string File %s for reading\n",currInputFileName);
				return 1;
			}
		
			result=fread(state.inputInBinary[s],sizeof(unsigned int),
				state.strings[s].binaryLength,inputFile);
		
			if(result!=state.strings[s].binaryLength)
			{
				printf("binary string %i reading error\n",s);
				return 1;
			}
			fclose(inputFile);

			state.inputSuffixArrays[s]=(Suffix*) calloc (state.inputBufferMax, sizeof(Suffix));

		}
		for(j=i+1;j<state.inputInfo[0].totalPartitions;j++)
		{
			//2. read corresponding input strings for partition j
			for(s=state.partitions[j].firstSubscript;
				s<state.partitions[j].firstSubscript+state.partitions[j].numberOfStrings;s++)
			{
				//allocate
				state.inputInBinary[s]=(unsigned int *)calloc (state.strings[s].binaryLength,sizeof(unsigned int));
				//open file
				sprintf(currInputFileName,"%s_%i", state.inputFilePrefixForBinary,s);

				if(!(inputFile=fopen(currInputFileName,"rb")))
				{
					printf("Cannot open input string File %s for reading\n",currInputFileName);
					return 1;
				}
			
				result=fread(state.inputInBinary[s],sizeof(unsigned int),
					state.strings[s].binaryLength,inputFile);
			
				if(result!=state.strings[s].binaryLength)
				{
					printf("binary string %i reading error\n",s);
					return 1;
				}
				fclose(inputFile);
				state.inputSuffixArrays[s]=(Suffix*) calloc (state.inputBufferMax, sizeof(Suffix));
			}
			
			state.currentFirstPartition=i;
			state.currentSecondPartition=j;

			state.writeSA1=0;
			state.writeSA2=0;

			if(i==j-1)
			{
				state.writeSA1=1;
				if(j==state.inputInfo[0].totalPartitions-1)
					state.writeSA2=1;
			}
			if(fillInitialBuffers(&state))
				return 1;

			//allocate particular heap
			state.currNumberOfStringsToMerge=state.partitions[i].numberOfStrings+state.partitions[j].numberOfStrings;
			state.heap=(HeapNode*) calloc (state.currNumberOfStringsToMerge, sizeof(HeapNode)); 
			state.outputSA1Counter=0;
			state.outputSA2Counter=0;
			state.outputOrderArrayCounter=0;
			state.heapCounter=0;


			//set corresponding output files pointers
      printf("############################# X001\n");
			if(state.writeSA1)
			{
        printf("############################# X002\n");
				sprintf(currInputFileName,"%s_%i", state.partitionSuffixArrayFilePrefix,i);
				if(!(state.currSAOutputFileFirst=fopen(currInputFileName,"wb")))
				{
					printf("Cannot open output suffix array File %s for partition %d\n",currInputFileName,i);
					return 1;
				}
			}

      printf("############################# X003\n");
			if(state.writeSA2)
			{
        printf("############################# X004\n");
				sprintf(currInputFileName,"%s_%i", state.partitionSuffixArrayFilePrefix,j);
				if(!(state.currSAOutputFileSecond=fopen(currInputFileName,"wb")))
				{
					printf("Cannot open output suffix array File %s for partition %d\n",currInputFileName,j);
					return 1;
				}
			}

			sprintf(currInputFileName,"%s_%i_%i", state.orderArrayFilePrefix,i,j);
			if(!(state.currOrderArrayOutputFile=fopen(currInputFileName,"wb")))
			{
				printf("Cannot open output order array File %s for partitions %d %d\n",currInputFileName,i,j);
				return 1;
			}

			if(initializeMerge(&state))
				return 1;

			//7. perform merge
			if(mergeSuffixArrays(&state))
				return 1;

			//free input string and input array for partition j	
			for(s=state.partitions[j].firstSubscript;
				s<state.partitions[j].firstSubscript+state.partitions[j].numberOfStrings;s++)
			{
				free(state.inputInBinary[s]);
				free(state.inputSuffixArrays[s]);
			}
			free(state.heap);
			if(state.writeSA1)
				fclose(state.currSAOutputFileFirst);
			if(state.writeSA2)
				fclose(state.currSAOutputFileSecond);
			fclose(state.currOrderArrayOutputFile);

			
		}
		//free input string and input array for partition i
		for(s=state.partitions[i].firstSubscript;
				s<state.partitions[i].firstSubscript+state.partitions[i].numberOfStrings;s++)
		{	
			free(state.inputInBinary[s]);
			free(state.inputSuffixArrays[s]);
		}
				
	}
	free(state.inputInBinary);
	
	return 0;
}

	
} /* namespace libednai_b2st */
