#include "Common.h"

namespace libednai_b2st {

#define debug 0

#define SUFFIX_COLLECTOR_SIZE 7680000   //7500*1024
#define EMPTY_BIT 2

typedef struct Suffix
{
	int fileNumber;
	int startPos;
	unsigned int bitsPrefix1;
	unsigned int bitsPrefix2;
}Suffix;

typedef struct HeapNode
{
	int startPos;
	unsigned int bitsPrefix1;
	unsigned int bitsPrefix2;
	int fileNumber;	
}HeapNode;

typedef struct PairwiseSortingState
{
	InputInfo inputInfo[1];
	PartitionMember *strings;
	PartitionInfo *partitions;
	char inputFilePrefixForNumeric[MAX_PATH_LENGTH];
	char inputFilePrefixForBinary[MAX_PATH_LENGTH];
	char stringSuffixArrayFilePrefix [MAX_PATH_LENGTH]; //intermediate output into temp folder
	int *inputNumericBuffer;
	unsigned int *inputBinaryBuffer;
	int *tempLarsson;
	Suffix *stringSuffixBuffer;	
	char partitionSuffixArrayFilePrefix [MAX_PATH_LENGTH]; //final output
	char orderArrayFilePrefix [MAX_PATH_LENGTH];
	int inputBufferMax;
	int outputBufferMax;
	unsigned int **inputInBinary;
	SimpleSuffix *outputSuffixArray;
	SimpleSuffix *outputSuffixArray2;
	int outputSA1Counter;
	int outputSA2Counter;
	int outputOrderArrayCounter;
	OrderArrayElement *outputOrderArray;
	Suffix **inputSuffixArrays;
	int *currStartPositionsInSufArrayFile; //for the next read, -1 if no more reads
	int *buffersLengths;  //the current lengths of the in-memory buffers
	int *buffersPointers;    //from 0 to bufferslengths[i]
	int currNumberOfStringsToMerge;
	HeapNode *heap;
	HeapNode lastTransferred;
	int heapCounter; //heap position counter
	FILE *currSAOutputFileFirst;
	FILE *currSAOutputFileSecond;
	FILE *currOrderArrayOutputFile;
	int currentFirstPartition;
	int currentSecondPartition;
	short writeSA1;
	short writeSA2;
}PairwiseSortingState;

int sortSuffixesOfStrings(PairwiseSortingState *state);
int buildAndOutputSuffixArray(PairwiseSortingState *state, int stringNumber);
void suffixsort(int *x, int *p, int n, int k, int l);
int buildSuffixArrayLarsson(int *buff,int *intBuffer,unsigned int *binaryinputseq,
							 int seq_len,	int binary_seq_length, int output_len,
							 FILE *outputFile, int fileNumber,Suffix *outputBuffer);


//merge
unsigned int getBitsPrefix(PairwiseSortingState *state, int fileNumber,int startPos);
int fillInitialBuffers(PairwiseSortingState *state);
int refillBuffer(PairwiseSortingState *state, int bufferNumber);
int initializeMerge(PairwiseSortingState *state);
int mergeSuffixArrays(PairwiseSortingState *state);



int getLCP(PairwiseSortingState *state, HeapNode *prev, HeapNode *next,short *firstbitafterlcp);
int compare(PairwiseSortingState *state, HeapNode *first, HeapNode *second);
Suffix *getNextStartPos(PairwiseSortingState *state, int bufferNumber);
void restartOutputBuffers(PairwiseSortingState *state); //empty when full
void addSuffixToOutputBuffers(PairwiseSortingState *state, 
							  HeapNode next); //here we transform to the partition start, and partition number instead of file number

} /* namespace libednai_b2st */
