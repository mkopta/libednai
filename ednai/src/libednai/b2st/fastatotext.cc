/*
 ============================================================================
 Name        : fastatotext.c
 Author      : M.Barsky
 Description : Converts fasta file into several text files
 An example usage is shown in main, which takes following arguments:
	<input folder> <input file name> <output folder> <output file prefix> <output file numeration start>
	<maxnumberofsequences> <maxlinelengthlength>
 ============================================================================
 */

#include "Preprocessing.h"

namespace libednai_b2st {

void copyString(char *from, char *to, int *size)
{
	int i=0;
	int endOfLine=0;
	while(!endOfLine)
	{
		if(from[i]==10 ||from[i]==32 ||from[i]=='\0')
		{
			endOfLine=1;
			*size=i;
			to[i]='\0';
		}
		else
		{
			to[i]=from[i];
			i++;
		}
		
	}
}

int fastaToTextFiles(char *fastaFileName, PreprocessingState *state, 
					 int numerationStart, int max_line, int maxPartitionSize)
{
	//int fileCurrSubscript=numerationStart;
	char currOutputName [MAX_PATH_LENGTH];
	
	char *line=(char*) calloc (max_line, sizeof(char));
	char *tmp=(char*) calloc (max_line, sizeof(char));
	char *key=(char*) calloc (max_line, sizeof(char));	
	
	FILE *fastaFile;
	FILE *outputFile=NULL;

	int totalStringLength=0;

	int writtenBytes;

	int stringsPerCurrentPartition=0;

	if(!(fastaFile= fopen ( fastaFileName , "rb" )))
	{
		printf("Could not open input fasta file \"%s\" \n", fastaFileName);
		return 1;
	}
	
	state->partitions[state->inputInfo[0].totalPartitions].firstSubscript=numerationStart;

	while(fgets(line, max_line, fastaFile)!=NULL)
	{
		int lineLength=0;
		copyString(line, tmp, &lineLength);
		
		if(lineLength>maxPartitionSize)
		{
			printf("Max partition size is smaller than 1 line of the input.\n"
				"Update Max partition size and try again \n");
			return 1;
		}

//1. DATA LINE
		if(line[0]!='>') //data line
		{	
			//1A. we can append current line to an existing file
			if(totalStringLength+lineLength<state->maxStringLength)
			{
				totalStringLength+=lineLength;
			
				if(outputFile==NULL) //first line
				{
					//and open file for writing this string or its part
					sprintf(currOutputName,"%s_%i%s", state->tempfileprefix,
							numerationStart+state->inputInfo[0].totalStrings,TXT_FILE);
					outputFile = fopen(currOutputName, "wb");
					if(outputFile==NULL) 
					{
						printf("Error: can't create output txt file %s.\n",currOutputName);
						return 1;
					}
				}
				writtenBytes=fwrite(line, sizeof(char), lineLength, outputFile);
				if(writtenBytes!=lineLength)
				{
					printf("Error: not all txt file was written\n");
					return 1;
				}
			}
			else //cannot append to the current string since the string itself is too big
				//in this case we create several files from the same string
			{
				//see if the existing current string CANNOT be appended to the current partition
				//1B - 1. 
				if(state->partitions[state->inputInfo[0].totalPartitions].partitionLength
					+totalStringLength >	maxPartitionSize)
				{
					//update an old partition 
					state->partitions[state->inputInfo[0].totalPartitions].numberOfStrings
						=stringsPerCurrentPartition;
					if(stringsPerCurrentPartition > state->inputInfo[0].maxStringsPerPartition)
						state->inputInfo[0].maxStringsPerPartition=stringsPerCurrentPartition;					
					
					//advance to a new partition
					(state->inputInfo[0].totalPartitions)++;
					state->partitions[state->inputInfo[0].totalPartitions].firstSubscript=
						state->inputInfo[0].totalStrings;
					state->partitions[state->inputInfo[0].totalPartitions].partitionLength
						=totalStringLength;
					stringsPerCurrentPartition=1;
					//partition of a new string part - after counter increases
					state->inputStrings[state->inputInfo[0].totalStrings].partitionNumber=
						state->inputInfo[0].totalPartitions;
				}
				else
				{
					//partition of an old string part
					state->inputStrings[state->inputInfo[0].totalStrings].partitionNumber=
						state->inputInfo[0].totalPartitions;
					stringsPerCurrentPartition++;
					//update the length of a partition - old 
					state->partitions[state->inputInfo[0].totalPartitions].partitionLength+=totalStringLength;
				}			
			
				//finish info for the current string				
				state->inputStrings[state->inputInfo[0].totalStrings].stringLength=totalStringLength;
				if(totalStringLength>state->inputInfo[0].maxLength)
					state->inputInfo[0].maxLength=totalStringLength;
				if(totalStringLength<state->inputInfo[0].minLength || state->inputInfo[0].minLength==0 )
					state->inputInfo[0].minLength=totalStringLength;
				state->inputStrings[state->inputInfo[0].totalStrings].subscript=
					state->inputInfo[0].totalStrings;
				
				
				(state->inputInfo[0].totalStrings)++;
				totalStringLength=lineLength;

				//open file and write current line for the next part - this will have the next subscript
				state->inputStrings[state->inputInfo[0].totalStrings].partitionNumber
					=state->inputInfo[0].totalPartitions;
				state->inputStrings[state->inputInfo[0].totalStrings].stringLength=0;
				totalStringLength=lineLength;
				state->inputStrings[state->inputInfo[0].totalStrings].subscript
					=state->inputInfo[0].totalStrings;
			
				fclose(outputFile);
				//and open file for writing this string or its part
				sprintf(currOutputName,"%s_%i%s", state->tempfileprefix,
						numerationStart+state->inputInfo[0].totalStrings,TXT_FILE);
				outputFile = fopen(currOutputName, "wb");
				if(outputFile==NULL) 
				{
					printf("Error: can't create output txt file %s.\n",currOutputName);
					return 1;
				}
				writtenBytes=fwrite(line, sizeof(char), lineLength, outputFile);
				if(writtenBytes!=lineLength)
				{
					printf("Error: not all txt file was written\n");
					return 1;
				}
			}
		}
		else  //line of key > -- symbolizes that current string is finished and the new one starts, unless this is the first line
		{
			if(outputFile!=NULL) //not first time key line
			{			
				state->inputStrings[state->inputInfo[0].totalStrings].stringLength=totalStringLength;
				state->inputStrings[state->inputInfo[0].totalStrings].subscript
					=numerationStart+state->inputInfo[0].totalStrings;
				
				
				if(totalStringLength>state->inputInfo[0].maxLength)
					state->inputInfo[0].maxLength=totalStringLength;
				if(totalStringLength<state->inputInfo[0].minLength || state->inputInfo[0].minLength==0 )
					state->inputInfo[0].minLength=totalStringLength;
				
				//if possible - add to a current partition, if not - open new partition and add there
				if(state->partitions[state->inputInfo[0].totalPartitions].partitionLength
					+totalStringLength>	maxPartitionSize)
				{
					//update an old partition 
					state->partitions[state->inputInfo[0].totalPartitions].numberOfStrings
						=stringsPerCurrentPartition;
					if(stringsPerCurrentPartition > state->inputInfo[0].maxStringsPerPartition)
						state->inputInfo[0].maxStringsPerPartition=stringsPerCurrentPartition;

					//partition of an old string part - before counter increases
					
					
					//advance to a new partition
					(state->inputInfo[0].totalPartitions)++;
					state->partitions[state->inputInfo[0].totalPartitions].firstSubscript=
						state->inputInfo[0].totalStrings;
					state->partitions[state->inputInfo[0].totalPartitions].partitionLength
						=totalStringLength;
					stringsPerCurrentPartition=1;
					state->inputStrings[state->inputInfo[0].totalStrings].partitionNumber=
						state->inputInfo[0].totalPartitions;
					sprintf(state->inputStrings[state->inputInfo[0].totalStrings].fasta_key,"%s", key);
				}
				else
				{
					//partition of an old string part
					state->inputStrings[state->inputInfo[0].totalStrings].partitionNumber=
						state->inputInfo[0].totalPartitions;
					stringsPerCurrentPartition++;
					//update the length of a partition - old 
					state->partitions[state->inputInfo[0].totalPartitions].partitionLength
						+=totalStringLength;		
				}			
			
				
				(state->inputInfo[0].totalStrings)++;			
				
				fclose(outputFile);
				totalStringLength=0;
				outputFile=NULL;			
			}
			copyString(tmp, key, &lineLength);
			sprintf(state->inputStrings[state->inputInfo[0].totalStrings].fasta_key,"%s", key);
	
			
		}
	}



	if(outputFile!=NULL) //not first time key line
	{			
		state->inputStrings[state->inputInfo[0].totalStrings].stringLength=totalStringLength;
		state->inputStrings[state->inputInfo[0].totalStrings].subscript
			=numerationStart+state->inputInfo[0].totalStrings;
		state->inputStrings[state->inputInfo[0].totalStrings].partitionNumber
			=state->inputInfo[0].totalPartitions;
		if(totalStringLength>state->inputInfo[0].maxLength)
			state->inputInfo[0].maxLength=totalStringLength;
		if(totalStringLength<state->inputInfo[0].minLength || state->inputInfo[0].minLength==0 )
			state->inputInfo[0].minLength=totalStringLength;
		
		//if possible - add to a current partition, if not - open new partition and add there
		if(state->partitions[state->inputInfo[0].totalPartitions].partitionLength
			+totalStringLength>	maxPartitionSize)
		{
			//update an old partition 
			state->partitions[state->inputInfo[0].totalPartitions].numberOfStrings
				=stringsPerCurrentPartition;
			if(stringsPerCurrentPartition > state->inputInfo[0].maxStringsPerPartition)
				state->inputInfo[0].maxStringsPerPartition=stringsPerCurrentPartition;

		
			//advance to a new partition
			(state->inputInfo[0].totalPartitions)++;
			state->partitions[state->inputInfo[0].totalPartitions].firstSubscript=
				state->inputInfo[0].totalStrings;
			state->inputStrings[state->inputInfo[0].totalStrings].partitionNumber=
						state->inputInfo[0].totalPartitions;
			state->partitions[state->inputInfo[0].totalPartitions].partitionLength
				=totalStringLength;
			stringsPerCurrentPartition=1;
		}
		else
		{
			//partition of an old string part
			state->inputStrings[state->inputInfo[0].totalStrings].partitionNumber=
				state->inputInfo[0].totalPartitions;
			stringsPerCurrentPartition++;
			//update the length of a partition - old 
			state->partitions[state->inputInfo[0].totalPartitions].partitionLength
				+=totalStringLength;		
		}			
	
		
		(state->inputInfo[0].totalStrings)++;			
		
		fclose(outputFile);
		totalStringLength=0;
		outputFile=NULL;			
	}
	
	state->partitions[state->inputInfo[0].totalPartitions].numberOfStrings=stringsPerCurrentPartition;
	if(stringsPerCurrentPartition > state->inputInfo[0].maxStringsPerPartition)
		state->inputInfo[0].maxStringsPerPartition=stringsPerCurrentPartition;
	(state->inputInfo[0].totalPartitions)++;
	return 0;
}

} /* namespace libednai_b2st */
