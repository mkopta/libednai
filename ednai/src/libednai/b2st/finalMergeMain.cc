#include "SuffixTree.h"

namespace libednai_b2st {

/*
This module performs final merge by sequential reads of pairwise order arrays
which is the main idea of BBST algorithm

Input: suffix arrays and the pairwise order arrays created in the previous step.
Output: suffix tree of the entire input, partitioned by lexicographic intervals

Parameters:
<inputSAsFolder> - temp folder where the suffix arrays for partitions and order arrays were written
<inputprefix> - prefix of the intermediate files
<outputfolder> - folder where the processed input strings and inputInfo are, the trees will be
whritten there, since suffix trees are not valid without the input strings
<outputtreeprefix> - how the output trees will be called
<memoryForInputBuffers> - this can take the entire available memory - memory for 1 final tree, 
since no memory for input strings is required anymore
<memoryPerOutputTree> - memory in bytes per 1 output tree
*/


int final_merge_main(const char *tmpdir, const char *tmpprefix,
    const char *outputdir, const char* outputprefix,
    int memforinputbuffers, int memoryforoutputtree)
{
	char infoFileName[MAX_PATH_LENGTH];
	char stringsFileName[MAX_PATH_LENGTH];
	char partitionsFileName[MAX_PATH_LENGTH];
	char inputFilePrefix[MAX_PATH_LENGTH];
	char outputFilePrefix[MAX_PATH_LENGTH];
	FILE *inputFile;
	char dividersFileName[MAX_PATH_LENGTH];
	int i,j;
	FinalMergeState state;
	int memoryForBuffers;
	int orderArraysCount;

	sprintf(inputFilePrefix,"%s/%s", tmpdir, tmpprefix);
	sprintf(outputFilePrefix,"%s/%s", outputdir, outputprefix);
	sprintf(infoFileName,"%s%s", outputFilePrefix, INPUT_INFO);
	sprintf(stringsFileName,"%s%s", outputFilePrefix, STRINGS_INFO);
	sprintf(partitionsFileName,"%s%s", outputFilePrefix, PARTITIONS_INFO);
	sprintf(state.partitionSuffixArrayFilePrefix,"%s%s", inputFilePrefix, PARTITION_SUFFIX_ARRAY);
	sprintf(state.orderArrayFilePrefix,"%s%s", inputFilePrefix, ORDER_ARRAY);
	sprintf(state.outputTreePrefix,"%s%s", outputFilePrefix, FINAL_TREE);
	//state.totalPartitions=getNumberOfPartitionsForFinalMerge(lengthsFileName);

	memoryForBuffers = memforinputbuffers;

	//1. read info to compute min substript,max subscript and maxfile size
	if(!(inputFile= fopen ( infoFileName , "rb" )))
	{
		printf("Could not open input info file %s for reading \n",infoFileName);
		return 1;
	}	
	if(fread(state.inputInfo, sizeof(InputInfo), 1, inputFile)!=1)
	{
		printf("Error reading input info file\n");
		return 1;
	}	
	fclose(inputFile);
	
	state.strings=(PartitionMember*) calloc (state.inputInfo[0].totalStrings, sizeof(PartitionMember)); 
	state.partitions=(PartitionInfo*) calloc (state.inputInfo[0].totalPartitions, sizeof(PartitionInfo)); 
	
	if(!(inputFile= fopen ( stringsFileName , "rb" )))
	{
		printf("Could not open string info file %s for reading \n",stringsFileName);
		return 1;
	}	
	if((int)fread(state.strings, sizeof(PartitionMember), state.inputInfo[0].totalStrings, inputFile)!=
		state.inputInfo[0].totalStrings)
	{
		printf("Error reading string info file\n");
		return 1;
	}	
	fclose(inputFile);
	
	if(!(inputFile= fopen ( partitionsFileName , "rb" )))
	{
		printf("Could not open partitions info file %s for reading \n",partitionsFileName);
		return 1;
	}	
	if((int)fread(state.partitions, sizeof(PartitionInfo), state.inputInfo[0].totalPartitions, inputFile)!=
		state.inputInfo[0].totalPartitions)
	{
		printf("Error reading partitions info file\n");
		return 1;
	}	
	fclose(inputFile);

	
	
	orderArraysCount=state.inputInfo[0].totalPartitions*(state.inputInfo[0].totalPartitions-1)/2;

	//allocate buffers
	state.outputBufferMax = memoryforoutputtree;
	printf("%d\n", memoryforoutputtree);
	state.outputBuffer=(STNode*) calloc (state.outputBufferMax, sizeof(STNode));
	state.outputNodesCounter=0;
	state.lastPath=(PathElement*) calloc (state.outputBufferMax, sizeof(PathElement));
	state.pathCounter=0;

	state.heap=(SimpleHeapNode*) calloc (state.inputInfo[0].totalPartitions, sizeof(SimpleHeapNode));

	
	state.inputBufferMax=(memoryForBuffers/(state.inputInfo[0].totalPartitions+orderArraysCount))/sizeof(SimpleSuffix);
	
	if((state.outputBufferMax*sizeof(STNode))<100000) //less than 1 MB per output buffer
	{
		printf("The selected size of the output buffer (%d elements) is too small. This will produce too many small output trees\n",state.outputBufferMax);
		printf("Please increase memory budget for this dataset, and try again\n");
		return 1;
	}
	if((state.inputBufferMax*sizeof(SimpleSuffix))<100000) //less than 1 MB per input buffer
	{
		printf("The computed size of each input buffer (%d elements) is too small.\n",state.inputBufferMax);
		printf("Please increase memory budget for this dataset, and try again\n");
		return 1;
	}
	printf("\n***********STARTED FINAL MERGE ********\n");
	printf("Input buf max=%d total %d input buffers; output buffer max=%d\n", state.inputBufferMax,
		(state.inputInfo[0].totalPartitions+orderArraysCount),state.outputBufferMax);

	//the lengths of current buffer for input suffix array
	state.buffersLengths=(int*) calloc (state.inputInfo[0].totalPartitions, sizeof(int));

	//the pointer for each suffix array buffer
	state.buffersPointers=(int*) calloc (state.inputInfo[0].totalPartitions, sizeof(int));

	//start position in suffix array file
	state.startPosInSAFile=(int *) calloc (state.inputInfo[0].totalPartitions, sizeof(int));
	
	//2D array of order array lengths - allocate (the length of 23 is read from position [2][3] of this array)
	state.orderArraysLengths=(int **) calloc (state.inputInfo[0].totalPartitions, sizeof(int*));
	for(i=0;i<state.inputInfo[0].totalPartitions;i++)
		state.orderArraysLengths[i]=(int *) calloc (state.inputInfo[0].totalPartitions, sizeof(int));
	for(i=0;i<state.inputInfo[0].totalPartitions-1;i++)
		for(j=i+1;j<state.inputInfo[0].totalPartitions;j++)
			state.orderArraysLengths[i][j]=state.partitions[i].partitionLength+
					state.partitions[j].partitionLength;

	//the lengths of current buffer for each order array buffer - also 2D array
	state.orderBuffersLengths=(int **) calloc (state.inputInfo[0].totalPartitions, sizeof(int*));
	for(i=0;i<state.inputInfo[0].totalPartitions;i++)
		state.orderBuffersLengths[i]=(int *) calloc (state.inputInfo[0].totalPartitions, sizeof(int));

	//the current pointers for each order array buffer
	state.orderBuffersPointers=(int **) calloc (state.inputInfo[0].totalPartitions, sizeof(int*));
	for(i=0;i<state.inputInfo[0].totalPartitions;i++)
		state.orderBuffersPointers[i]=(int *) calloc (state.inputInfo[0].totalPartitions, sizeof(int));

	//start position in order array file
	state.startPosInOrderArrayFile=(int **) calloc (state.inputInfo[0].totalPartitions, sizeof(int*));
	for(i=0;i<state.inputInfo[0].totalPartitions;i++)
		state.startPosInOrderArrayFile[i]=(int *) calloc (state.inputInfo[0].totalPartitions, sizeof(int));


	//5. open dividerFile for writing boundaries	
	sprintf(dividersFileName,"%s%s", outputFilePrefix,TREE_DIVIDERS);
	if(!(state.dividersFile=fopen(dividersFileName,"wb")))
	{
		printf("Cannot open dividersFile %s for writing\n",dividersFileName);
		exit(1);
	}

	//6. fill buffers for the first time and build heap
	// from the first elements of partitions
	if(fillInitialBuffersFinalMerge(&state))
		return 1;
	if(initializeFinalMerge(&state))
		return 1;

	//7. perform merge
	if( mergeSuffixArraysIntoFinalTree(&state))
		return 1;

	fclose(state.dividersFile);
	

	return 0;
}

} /* namespace libednai_b2st */
