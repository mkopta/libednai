#ifndef COMMON_H
#define COMMON_H

#include <stdio.h>
#include <stdlib.h>

namespace libednai_b2st {

#define MIN(a, b) ((a)<=(b) ? (a) : (b))
#define MAX(a, b) ((a)>=(b) ? (a) : (b))

#define MAX_PATH_LENGTH 200
#define TAIL_OVERLAP 1000

#define MAX_MOVE 40000000  //50 times less than the largest int

#define INPUT_INFO	"_input_info"  //file suffix for input info file
#define PARTITIONS_INFO	"_partitions_info"	//suffix for partitions info file
#define STRINGS_INFO	"_strings_info" //lengths of original strings file suffix
#define TXT_FILE ".txt"
#define NUMERIC_STRING "_smallnumeric"
#define BINARY_STRING "_smallbinary"
#define STRING_SUFFIX_ARRAY "_smallarray"
#define PARTITION_SUFFIX_ARRAY "_partition_sarray"
#define ORDER_ARRAY "_order_array"
#define FINAL_TREE "_final_tree"
#define TREE_DIVIDERS "_dividers"

extern FILE *debugFile;
extern int outputBufferSize; //output size of the buffer for collecting suffixes of each string

typedef struct SimpleSuffix
{
	int fileNumber;
	int startPos;
	unsigned int bitPrefix;
}SimpleSuffix;

typedef struct OrderArrayElement
{
	int LCP; //TBD if first bit is 0 - smaller partition, if 1 - larger partition
	char sign; //-1 - if smaller, 1 - if larger			//if second bit is 0/1 then the first bit after lcp is 0/1 - mostly 1
	char firstBitAfterLCP; //0 bit or 1 bit  			//remains 1 GB for the length of the lcp
}OrderArrayElement;

typedef struct PartitionInfo
{
	int firstSubscript;
	int numberOfStrings;
	int partitionLength;
}PartitionInfo;

typedef struct PartitionMember
{
	int subscript;
	int stringLength;
	int partitionNumber;
	int startPosInsidePartition;
	int binaryLength; //number of unsigned ints
	char fasta_key[MAX_PATH_LENGTH];
}PartitionMember;

typedef struct InputInfo
{
	int totalStrings;
	int minLength;
	int maxLength;
	int totalPartitions;
	int maxStringsPerPartition;
}InputInfo; 

//What information do we need about pairwise suffix arrays with LCP:
//we need total number of partitions
int getNumberOfPartitionsForFinalMerge(char *lengthsFileName); //we get it from the size of lengths file

int getPartitionLengthsForFinalMerge(char *lengthsFileName, int totalPartitions, 
									 int *partitionsLengths, int **orderArrayLengths);
//from lengths file we get the lengths of each partition and therefore of its suffix array
//and put them into int array partitionsLengths
//we compute pairwise lengths and put into 2D array orderArrayLengths

} /* namespace libednai_b2st */

#endif /* COMMON_H */
