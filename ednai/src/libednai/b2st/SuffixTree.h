#include "Common.h"

namespace libednai_b2st {

#define debug 0

typedef struct DividerElement
{
	unsigned int maxBitSequence;
	int treeFileNameSubscript;
	int totalNodes;
}DividerElement;



typedef struct SimpleHeapNode
{
	int startPos;
	int fileNumber;
	unsigned int bitPrefix;
}SimpleHeapNode;

typedef struct STNode
{	
	int startPos; //4
	int fileNumber; //4
	int length;  //4
	int children[2]; //8	
	int nextLeaf;	//4
}STNode;

typedef struct PathElement
{
	STNode *node;
	int length;
}PathElement;

typedef struct FinalMergeState
{
	InputInfo inputInfo [1];
	PartitionMember *strings;
	PartitionInfo *partitions;
	int **orderArraysLengths;
	char partitionSuffixArrayFilePrefix [MAX_PATH_LENGTH];
	char orderArrayFilePrefix[MAX_PATH_LENGTH];
	char outputTreePrefix [MAX_PATH_LENGTH];
	SimpleSuffix ** inputSABuffers;
	int *startPosInSAFile;
	OrderArrayElement *** inputOrderArrayBuffers;
	int *buffersLengths;  //the current lengths of the in-memory buffers
	int **orderBuffersLengths;  //the current lengths of the pairwise in-memory buffers
	int *buffersPointers;    //from 0 to bufferslengths[i]
	int **orderBuffersPointers;    //from 0 to bufferslengths[i]
	int **startPosInOrderArrayFile;
	int inputBufferMax;
	int outputBufferMax;
	FILE *dividersFile;
	STNode *outputBuffer;
	int outputNodesCounter;
	PathElement *lastPath;
	int pathCounter;
	SimpleHeapNode *heap;
	int heapCounter;
	STNode *outputRoot;
	SimpleHeapNode lastTransferred;
	int prevLCP;
	DividerElement currentDivider[1];
	int outputTreeFileNumberCounter;
	int lastAccessedFirstIndex;
	int lastAccessedSecondIndex;
	int dontChangelastAccessed;
}FinalMergeState;

//in partitions

//What information do we need about pairwise suffix arrays with LCP:
//we need total number of partitions
int getNumberOfPartitionsForFinalMerge(char *lengthsFileName); //we get it from the size of lengths file

int getPartitionLengthsForFinalMerge(char *lengthsFileName, int totalPartitions, 
									 int *partitionsLengths, int **orderArrayLengths);
//from lengths file we get the lengths of each partition and therefore of its suffix array
//and put them into int array partitionsLengths
//we compute pairwise lengths and put into 2D array orderArrayLengths


//in finalReader.c
int fillInitialBuffersFinalMerge(FinalMergeState *state);
int refillPairwiseBufferFinal(int first, int second, FinalMergeState *state);
int refillBufferFinal(int bufferNumber, FinalMergeState *state);
void fseekXXLFinal(FILE *file, int pos);
void fseekXXLOrderArray(FILE *file, int pos);


//in finalMergeST.c
int initializeFinalMerge(FinalMergeState *state);
//int addSuffixToEmptyHeap(FinalMergeState *state, SimpleSuffix *suffix);

SimpleSuffix *getNextSuffix(FinalMergeState *state,int partitionNumber);
int addNextSuffixToHeap(FinalMergeState *state, SimpleSuffix *suffix);
int compare (int firstFile,  int secondFile, FinalMergeState *state);

int  mergeSuffixArraysIntoFinalTree(FinalMergeState *state);

SimpleHeapNode getSmallestSimpleHeapNode(FinalMergeState *state);
void moveUpPath(int lcp, FinalMergeState *state);
int addToFinalOutputTree(SimpleHeapNode next, FinalMergeState *state);

int restartFinalOutputTree(FinalMergeState *state);

int moveOrderArrayPointers(FinalMergeState *state, int partitionNumber);
int getLCP(FinalMergeState *state, int first, int second,char *firstbitafterlcp);

} /* namespace libednai_b2st */
