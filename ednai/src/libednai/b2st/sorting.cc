#include "SuffixSorting.h"

namespace libednai_b2st {

int buildAndOutputSuffixArray(PairwiseSortingState *state, int stringNumber)
{
	char currnumericfilename [MAX_PATH_LENGTH];
	char currbinaryfilename [MAX_PATH_LENGTH];
	char currsarrayfilename [MAX_PATH_LENGTH];
	
	FILE *outputFile;
	FILE *inputFile;

	int result;
	
	int writtensuffixes;

	sprintf(currnumericfilename,"%s_%i", state->inputFilePrefixForNumeric,stringNumber);
	sprintf(currbinaryfilename,"%s_%i", state->inputFilePrefixForBinary,stringNumber);
	sprintf(currsarrayfilename,"%s_%i", state->stringSuffixArrayFilePrefix,stringNumber);

	//read numeric input into int array	
	inputFile = fopen(currnumericfilename, "rb");
	if(inputFile==NULL) 
	{
		printf("Error: can't open input file %s to read numeric encoded DNA.\n",currnumericfilename);
		return 2;
	}

	result = fread (state->inputNumericBuffer,sizeof(int),
				state->strings[stringNumber].stringLength,inputFile);

	if (result != state->strings[stringNumber].stringLength) 
	{
		printf ("Reading numeric DNA error 2\n");
		return 1;
	}
	
fclose(inputFile);
	//2. read binary input to attach binary prefixes
	inputFile = fopen(currbinaryfilename, "rb");
	if(inputFile==NULL) 
	{
		printf("Error: can't open file %s of binary encoded DNA.\n",currbinaryfilename);
		return 2;
	}

	result = fread (state->inputBinaryBuffer,sizeof(unsigned int),
		state->strings[stringNumber].binaryLength,inputFile);


	if (result != state->strings[stringNumber].binaryLength) 
	{
		printf ("Reading binary DNA error 2\n");
		return 1;
	}
fclose(inputFile);	
	if(!(outputFile= fopen ( currsarrayfilename , "wb" )))
	{
		printf("Could not open suffix array file \"%s\" for output \n", currsarrayfilename);
		return 1;
	}

	writtensuffixes=buildSuffixArrayLarsson(state->tempLarsson,state->inputNumericBuffer,
		state->inputBinaryBuffer,state->strings[stringNumber].stringLength,
		state->strings[stringNumber].binaryLength, state->strings[stringNumber].stringLength,
		outputFile,stringNumber,state->stringSuffixBuffer);
	
	if(writtensuffixes!=state->strings[stringNumber].stringLength)
	{
		printf("error writing suffixes of string %d\n",stringNumber);
		return 1;
	}	
	
	fclose(outputFile);
	return 0;
}


int sortSuffixesOfStrings(PairwiseSortingState *state)
{
	
	int i;	
	printf("\n**********Started suffix sorting of separate strings (Sorting phase of DiGeST)****\n\n");
	//4.sort suffixes in each file and output small suffix arrays
	for(i=0;i<state->inputInfo[0].totalStrings;i++)  
	{	
		printf("Performing in-memory sorting of suffixes for the %i-th string (out of %i). Please wait ...\n",(i+1),state->inputInfo[0].totalStrings);
		if(buildAndOutputSuffixArray(state,i))
			return 1;	
	
		printf("Finished sorting suffixes of the %i-th string\n\n",(i+1));
	
	}
	
	return 0;
}

} /* namespace libednai_b2st */
