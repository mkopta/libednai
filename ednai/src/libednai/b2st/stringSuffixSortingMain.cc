#include "SuffixSorting.h"

namespace libednai_b2st {

/*This module produces the sorted suffixes of each input string created in the preprocessing step,
and writes them as separate files into temp folder, those will be used for digest-like pairwise
partition sorting

//input: 
//1. input strings as chars (output folder), as numbers (temp folder), as bits (temp folder)in files
//2. inputInfo: in file in output folder

//output: suffix arrays of each input string in separate file in temp folder

Parameters:
<outputfilefolder> - the folder where the input info was written in the preprocessing
<tempfilefolder> - the folder with temporary files
<inputfileprefix> - prefix given for each input file in a preprocessing step
<memforinputbuffers> - 

*/

int string_suffix_sorting_main(const char *outputdir, const char *tmpdir,
    const char *prefix)
{	
	char inputFilePrefix [MAX_PATH_LENGTH];	
	char infoFileName [MAX_PATH_LENGTH];
	char stringsFileName [MAX_PATH_LENGTH];
	char partitionsFileName [MAX_PATH_LENGTH];


	FILE *inputFile;
	PairwiseSortingState state;

	sprintf(inputFilePrefix,"%s/%s", tmpdir, prefix);	
	sprintf(infoFileName,"%s/%s%s", outputdir, prefix, INPUT_INFO);
	sprintf(stringsFileName,"%s/%s%s", outputdir, prefix, STRINGS_INFO);
	sprintf(partitionsFileName,"%s/%s%s", outputdir, prefix, PARTITIONS_INFO);


	sprintf(state.inputFilePrefixForNumeric,"%s%s", inputFilePrefix, NUMERIC_STRING);
	sprintf(state.inputFilePrefixForBinary,"%s%s", inputFilePrefix, BINARY_STRING);

	sprintf(state.stringSuffixArrayFilePrefix,"%s%s", inputFilePrefix, STRING_SUFFIX_ARRAY);
	
	sprintf(state.partitionSuffixArrayFilePrefix,"%s%s", inputFilePrefix, PARTITION_SUFFIX_ARRAY);
	sprintf(state.orderArrayFilePrefix,"%s%s", inputFilePrefix, ORDER_ARRAY);

	//1. read info to compute min substript,max subscript and maxfile size
	if(!(inputFile= fopen ( infoFileName , "rb" )))
	{
		printf("Could not open input info file %s for reading \n",infoFileName);
		return 1;
	}	
	if(fread(state.inputInfo, sizeof(InputInfo), 1, inputFile)!=1)
	{
		printf("Error reading input info file\n");
		return 1;
	}	
	fclose(inputFile);
	
	state.strings=(PartitionMember*) calloc (state.inputInfo[0].totalStrings, sizeof(PartitionMember)); 
	state.partitions=(PartitionInfo*) calloc (state.inputInfo[0].totalPartitions, sizeof(PartitionInfo)); 
	
	if(!(inputFile= fopen ( stringsFileName , "rb" )))
	{
		printf("Could not open string info file %s for reading \n",stringsFileName);
		return 1;
	}	
	if((int)fread(state.strings, sizeof(PartitionMember), state.inputInfo[0].totalStrings, inputFile)!=
		state.inputInfo[0].totalStrings)
	{
		printf("Error reading string info file\n");
		return 1;
	}	
	fclose(inputFile);
	
	if(!(inputFile= fopen ( partitionsFileName , "rb" )))
	{
		printf("Could not open partitions info file %s for reading \n",partitionsFileName);
		return 1;
	}	
	if((int)fread(state.partitions, sizeof(PartitionInfo), state.inputInfo[0].totalPartitions, inputFile)!=
		state.inputInfo[0].totalPartitions)
	{
		printf("Error reading partitions info file\n");
		return 1;
	}	
	fclose(inputFile);


	//2. Allocate buffers: 2 for inputs 1 for temp 1 for output suffix array for each string
	state.inputNumericBuffer=(int*) calloc (state.inputInfo[0].maxLength, sizeof(int));
	state.inputBinaryBuffer=(unsigned int*) calloc ((state.inputInfo[0].maxLength*2)/32+2, sizeof(unsigned int));
	state.tempLarsson=(int *)calloc (state.inputInfo[0].maxLength, sizeof(int));

	outputBufferSize=MIN(state.inputInfo[0].minLength, SUFFIX_COLLECTOR_SIZE);
	state.stringSuffixBuffer=(Suffix *)calloc (outputBufferSize,sizeof(Suffix));
	
	//3. Sort suffixes of each input string
	if(sortSuffixesOfStrings(&state))
		return 1;

	
	
	return 0;
}

} /* namespace libednai_b2st */
