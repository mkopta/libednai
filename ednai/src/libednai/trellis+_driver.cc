/* Copyright (c) 2011, Martin Kopta <martin@kopta.eu>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include "./libednai.h"
#include "./trellis/trellis.h"
#include "./trellis+/trellis+.h"
#include <libgen.h> /* basename */

namespace libednai {

int trellis_plus_driver::create_index(string inputfile, string outputdir,
  string tmpdir, int memory /* MB */) {

  int retval;

  cout << "+-----------------------------------------+" << endl;
  cout << "| Creating index using TRELLIS+ algorithm |" << endl;
  cout << "+-----------------------------------------+" << endl;
  /* FASTA input to NUM format conversion */
  cout << "* Phase 1: converting FASTA input to NUM format" << endl;
  string inputfile_basename = string(
      basename(
        const_cast<char *> ( // ugly, but doesn't matter
          inputfile.c_str())));
  string numfile =
    tmpdir + string("/") + inputfile_basename + string(".num"); 
  retval = libednai_trellis::fast2num_mypst_main(
      inputfile.c_str(), numfile.c_str(), 0);
  if (retval) {
    PRINT_ERROR("Phase 1 failed. Exiting.");
    return 1;
  }
  cout << "[Phase 1 done]" << endl;

  /* create index */
  /* trellis -i human100MB.num -m 1024000 -l 3 */
  cout << "* Phase 2: creating index" << endl;
  PRINT_WARNING("Make sure your ulimit allows at least 1024 files to be open");
  PRINT_WARNING("Write down printed threshold value for later use.");
  retval = libednai_trellis_plus::trellis_plus_main(
      numfile.c_str(), outputdir.c_str(), tmpdir.c_str(),
      memory /* MB */, 3 /* prefix length */);
  if (retval) {
    PRINT_ERROR("Phase 2 failed. Exiting.");
    return 1;
  }
  cout << "[Phase 2 done]" << endl;

  /* add suffix links */
  cout << "* Phase 3: calculating suffix links" << endl;
  cout << "dirPath    : "
    << libednai_trellis_plus::trellis_global_dirPath() << endl;
  cout << "tempDirPath: "
    << libednai_trellis_plus::trellis_global_tempDirPath() << endl;
  cout << "prefix file: "
    << libednai_trellis_plus::trellis_global_prefixFileName() << endl;
  cout << "threshold  : "
    << libednai_trellis_plus::trellis_global_threshold() << endl;
  retval = libednai_trellis::AddSFLinks_main(
   libednai_trellis_plus::trellis_global_tempDirPath(),
   libednai_trellis_plus::trellis_global_prefixFileName(),
   libednai_trellis_plus::trellis_global_threshold(),
   numfile.c_str());
  if (retval) {
    PRINT_ERROR("Phase 3 failed. Exiting.");
    return 1;
  }
  cout << "[Phase 3 done]" << endl;

  return 0;
}

} /* namespace libednai */
