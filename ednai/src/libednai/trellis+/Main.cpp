#include <sstream>
#include <iostream>
#include <unistd.h>
#include <string>
#include <new>
#include "Global.h"
#include "MergeTree.h"
#include "PrefixGen.h"
#include "SuffixTree.h"
#include "SmallSuffixTree.h"
#include "LoadedTree.h"
#include "StringBuffer.h"

namespace libednai_trellis_plus {

void createPSTFiles()
{
  cout << "Creating files... for " << prefVec.size() << " prefixes" <<  endl;
  TimeTracker tt;
  tt.Start();
  string com;
  for(UI4Bytes pn = 0; pn < prefVec.size(); ++pn)
    {
      com = tempDirPath;
      stringstream ss;
      ss << (int)pn;
      com += ss.str();
       
      int fd = open(com.c_str(), O_RDWR | O_CREAT, S_IRUSR | S_IWUSR);
      
      if(fd == -1)
	{
	  cerr << "Error opening file " << com << endl;
	  abort();
	}
      if (ftruncate(fd, 25*freqVec[pn])) abort();
      close(fd);
    }
  cout << "Done! Time taken = " << tt.Stop() << " seconds.\n" << endl;
}

void showUsage()
{ 
  cout << "./trellis -i inputFileName -m availMEM(KB) (-l initialPrefixLength | -p prefixFileName)" << endl;
}

void prepDir(const char *outputdir, const char *tmpdir)
{
  TimeTracker tt;
  tt.Start();
  cout << "Preparing the directories ... \n" << flush;

  strcpy(tempDirPath, tmpdir);
  strcat(tempDirPath, "/trellis_tmp_data/");
  strcpy(dirPath, outputdir);
  strcat(dirPath, "/trellis_bin_data/");

  string cmd1 = string("rm -rf ") + string(tempDirPath);
  string cmd2 = string("mkdir ")  + string(tempDirPath);
  string cmd3 = string("rm -rf ") + string(dirPath);
  string cmd4 = string("mkdir ")  + string(dirPath);
   
  cout << "Executing " << cmd1 << endl;
  if (system(cmd1.c_str())) abort(); //rm
  cout << "Executing " << cmd2 << endl;
  if (system(cmd2.c_str())) abort(); //mkdir
  cout << "Executing " << cmd3 << endl;
  if (system(cmd3.c_str())) abort(); //rm
  cout << "Executing " << cmd4 << endl;
  if (system(cmd4.c_str())) abort(); //mkdir

  strcpy(prefixFileName, dirPath);
  strcat(prefixFileName, "prefix.txt");

  cout << "Done! Time taken = " << tt.Stop() << " seconds.\n" << endl;
}

int trellis_plus_main(const char *inputfile, const char *outputdir,
    const char *tmpdir, int memory /* MB */, int preflen)
{ 
  TimeTracker allTime, p1Time;
  allTime.Start();

  UI4Bytes availMem = 0;
  inputFileName = const_cast<char *> (inputfile);
  cout << "Input file: " << inputFileName << endl;
  availMem = memory * 1024; /*kB*/
  cout << "Available memory = " << availMem << "KB" << endl;
  maxPrefLen = preflen;
  cout << "Initial Prefix Length: " << maxPrefLen << endl;
  ifstream f(inputFileName);
  
  f.seekg(0, ios::end);
  fileSize = f.tellg();
  f.seekg(0, ios::beg);

  UI4Bytes smallTreeSize = 2000000;
  char *smallStr = new char[smallTreeSize];
  f.read(smallStr, smallTreeSize);
  string tempS = smallStr;
  tempS += '0';
  
  SmallSuffixTree sst(&tempS, fileSize);
  
  f.seekg(0, ios::beg);

  cout << "sizeof(I_Node) = " << sizeof(I_Node) << endl;
  cout << "sizeof(L_Node) = " << sizeof(L_Node) << endl;
  cout << "sizeof(I_LoadedNode) = " << sizeof(I_LoadedNode) << endl;
  cout << "sizeof(L_LoadedNode) = " << sizeof(L_LoadedNode) << endl;

  availMem -= (68*1024); 
  cout << "availMem = " << availMem/1024 << " MB" << endl;
  
  factor_phase1 = 0.8, factor_phase2 = 2;

  candT1 = (availMem*1024)/(int)((factor_phase1*sizeof(I_Node))+sizeof(L_Node)+1);

  candT2 = (availMem*1024)/(int)((1.75*sizeof(I_LoadedNode)) + sizeof(L_LoadedNode)); 
  
  cout << "candT1 " << candT1 << endl; 
  cout << "candT2 " << candT2 << endl;
  threshold = candT1 < candT2 ? candT1 : candT2;
  cout << "Threshold = " << threshold << endl;
  
  prepDir(outputdir, tmpdir);
  generateAllDynamicPrefixes();
 
  if(prefVec.size() >= 10240)
    {
      cout << "Too many prefixes: " << prefVec.size() << endl;
      return 1;
    }

  vector<short> tempV;
  for(unsigned int i = 0; i != prefVec.size(); ++i)
    noPrefVec.push_back(tempV);

  cout << "Input file size = " << fileSize << endl;
  
  int totalParNum = fileSize / candT1, r = 0; 
  if(fileSize%candT1 != 0)
    ++totalParNum;  
  
  UI4Bytes extraLen = 50000; 
  
  char *extraS = new char[extraLen], *pStr;
  cout << "#Partitions Required = " << totalParNum << endl;
  string fname1;
  
  createPSTFiles();
  
  cout << "// Creating suffix trees for all partitions" << endl;

  p1Time.Start();


  I_NodeMax = (UI4Bytes)(candT1 * factor_phase1);
  cout << "Initializing the raw memory for " << I_NodeMax << " I_Nodes ..." << flush;
  rawMemory0 = operator new[](sizeof(I_Node) * I_NodeMax);
  I_NodeArray = static_cast<I_Node*>(rawMemory0);
  cout << "Finish." << endl;
  cout << "I_Node size = " << sizeof(I_Node) << endl;
  cout << "Size for " << I_NodeMax << " I_Nodes = " << sizeof(I_Node)*I_NodeMax/(1024*1024) << " MB.\n" << endl;
  
  L_NodeMax = (UI4Bytes)(candT1);
  cout << "Initializing the raw memory for " << L_NodeMax << " L_Nodes ..." << flush;
  rawMemory1 = operator new[](sizeof(L_Node) * L_NodeMax);
  L_NodeArray = static_cast<L_Node*>(rawMemory1);
  cout << "Finish." << endl;
  cout << "L_Node size = " << sizeof(L_Node) << endl;
  cout << "Size for " << L_NodeMax << " L_Nodes = " << sizeof(L_Node)*L_NodeMax/(1024*1024) << " MB.\n" << endl;

  tfArray = new fstream[prefVec.size()];
  for(UI4Bytes i = 0; i < prefVec.size(); ++i)
    {
      fname1 = tempDirPath;
      stringstream ss;
      ss << i;
      fname1 += ss.str();
      tfArray[i].open(fname1.c_str(), fstream::in | fstream::out | fstream::binary);
    }
  
  maxBufSize = 20 * (candT1/prefVec.size()) * (sizeof(char)+sizeof(short)+(3*UI4SIZE));
  cout << "maxBufSize = " << maxBufSize << endl; //size in bytes
  charBuf = new char[maxBufSize];

  TimeTracker pT;
  UI4Bytes maxICount = 0;

  int ee = 0;
  while(r < totalParNum)
    {

      cout << "================= Partition " << r << " out of " << totalParNum-1 << " ===================" << endl;
      pT.Start(); 
      
      if(r>0) 
	f.seekg(-1*ee, ios::cur);

      if(r == totalParNum-1 && fileSize%candT1 !=0)
	{
	  cout << "Last unequal string, read at " << f.tellg() << endl;
	  pStr = new char[fileSize%candT1 + 1];
	  f.read(pStr, fileSize % candT1);
	  pStr[fileSize % candT1] = '0';
	}
      else
	{ 
	  if(r != totalParNum-1)
	    ee = extraLen;

	  pStr = new char[candT1+1+ee];
	  f.read(pStr, candT1+ee);

	  pStr[candT1+ee]='\0'; 
	}

      SimpleString ss(pStr, ee);
      s = &ss;


      I_NodeCount = 0;
      L_NodeCount = 0;
      SuffixTree st(r==(totalParNum-1)); 
      currentEnd = fileSize; 
      
       iFound = 0; lFound = 0;
      offset = r*candT1;
      st.storeSubTrees(r, sst); 
            
      delete []pStr;
      ++r;
      cout << "Total time for this partition = " << pT.Stop() << endl;
            
      if(maxICount < I_NodeCount)
	maxICount = I_NodeCount;
    }
   
  cout << "bigRangeVec.size() = " << bigRangeVec.size() << endl;
  int sumLen = 0;
    
    for(unsigned int i = 0; i != bigRangeVec.size(); ++i)
    {
      sumLen += (bigRangeVec[i].end - bigRangeVec[i].beg + 1);
    }
  cout << "I sumLen = " << sumLen << endl;
  
  delete []extraS;
  delete []charBuf;
  cout << "Done!" << endl;

  for(UI4Bytes i = 0; i < prefVec.size(); ++i)
    tfArray[i].close();
  cout << "All temp files closed." << endl;

  for(UI4Bytes i = 0; i < maxICount;  ++i)
    I_NodeArray[i].~I_Node();
  operator delete[](rawMemory0);
  cout << "All I_Nodes destroyed." << endl;

  for(UI4Bytes i = 0; i < L_NodeCount; ++i)
    L_NodeArray[i].~L_Node();
  operator delete[](rawMemory1);
  cout << "All L_Nodes destroyed." << endl;
  
  cout << "Phase 1 finished in " << p1Time.Stop() << " seconds" << endl;
    
  cout << "// Merging and creating prefixed suffix sub-trees" << endl;

  cout << "Prefixes not in Partitions Summary" << endl;
  for(unsigned int i = 0; i != noPrefVec.size(); ++i)
    {
      if(noPrefVec[i].empty())
        continue;
      cout << "prefVec[" << i << "] OR " << prefVec[i] << " is not in partition# " << flush;
      for(unsigned int j = 0; j != noPrefVec[i].size(); ++j)
        cout << noPrefVec[i][j] << ", ";
      cout << endl;
    }
  cout << endl;

  I_LoadedNodeMax = (UI4Bytes)(threshold*factor_phase2); 
  cout << "Initializing the raw memory for " << I_LoadedNodeMax << " I_LoadedNodes ..." << flush;
  void *rawMemory3 = operator new[](sizeof(I_LoadedNode)*I_LoadedNodeMax);
  I_LoadedNodeArray = static_cast<I_LoadedNode*>(rawMemory3);
  cout << "Finish." << endl;
  cout << "I_LoadedNode size = " << sizeof(I_LoadedNode) << endl;
  cout << "Size for " << I_LoadedNodeMax << " I_LoadedNodes = " 
       << sizeof(I_LoadedNode)*I_LoadedNodeMax/(1024*1024) << " MB.\n" << endl;
  
  L_LoadedNodeMax = (UI4Bytes)(threshold);
  cout << "Initializing the raw memory for " << L_LoadedNodeMax << " L_LoadedNodes ..." << flush;
  void *rawMemory4 = operator new[](sizeof(L_LoadedNode)*L_LoadedNodeMax);
  L_LoadedNodeArray = static_cast<L_LoadedNode*>(rawMemory4);
  cout << "Finish." << endl;
  cout << "L_LoadedNode size = " << sizeof(L_LoadedNode) << endl;
  cout << "Size for " << L_LoadedNodeMax << " L_LoadedNodes = " 
       << sizeof(L_LoadedNode)*L_LoadedNodeMax/(1024*1024) << " MB.\n" << endl;

  currentEnd = fileSize;
  
  cout << "Merging the trees...\n" << endl;
  string fname;
  TimeTracker p3Timer;
  p3Timer.Start();

  StringBuffer ph2Str(bigRangeVec, inputFileName, candT1);
  MergeTree<StringBuffer> MT(&ph2Str, &sst);
  MT.parSize = candT1;
  
  int pc = 0;
  TimeTracker pTime, rTime; 
   
  valid = new bool[(int)(threshold*factor_phase2)];

  bool firstTreeFound = false;
 
  for(vector<PREF>::iterator pref = prefVec.begin(); pref != prefVec.end(); ++pref)
    {      
      firstTreeFound = false;
      
      for(UI4Bytes i = (UI4Bytes)(threshold*factor_phase2); i--;)
	valid[i] = true;
      
      fname1 = tempDirPath;
      stringstream ss;
      ss << pc;
      fname1 += ss.str();

      invalid = 0;
      nMergeCreated = 0;

      cout << pc << ") Prefix " << *pref << " open " << fname1 << endl;
      fstream f(fname1.c_str(), fstream::in | fstream::out | fstream::binary);
      pTime.Start();
            
      I_LoadedNodeCount = 0;
      L_LoadedNodeCount = 0;
      LoadedTree ltree;
      I_LoadedNode *root1 = NULL, *root2 = NULL;

      for(int r = 0 ; r < totalParNum; ++r)
	{ 	
	  ph2Str.loadPartition(r);
	  
	  if(find(noPrefVec[pc].begin(), noPrefVec[pc].end(), r) != noPrefVec[pc].end())
	    {
	      cout << "r = " << r << " doesn't have this prefix " << prefVec[pc] << endl;
	      continue;
	    }

	  if(!firstTreeFound)
	    {
	      root1 = ltree.load(f, &ph2Str);
	      firstTreeFound = true;
	    }
	  else
	    {
	      root2 = ltree.load(f, &ph2Str);
	      MT.mergeTreeRoots(root1, root2);
	    }
	}

      f.seekg(0, ios::beg);
      
      UI4Bytes iBufPos = 0, cIndex = 0, zero = 0;

      UI4Bytes iBufSize = 256*KBYTE;
      char *iBuf = new char[iBufSize];

      LoadedNode *child = NULL;
      
      invalid = 0;
      for(UI4Bytes i = 0; i < I_LoadedNodeCount; ++i)
	{
	  if(!valid[i])
	    ++invalid;
	  I_LoadedNodeArray[i].actualIndex = i - invalid;
	}

      for(UI4Bytes i = 0; i < I_LoadedNodeCount; ++i)
	{
	  if(!valid[i])
	    continue;

	  if(iBufPos == iBufSize)
	    {
	      f.write(iBuf, iBufSize);
	      iBufPos = 0;
	    }
	  memcpy(iBuf+iBufPos, &I_LoadedNodeArray[i].s, UI4SIZE);
	  iBufPos+=UI4SIZE;

	  if(iBufPos == iBufSize)
	    {
	      f.write(iBuf, iBufSize);
	      iBufPos = 0;
	    }
	  memcpy(iBuf+iBufPos, &I_LoadedNodeArray[i].e, UI4SIZE);
	  iBufPos+=UI4SIZE;

	  for(UI4Bytes j = 0; j <= NUMCHAR; ++j)
	    {
	      child = I_LoadedNodeArray[i].children[j]; 

	      if(child != NULL)
		{ 
		  if(child->isLeaf())
		    {
		      cIndex = 1+ (((intptr_t)child - (intptr_t)&L_LoadedNodeArray[0])/sizeof(L_LoadedNode));
		      if(iBufPos == iBufSize)
			{
			  f.write(iBuf, iBufSize);
			  iBufPos = 0;
			}
		      memcpy(iBuf+iBufPos, &cIndex, UI4SIZE);
		      iBufPos += UI4SIZE;
		    }
		  else
		    {
		      UI4Bytes tempOrder = 1 + threshold + (static_cast<I_LoadedNode*>(child))->actualIndex;
		      if(iBufPos == iBufSize)
			{
			  f.write(iBuf, iBufSize);
			  iBufPos = 0;
			}
		      memcpy(iBuf+iBufPos, &tempOrder, UI4SIZE);
		      iBufPos += UI4SIZE;
		      
		    }
		} 
	      else
		{
		  if(iBufPos == iBufSize)
		    {
		      f.write(iBuf, iBufSize);
		      iBufPos = 0;
		    }
		  memcpy(iBuf+iBufPos, &zero, UI4SIZE);
		  iBufPos += UI4SIZE;
		}
	    } 
	} 
      
      gTimer.Start();

      if(iBufPos > 0)
	f.write(iBuf, iBufPos);

      diskTimeW+=gTimer.Stop();
            
      delete []iBuf;
      f.close();
  
      iBufSize = (I_LoadedNodeCount-invalid)*((NUMCHAR+3)*UI4SIZE);
      int fd = open(fname1.c_str(), O_RDWR | S_IRUSR | S_IWUSR);
      if(fd == -1)
	{
	  cerr << "Error opening file " << fname1 << endl;
	  return 1;
	}
      if (ftruncate(fd, iBufSize)) abort();
      close(fd);

      string leafFileName = dirPath;
      leafFileName += "L_";
      leafFileName += *pref; 
      ofstream leafFile(leafFileName.c_str(), ios::binary);
      UI4Bytes leafBufSize = 256*KBYTE;
      char *tempBuf = new char[leafBufSize];
      
      UI4Bytes tempO=0;


      for(UI4Bytes i = 0; i < L_LoadedNodeCount; ++i)
	{
	  if(tempO == leafBufSize)
	    {
	      leafFile.write(tempBuf, tempO);
	      tempO = 0;
	    }
	  memcpy(tempBuf+tempO, &L_LoadedNodeArray[i].s, UI4SIZE);
	  tempO += UI4SIZE;
	  
	  if(tempO == leafBufSize)
	    {
	      leafFile.write(tempBuf, tempO);
	      tempO = 0;
	    }
	  memcpy(tempBuf+tempO, &L_LoadedNodeArray[i].as, UI4SIZE);
	  tempO += UI4SIZE;
	}
      gTimer.Start();
      
      if(tempO > 0)
	leafFile.write(tempBuf, tempO);
      diskTimeW+=gTimer.Stop();
      leafFile.close();
      delete []tempBuf;
           
      cout << "Time taken for this prefix " << pTime.Stop() << endl;

      pTime.Start();
      
      ++pc;
    }

  cout << "Time for merging " << p3Timer.Stop() << " seconds" << endl;

  double totalTime = allTime.Stop();
  cout << "\nTotal time for the program " << totalTime << " secs OR " << totalTime/60.0 
       << " mins OR " << totalTime/3600.0 << " hrs\n" << endl;

  cout << "Total Disk Write = " << diskTimeW << " seconds OR " << diskTimeW/60.0 << " mins." << endl;
  cout << "Total Disk Read = " << diskTimeR << " seconds OR " << diskTimeR/60.0 << " mins." << endl;
  cout << "Threshold " << threshold << endl;
  return 0;
}

char *trellis_global_dirPath(void) {
  return dirPath;
}

char *trellis_global_tempDirPath(void) {
  return tempDirPath;
}

char *trellis_global_prefixFileName(void) {
  return prefixFileName;
}

unsigned trellis_global_threshold(void) {
  return threshold;
}

} /* namespace libednai_trellis_plus */
