#ifndef SSTREE_H
#define SSTREE_H

#include <vector>
#include <fstream>
#include "Node.h"

#include "TimeTracker.h"
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <string>
#include "SimpleString.h"

namespace libednai_trellis_plus {

#define KBYTE 1024
#define BUFSIZE KBYTE 

using namespace std;

class SmallSuffixTree
{
 public: 
  SmallSuffixTree(string *s, const unsigned int & origSize);
  SmallSuffixTree()
    {
      rawMemory0_ = NULL;
      I_NodeArray_ = NULL;
      I_NodeCount_ = 0;
      
      rawMemory1_ = NULL;
      L_NodeArray_ = NULL;
      L_NodeCount_ = 0;
    }
  ~SmallSuffixTree(){}
  
  void destroy()
    {
      for(UI4Bytes i = 0; i < I_NodeCount_;  ++i)
	I_NodeArray_[i].~I_Node();
      operator delete[](rawMemory0_);
      cout << "All I_Nodes in the small sfTree destroyed." << endl;
    } 
 
  I_Node* getRoot() { return root_; }  
  void setRoot(I_Node* r) { root_ = r; }

  template <class T>
  bool match(T *bigS, const UI4Bytes & startLoc, 
	     const UI4Bytes & len, pair<Node*, UI4Bytes> & p);

  bool match(SimpleString *bigS, const UI4Bytes & startLoc, 
	     const UI4Bytes & len, pair<Node*, UI4Bytes> & p, UI4Bytes & length_matched);

    
 private: 
  I_Node *root_, *lastInternalNodeAdded_;
  void *rawMemory0_, *rawMemory1_;
  I_Node *I_NodeArray_;
  L_Node *L_NodeArray_;
  UI4Bytes I_NodeCount_, L_NodeCount_;
  string* s_;
  unsigned int origStrSize;

  template <class T>
  UI4Bytes match(T *bigS, UI4Bytes begin_pos, const UI4Bytes & len, 
		 Node* curr_node, UI4Bytes loc, 
		 pair<Node*, UI4Bytes> & p);

  UI4Bytes match(SimpleString *bigS, UI4Bytes begin_pos, const UI4Bytes & len, 
	     Node* curr_node, UI4Bytes loc, 
	     pair<Node*, UI4Bytes> & p, UI4Bytes maxLen);

  UI4Bytes SPA(UI4Bytes beginSPA);
  void reach(Node* node, UI4Bytes index, UI4Bytes length, pair<Node*, int> & p, UI4Bytes & childIndex);
  void rule2case1(I_Node* parent, const UI4Bytes & actualStart);
  void rule2case2(I_Node* oldParent, Node* oldChild, const UI4Bytes & stopPos, 
		  const UI4Bytes & beginPos, const UI4Bytes & childIndex);
  
  void initialize();
};

SmallSuffixTree::SmallSuffixTree(string * s, const unsigned int & origSize)
{  
  cout << "Small Suffix Tree Constructor" << endl;
  origStrSize = origSize;
  cout << "origStrSize " << origStrSize << endl;
  s_ = s;
  rawMemory0_ = NULL;
  I_NodeArray_ = NULL;
  I_NodeCount_ = 0;
  
  rawMemory1_ = NULL;
  L_NodeArray_ = NULL;
  L_NodeCount_ = 0;
  
  UI4Bytes iMax = (UI4Bytes)(s_->size() * 0.8);
  cout << "Initializing the raw memory for " << iMax << " I_Nodes ..." << flush;
  rawMemory0_ = operator new[](sizeof(I_Node) * iMax);
  I_NodeArray_ = static_cast<I_Node*>(rawMemory0_);
  cout << "Finish." << endl;
  cout << "Size for " << iMax << " I_Nodes = " << sizeof(I_Node)*iMax/(1024*1024) << " MB.\n" << endl;
  
  UI4Bytes lMax = (UI4Bytes)(s_->size());
  cout << "Initializing the raw memory for " << lMax << " L_Nodes ..." << flush;
  rawMemory1_ = operator new[](sizeof(L_Node) * lMax);
  L_NodeArray_ = static_cast<L_Node*>(rawMemory1_);
  cout << "Finish." << endl;
  cout << "Size for " <<lMax << " L_Nodes = " << sizeof(L_Node)*lMax/(1024*1024) << " MB.\n" << endl;
  
  bufPos = 0;
  cout << "Building the small suffix Tree... " << endl;
  
  TimeTracker tt;
  tt.Start();
  
  initialize();
  currentEnd = 0; 
 
  UI4Bytes beginSPA = 0;
  
  while(currentEnd < s_->size())
    { 
      beginSPA = SPA(beginSPA); 
      currentEnd++;
    }
  
  cout << "lastCurrentEnd = " << currentEnd-1 << endl;
  currentEnd--; 

  if(lastInternalNodeAdded_ != NULL && lastInternalNodeAdded_->suffixLink == NULL)
    lastInternalNodeAdded_->suffixLink = root_;
  lastInternalNodeAdded_ = NULL; 
   
  double tstop = tt.Stop();

  cout << "Time Taken = " << tstop/60.0 << " minutes or " << tstop << " seconds." << endl;
  cout << "Number of internal nodes created for this tree = " << I_NodeCount_ << endl;
  cout << "Number of leaves created for this tree = " << L_NodeCount_ << endl;

  /*
  root_->getDepth();
  UI4Bytes iCount = 0, lCount = 0;
  for(UI4Bytes i = 0; i < I_NodeCount_;  ++i)
    if(I_NodeArray_[i].depth <= 50)
      ++iCount;
  cout << iCount << " nodes or " << 100.0 * (double)(iCount) / (double)I_NodeCount_ << "% are short" << endl;

  for(UI4Bytes i = 0; i < L_NodeCount_; ++i)
    if(L_NodeArray_[i].depth <= 50)
      ++lCount;
  cout << lCount << " nodes or " << 100.0 * (double)(lCount) / (double)L_NodeCount_ << "% are short" << endl;
  */
  
  cout << "Reset children" << endl;
  for(UI4Bytes i = 0; i < I_NodeCount_; ++i)
    for(UI4Bytes j = 0; j <= NUMCHAR; ++j)
      if(I_NodeArray_[i].children[j] != NULL && I_NodeArray_[i].children[j]->isLeaf)
	I_NodeArray_[i].children[j] = NULL;
  cout << "done" << endl;

  for(UI4Bytes i = 0; i < L_NodeCount_; ++i)
    L_NodeArray_[i].~L_Node();
  operator delete[](rawMemory1_);
  cout << "All L_Nodes in the small sfTree destroyed." << endl;
}
 
void 
SmallSuffixTree::initialize()
{
  root_ = new (&I_NodeArray_[I_NodeCount_++]) I_Node();
  lastInternalNodeAdded_ = NULL;
}

UI4Bytes SmallSuffixTree::SPA(UI4Bytes beginSPA)
{  
  I_Node *n_ptr = root_;
  
  Node* found_node_ptr = NULL;
  UI4Bytes length, length_of_B, begin_search_at;

  if(lastInternalNodeAdded_ != NULL && lastInternalNodeAdded_->suffixLink == NULL)
    lastInternalNodeAdded_->suffixLink = root_;
	
  lastInternalNodeAdded_ = NULL; 

  if(beginSPA != 0)
      beginSPA++;

  for(UI4Bytes i = beginSPA; i <= currentEnd; ++i)
    { 
      length = currentEnd - i + 1; 
      pair<Node*, int> p;
      p.second = 0;

      if(n_ptr == root_)
	{
	  begin_search_at = i;
	  length_of_B = length-1;
	}
      else 
	{
	  begin_search_at = i + n_ptr->getSuffixLength();
	  length_of_B = (length-1) - n_ptr->getSuffixLength();
	  
	  if(length_of_B < 0)
	    {
	      cerr << "Length of B " << length_of_B << " < 0 " << endl;
	      exit(0);
	    }
	}

      UI4Bytes childIndex = 0;

      if (n_ptr != root_ && length_of_B == 0)
	{
	  UI4Bytes i = (*s_)[begin_search_at]-48; 
	  if(n_ptr->children[i]) 
	    { 
	      p.first = n_ptr->children[i];
	      p.second = -3;
	    }
	  else
	    {
	      p.first = n_ptr;
	      p.second = -2;
	    }
	}
      else
	reach((Node*)n_ptr, begin_search_at, length_of_B, p, childIndex);      
	
      found_node_ptr = p.first;
            
      if(p.second == -2) 
	rule2case1(static_cast<I_Node*>(found_node_ptr), i); 
      else if (p.second >= 0) 
	rule2case2(static_cast<I_Node*>(found_node_ptr->parent), found_node_ptr, p.second, i, childIndex);
      else if(p.second == -3) 
	return i-1;
      else if(p.second != -1)
	{
	  cerr << "p.second " << p.second << " does not fit any cases above." <<  endl;
	  exit(1);
	}

      if(i != currentEnd-1)
	{
	  if(p.second == -1 && (static_cast<I_Node*>(found_node_ptr->parent))->suffixLink != NULL)
	    n_ptr = (static_cast<I_Node*>(found_node_ptr->parent))->suffixLink;
	  else if(p.second == -2 && (static_cast<I_Node*>(found_node_ptr))->suffixLink != NULL)
	    n_ptr = (static_cast<I_Node*>(found_node_ptr))->suffixLink;
	  else if(p.second >=0 && (static_cast<I_Node*>(found_node_ptr->parent->parent))->suffixLink != NULL)
	    n_ptr = (static_cast<I_Node*>(found_node_ptr->parent->parent))->suffixLink;
	} 
      else
	n_ptr = root_;

    }

  return currentEnd;
}

void
SmallSuffixTree::reach(Node* node, UI4Bytes index, UI4Bytes length, 
		  pair<Node*, int> & p, UI4Bytes & childIndex)
{  
  UI4Bytes edge_width, target_length_left = length;
  UI4Bytes siplus1 = (*s_)[index+length]-48;

  p.second += target_length_left;

  I_Node* iNode = static_cast<I_Node*>(node);
  UI4Bytes i = (*s_)[index] - 48;

  if(iNode->children[i] != NULL)
    {  
      if(index == currentEnd) 
	{
	  p.first = iNode->children[i];
	  p.second = -3;
	  
	  if(lastInternalNodeAdded_ != NULL && lastInternalNodeAdded_->suffixLink == NULL)
	    lastInternalNodeAdded_->suffixLink = iNode;
	  return;
	}
      
      edge_width = iNode->children[i]->getEdgeLength();
      
      if(iNode->children[i]->isLeaf)
	edge_width--; 
      
      if(edge_width == target_length_left) 
	{ 
	  if(iNode->children[i]->isLeaf) 
	    {
	      p.first = iNode->children[i];
	      p.second = -1;
	    }
	  else 
	    {
	      UI4Bytes j = siplus1;
	      if(!iNode->children[i]->isLeaf && 
		 static_cast<I_Node*>(iNode->children[i])->children[j] != NULL)
		{
		  p.first = iNode->children[i];
		  if(lastInternalNodeAdded_ != NULL && lastInternalNodeAdded_->suffixLink == NULL)
		    lastInternalNodeAdded_->suffixLink = static_cast<I_Node*>(p.first); 
		  p.second = -3;
		}
	      else
		{
		  p.first = iNode->children[i];
		  p.second = -2;
		}
	    }
	  return;
	}
      
      if(edge_width > target_length_left)
	{
	  UI4Bytes maybe_siplus1_pos = iNode->children[i]->start + target_length_left;
	  
	  if(char(siplus1+48) == (*s_)[maybe_siplus1_pos])
	    {
	      p.first = iNode->children[i];
	      p.second = -3;
	      return;
	    }
	  else
	    {
	      p.first = iNode->children[i];
	      p.second = maybe_siplus1_pos;
	      childIndex = i;
	      return;
	    }
	}
      else 
	{
	  target_length_left -= edge_width; 
	  index = index + edge_width; 
	  reach(iNode->children[i], index, target_length_left, p, childIndex);
	}
    }
  else
    {
      p.first = root_;
      p.second = -2;
    }
}

void SmallSuffixTree::rule2case1(I_Node *parent, const UI4Bytes & actualStart)
{      
  if(actualStart >= s_->size())
    return;

  L_Node* newLeaf = new (&L_NodeArray_[L_NodeCount_++]) L_Node();
  newLeaf->actualStart = actualStart;
  newLeaf->start = currentEnd;
  newLeaf->parent = parent;
  parent->addChild(newLeaf, (*s_)[currentEnd]-48);
  
  if(lastInternalNodeAdded_ != NULL && lastInternalNodeAdded_->suffixLink == NULL)
    lastInternalNodeAdded_->suffixLink = parent;
    
}

void SmallSuffixTree::rule2case2(I_Node* oldParent, Node *oldChild, const UI4Bytes & stopPos, 
			    const UI4Bytes & beginPos, const UI4Bytes & childIndex)
{    
  if(beginPos >= s_->size())
    return;

  oldParent->removeChild(childIndex); 
  
  I_Node* newParent = new (&I_NodeArray_[I_NodeCount_++]) I_Node();
  newParent->actualStart = oldChild->actualStart;
  newParent->start = oldChild->start;
  newParent->end = stopPos-1;
  newParent->parent = oldParent;
  oldParent->addChild(newParent, childIndex);

  L_Node* newChild = new (&L_NodeArray_[L_NodeCount_++]) L_Node();
  newChild->actualStart = beginPos;
  newChild->start = currentEnd;
  newChild->parent = newParent;
  newParent->addChild(newChild, (*s_)[newChild->start]-48);
 
  oldChild->start = stopPos;
  oldChild->parent = newParent;
  newParent->addChild(oldChild, (*s_)[oldChild->start]-48);
    
  if(lastInternalNodeAdded_ != NULL && lastInternalNodeAdded_->suffixLink == NULL)
    lastInternalNodeAdded_->suffixLink = newParent;

  lastInternalNodeAdded_ = newParent;
}
 
template <class T>
bool SmallSuffixTree::match(T *origS, const UI4Bytes & startLoc, 
			    const UI4Bytes & len, pair<Node*, UI4Bytes> & p)
{   
  if(root_->children[(*origS)[startLoc]-48] == NULL)
    return false;

  Node* n = root_->children[(*origS)[startLoc]-48];
  
  UI4Bytes length_matched = match(origS, startLoc, len, n, n->start-1, p);

  return length_matched == len;
}

bool SmallSuffixTree::match(SimpleString *origS, const UI4Bytes & startLoc, 
			    const UI4Bytes & len, pair<Node*, UI4Bytes> & p,
			    UI4Bytes & length_matched)
{  
  if(root_->children[(*origS)[startLoc]-48] == NULL)
    {
      length_matched = 0;
      return false;
    }

  Node* n = root_->children[(*origS)[startLoc]-48];
  length_matched = match(origS, startLoc, len, n, n->start-1, p, 30);
   
  return (length_matched == 30 || length_matched == len);
}

template <class T> 
UI4Bytes SmallSuffixTree::match(T *origS, UI4Bytes begin_pos, const UI4Bytes & len, 
				Node* curr_node, UI4Bytes loc, 
				pair<Node*, UI4Bytes> & p)
{
  UI4Bytes length_matched = 0;
  p.first = NULL;
  p.second = 0;
    
  if(curr_node->isLeaf)
    {
      if(loc == curr_node->getEnd()-1)
      {
	return 0;
      }
    }
  else
    {
      if(loc == curr_node->getEnd())
	{      
	  if(!curr_node->isLeaf && (static_cast<I_Node*>(curr_node))->children[(*origS)[begin_pos]-48] != NULL)
	    {   
	      curr_node = (static_cast<I_Node*>(curr_node))->children[(*origS)[begin_pos]-48];
	      loc = curr_node->start; 
	    }
	  else 
	    {
	      p.first = curr_node;
	      p.second = loc;
	      return 0;
	    }
	}
      else
	++loc;
    }

  while(length_matched != len)
    {
      for(UI4Bytes i = loc; i <= curr_node->getEnd(); ++i)
	{
	  if((*s_)[i] != (*origS)[begin_pos])
	    {
	      p.first = curr_node;
	      p.second = i-1;
	      return length_matched;
	    }
	  ++length_matched;
	  ++begin_pos;

	  if(length_matched == len)
	    {
	      p.first = curr_node;
	      p.second = i;
	      return length_matched;
	    }

	  if(begin_pos == origS->size())
	    {
	      p.first = curr_node;
	      p.second = i;
	      return length_matched;
	    }
	} 
      
      if(curr_node->isLeaf || (static_cast<I_Node*>(curr_node))->children[(*origS)[begin_pos]-48] == NULL)
	{ 
	  p.first = curr_node;
	  p.second = curr_node->getEnd();
	  return length_matched;
	}
      
      curr_node = (static_cast<I_Node*>(curr_node))->children[(*origS)[begin_pos]-48];
      loc = curr_node->start;
    }
  
  return length_matched;
}

UI4Bytes SmallSuffixTree::match(SimpleString *origS, UI4Bytes begin_pos, const UI4Bytes & len, 
				Node* curr_node, UI4Bytes loc, 
				pair<Node*, UI4Bytes> & p, UI4Bytes maxLen)
{
  UI4Bytes length_matched = 0;
  p.first = NULL;
  p.second = 0;
    
  if(curr_node->isLeaf)
    {
      if(loc == curr_node->getEnd()-1)
      {
	return 0;
      }
    }
  else
    {
      if(loc == curr_node->getEnd())
	{      
	  if(!curr_node->isLeaf && (static_cast<I_Node*>(curr_node))->children[(*origS)[begin_pos]-48] != NULL)
	    {   
	      curr_node = (static_cast<I_Node*>(curr_node))->children[(*origS)[begin_pos]-48];
	      loc = curr_node->start; 
	    }
	  else 
	    {
	      p.first = curr_node;
	      p.second = loc;
	      return 0;
	    }
	}
      else
	++loc;
    }

  while(length_matched != len)
    {
      for(UI4Bytes i = loc; i <= curr_node->getEnd(); ++i)
	{
	  if((*s_)[i] != (*origS)[begin_pos])
	    {
	      p.first = curr_node;
	      p.second = i-1;
	      return length_matched;
	    }
	  ++length_matched;
	  ++begin_pos;

	  if(length_matched == len || length_matched == maxLen)
	    {
	      p.first = curr_node;
	      p.second = i;
	      return length_matched;
	    }

	  if(begin_pos == origS->size())
	    {
	      p.first = curr_node;
	      p.second = i;
	      return length_matched;
	    }
	} 
      
      if(curr_node->isLeaf || (static_cast<I_Node*>(curr_node))->children[(*origS)[begin_pos]-48] == NULL)
	{ 
	  p.first = curr_node;
	  p.second = curr_node->getEnd();
	  return length_matched;
	}
      
      curr_node = (static_cast<I_Node*>(curr_node))->children[(*origS)[begin_pos]-48];
      loc = curr_node->start;
    }
  
  return length_matched;
}

} /* namespace libednai_trellis_plus */

#endif
