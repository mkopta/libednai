#ifndef PH3_BUF_H
#define PH3_BUF_H

#include <iostream>
#include <fstream>
#include <vector>
#include <iomanip>

namespace libednai_trellis_plus {

using namespace std;

class Ph3Buffer
{
 public:
  Ph3Buffer(char* fname, UI4Bytes p, UI4Bytes e)
    {  
      numLevel_ = 250;
      parSize_ = p;
      extraLen_ = e;
      SECSIZE = 1 * 1024;
      buf_ = NULL;
      printOn = false;
      s1 = new char[parSize_+extraLen_];
      s2_ = new char[numLevel_*parSize_];
      si = new char[parSize_+extraLen_];

      f_.open(fname);

      f_.read(s1, parSize_+extraLen_); 
      f_.seekg(parSize_);
      f_.read(s2_, numLevel_*parSize_);
      f_.clear();
      f_.seekg(0, ios::end);
      fileSize_ = f_.tellg();
      f_.seekg(0, ios::beg);
      cout << "Ph3Buffer loaded with parSize " << parSize_ << " and file size " << fileSize_ << " of file " << fname << endl;
      cout << "Total characters stored = " << (2*(parSize_ + extraLen)) + (numLevel_*parSize_) << endl;
    }

  void resetCounts() { m_ = 0; d_ = 0; }
  void showCounts()
    {
      cout << "Mem " << ((double)m_ * 100.0)/(double)(m_+d_) <<
	" Disk " << ((double)d_ * 100.0)/(double)(m_+d_) << endl;
    }

  void clearFVec()
    {
      fVec.clear();
      for(int i = 0; i < 951; ++i)
	fVec.push_back(0);
    }
  
  void showFVec()
    {
      UI4Bytes sum = 0;
      for(unsigned int i = 0; i < fVec.size(); ++i)
	sum += fVec[i];
      double sumP = 0;
      for(unsigned int i = 0; i < fVec.size(); ++i)
	{
	  double d = ((double)fVec[i]*100.0)/(double)sum; 
	  if(d > 0)
	    {
	      sumP += d;
	      
	      streamsize prec = cout.precision();
	      cout << setprecision(2) << d << ", " << flush;
	      if(i == 99)
		cout << "<----  ";
	      cout << setprecision(prec);
	    }
	}
      cout << endl;
      cout << "sum = " << sumP << endl;
    }

  void readPar(unsigned int i, streamoff startReadAt)
    {
      curPar_ = i;
      if(i == 0) return;

      if(i < numLevel_)
	{
	  UI4Bytes maxLevel = (fileSize_/parSize_)-1;
	  UI4Bytes leftOver = fileSize_%parSize_;
	  if(leftOver != 0)
	    ++maxLevel;

	  if(i == maxLevel)
	    { 
	      if(leftOver == 0)
		{
		  strncpy(si, &s2_[parSize_*(i-1)], parSize_);
		  si[parSize_] = '0';
		  si[parSize_+1] = '\0'; 
		  
		} 
	      else
		{
		  strncpy(si, &s2_[parSize_*(i-1)], leftOver);
		  si[leftOver] = '0';
		  si[leftOver+1] = '\0';
		}
	    }
	  else
	    strncpy(si, &s2_[parSize_*(i-1)], parSize_+extraLen_); 
  
	  return;
	}

      if(!f_.good())
	f_.clear();

      f_.seekg(startReadAt);
      
      if(startReadAt + parSize_ + extraLen_ < fileSize_)
	f_.read(si, parSize_+extraLen_);
      else
	{ 
	  f_.read(si, fileSize_ - startReadAt);
	  si[fileSize_-startReadAt] = '0';
	  si[fileSize_-startReadAt+1] = '\0';
	}
    }
  
  int operator[](UI4Bytes i)
    {
      if(i < parSize_ + extraLen_)
	{ 
	  return s1[i]-48;
	}

      if(i >= curPar_*parSize_ && i < ((curPar_+1)*parSize_) + extraLen_)
	{
	  return si[i - (curPar_*parSize_)]-48;
	}

      if(i < (numLevel_+1)*parSize_)
	{
	  return s2_[i-parSize_]-48;
	}

      if(buf_ && i >= bBuf_ && i <= eBuf_)
	{
	  return buf_[i-bBuf_]-48;
	}
 
      if(buf_)
	delete []buf_;
      buf_ = new char[SECSIZE];
      
      bBuf_ = i;
      eBuf_ = i+SECSIZE-1;
      
      f_.seekg(i);
      f_.read(buf_, SECSIZE);
      
      return buf_[0]-48;
    }

  ~Ph3Buffer()
    { 
      if(f_.is_open()) f_.close();
      if(s1) delete []s1;
      if(s2_) delete []s2_;
      if(si) delete []si;
      if(buf_) delete []buf_;
    }

 public:
  char *s1, *si;
  bool printOn;
  vector<UI4Bytes> fVec;

 private:
  UI4Bytes m_, d_;
  ifstream f_;
  UI4Bytes parSize_, extraLen_;
  UI4Bytes curPar_, SECSIZE;
  UI4Bytes fileSize_;
  char* buf_, *s2_;
  UI4Bytes bBuf_, eBuf_;
  unsigned int numLevel_;
};

} /* namespace libednai_trellis_plus */

#endif
