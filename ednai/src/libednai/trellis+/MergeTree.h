#ifndef MERGETREE_H
#define MERGETREE_H

#include <fstream>
#include <iostream>
#include <vector>
#include <string>
#include "BitString.h"
#include "LoadedNode.h"
#include "TimeTracker.h"
#include "Range.h"
#include "SmallSuffixTree.h"

namespace libednai_trellis_plus {
  
using namespace std;

typedef LoadedNode MNODE;
bool printOn = false;

int lessCount = 0, moreCount = 0, iCount = 0, lCount = 0, leafFound = 0, intFound = 0;
int foundRange = 0, fromLeaf = 0, newIFound = 0;
  
string smallS;

template <class STRING_T>
class MergeTree
{
 public:   
  MergeTree(STRING_T *s, SmallSuffixTree* sm):myStr(s), sst(sm)
    {
    }

  void mergeTreeRoots(I_LoadedNode *root1, I_LoadedNode *root2);
  ~MergeTree() {}

  UI4Bytes parSize;

 private:
  STRING_T *myStr; 
  SmallSuffixTree *sst;

  int charAt(UI4Bytes i, MNODE* n)
    {
      if(i==fileSize)
        return 0;

      if(n->isLeaf() && i > parSize)
	{
	  ++lCount;
	  L_LoadedNode * lNode = static_cast<L_LoadedNode*>(n);
	
	  if(!(lNode->xc).touched())
	    {
	      lNode->xc.init(myStr, lNode->s);
	      ++leafFound;
	      UI4Bytes tempIndex = i - lNode->s + lNode->xc.pos();
	      if(tempIndex >= 0 && tempIndex < 29)
		{
		  return (lNode->xc)[tempIndex] - 48;
		}
	    }
	  else if(!(lNode->xc).done())
	    {
	      ++leafFound;
	      UI4Bytes tempIndex = i - lNode->s + lNode->xc.pos();
	      if(tempIndex >= 0 && tempIndex < 29)
		{
		  return (lNode->xc)[tempIndex] - 48;
		}
	    }
	}
      return (*myStr)[i]-48;
    }

  void check(MNODE *n);
  void cleanUpChildren(I_LoadedNode *node, const UI4Bytes & seqNo);
  void mergeEdge(MNODE *node1, MNODE *node2, const int & indexC);

  int getIndex(I_LoadedNode*parent, LoadedNode *node)
    {
      for(UI4Bytes i = NUMCHAR+1; i--; )  
	if(parent->children[i] && ((&(*(parent->children[i]))) == (&(*node))))
	  return i;
      return -1;
    }
};

template <class STRING_T>
void 
MergeTree<STRING_T>::check(MNODE *n)
{
  assert(n->s <= n->e);
}

template <class STRING_T>
void 
MergeTree<STRING_T>::cleanUpChildren(I_LoadedNode *node, const UI4Bytes & seqNo)
{  
  if(node->children[0] == NULL)
    return;

  int index = getIndex(node, node->children[0]); 

  if(index != 0) 
    {  
      if(node->children[index] != NULL)
	mergeEdge(node->children[index], node->children[0], index);
      else 
	{
	  node->children[index] = node->children[0];
	  node->children[0] = NULL;
	}
    }
}

template <class STRING_T>
void 
MergeTree<STRING_T>::mergeEdge(MNODE *node1, MNODE *node2, const int & indexC)
{ 
  smallS = (char)(indexC+48);

  UI4Bytes i = node1->s;
  UI4Bytes j = node2->s;
  UI4Bytes iEnd = node1->e;
  UI4Bytes jEnd = node2->e;

  while(true)
    {
      if(i == iEnd && j == jEnd)
	{   
	  if(j < i)
	    {
	      node1->s = node2->s;
	      node1->e = node2->e;
	    }
	  
	  I_LoadedNode *node1T = static_cast<I_LoadedNode*>(node1);
	  I_LoadedNode *node2T = static_cast<I_LoadedNode*>(node2);
	  
	  cleanUpChildren(node1T, 0);
	  cleanUpChildren(node2T, 1);
	  
	  for(UI4Bytes c = NUMCHAR+1; c--; )  
	    {   
	      if(node2T->children[c] != NULL)
		{
		  if(node1T->children[c] == NULL)
		    {
		      node1T->children[c] = node2T->children[c];
		      node1T->children[c]->p = node1T; 
		      node2T->children[c] = NULL;
		    }
		  else
		    mergeEdge(node1T->children[c], node2T->children[c], c);
		}
	    }
		  
	  ++invalid;
	  I_LoadedNodeArray[((intptr_t)node2-((intptr_t)&I_LoadedNodeArray[0]))/sizeof(I_LoadedNode)].~I_LoadedNode();
	  valid[((intptr_t)node2-((intptr_t)&I_LoadedNodeArray[0]))/sizeof(I_LoadedNode)] = false;
	  break;
	}

      else if(i < iEnd && j == jEnd)
	{    

	  ++nMergeCreated;
	  
	  I_LoadedNode *n;

	  if(I_LoadedNodeCount == I_LoadedNodeMax)
            {
              cout << "I_LoadedNodeCount " << I_LoadedNodeCount << " == I_LoadedNodeMax "
                   << I_LoadedNodeMax << endl;
              exit(0);
            }

	  if( j < i)
	    n = new (&I_LoadedNodeArray[I_LoadedNodeCount++]) I_LoadedNode(node2->s, j); 
	  else
	    n = new (&I_LoadedNodeArray[I_LoadedNodeCount++]) I_LoadedNode(node1->s, i); 

	  if(n->e > 2000000)
	    {
	      pair<Node*, UI4Bytes> p;
	      UI4Bytes el = n->e - n->s + 1;
	      if(sst->match(&smallS, 0, el, p))
		{
		  if(!p.first->isLeaf)
		    {
		      n->s = p.first->actualStart;
		      n->e = n->s + el - 1;
		    }
		}
	    }

	  I_LoadedNode *parent = static_cast<I_LoadedNode*> (node1->p);
	  int tempindex = getIndex(parent, node1);
	  parent->children[tempindex] = NULL;
	  parent->addChild(n, tempindex);
	  n->p = parent;

	  tempindex = charAt(i+1, node1); 
	  node1->s = i+1; 
	  n->addChild(node1, tempindex);
	  node1->p = n;

	  if(node1->isLeaf())
	    {
	      L_LoadedNode *tempLeaf = static_cast<L_LoadedNode*>(node1);
	      tempLeaf->xc.advancePos(j - node2->s + 1); 
	      }

	  I_LoadedNode* node2T = static_cast<I_LoadedNode*> (node2);
	  cleanUpChildren(node2T, 1);

	  for(UI4Bytes c = NUMCHAR+1; c--; )  	  
	    {  
	      if(node2T->children[c] != NULL) 
		{ 
		  if(n->children[c] == NULL) 
		    {
		      n->children[c] = node2T->children[c];
		      n->children[c]->p = n;
		      node2T->children[c] = NULL;
		    }
		  else  
		    mergeEdge(n->children[c], node2T->children[c], c);
		}
	    }
	  
	  if(!node1->isLeaf())
	    {
	      I_LoadedNode *node1T = static_cast<I_LoadedNode*>(node1);
	      if(node1T->getNumChildren() == 1)
		{

		  MNODE* child = NULL;
		  for(UI4Bytes i = NUMCHAR+1; i--; )  
		    if(node1T->children[i] != NULL)
		      {
			child = node1T->children[i];
			break;
		      }
		  
		  child->s = node1T->s;
		  I_LoadedNode *parent = static_cast<I_LoadedNode*>(node1T->p);
		  
		  parent->children[tempindex] = NULL;
		  parent->addChild(child, tempindex);
		  child->p = parent;
		  
		  ++invalid;
		  I_LoadedNodeArray[((intptr_t)node1-((intptr_t)&I_LoadedNodeArray[0]))/sizeof(I_LoadedNode)].~I_LoadedNode();
		  valid[((intptr_t)node1-((intptr_t)&I_LoadedNodeArray[0]))/sizeof(I_LoadedNode)] = false;
		}
	    }
	  
	  ++invalid;
	  I_LoadedNodeArray[((intptr_t)node2-((intptr_t)&I_LoadedNodeArray[0]))/sizeof(I_LoadedNode)].~I_LoadedNode();
	  valid[((intptr_t)node2-((intptr_t)&I_LoadedNodeArray[0]))/sizeof(I_LoadedNode)] = false;
	 
	  break;
	}
      else if(i == iEnd && j < jEnd)
	{    
	  if(j < i)
	    {
	      node1->s = node2->s;
	      node1->e = j;
	    }
	  if(node2->isLeaf())
	    {
	      L_LoadedNode* tempLeaf = static_cast<L_LoadedNode*>(node2);
	      tempLeaf->xc.advancePos(j - node2->s + 1);
	    } 

	  node2->p = NULL;
	  node2->s = j+1; 
	  
	  int index = charAt(j+1, node2);
		  
	  I_LoadedNode *node1T = static_cast<I_LoadedNode*>(node1);
	  cleanUpChildren(node1T, 1);
	  	  	   
	  if(node1T->children[index] != NULL)
	    mergeEdge(node1T->children[index], node2, index);
	  else
	    {
	      node2->p = node1T;
	      node1T->addChild(node2, index);
	    }

	  UI4Bytes childIndex = 0;
	  if(node1T->hasOneChild(childIndex))
	    {
	      MNODE* child = node1T->children[childIndex];
	  
	      child->s = node1T->s;
	      I_LoadedNode *parent = static_cast<I_LoadedNode*>(node1T->p);
		    
	      int tempindex = getIndex(parent, node1T);
	      
	      parent->children[tempindex] = NULL;
	      parent->addChild(child, tempindex);
	      child->p = parent;
	      
	      ++invalid;
	      I_LoadedNodeArray[((intptr_t)node1-((intptr_t)&I_LoadedNodeArray[0]))/sizeof(I_LoadedNode)].~I_LoadedNode();
	      valid[((intptr_t)node1-((intptr_t)&I_LoadedNodeArray[0]))/sizeof(I_LoadedNode)] = false;
	    }  
	  
	  break;
	}

      ++i;
      ++j;

      int charI = charAt(i, node1);
      int charJ = charAt(j, node2);
      
      if(charI == charJ)
	smallS += (char)(charI+48);
      
      if(charI != charJ)
	{
	  ++nMergeCreated;
	  I_LoadedNode *n;

	  if(I_LoadedNodeCount == I_LoadedNodeMax)
            {
              cout << "I_LoadedNodeCount " << I_LoadedNodeCount << " == I_LoadedNodeMax "
                   << I_LoadedNodeMax << endl;
              exit(0);
            }

	  if( j < i)
	    n = new (&I_LoadedNodeArray[I_LoadedNodeCount++]) I_LoadedNode(node2->s, j-1);
	  else
	    n = new (&I_LoadedNodeArray[I_LoadedNodeCount++]) I_LoadedNode(node1->s, i-1); 

	  if(n->e > 2000000)
	    {
	      pair<Node*, UI4Bytes> p;
	      UI4Bytes el = n->e - n->s + 1;

	      if(sst->match(&smallS, 0, el, p))
		{
		  if(!p.first->isLeaf)
		    {
		      n->s = p.first->actualStart;
		      n->e = n->s + el - 1;
		    }
		}
	    }

	  I_LoadedNode *parent = static_cast<I_LoadedNode*> (node1->p);
	  UI4Bytes tempindex = getIndex(parent, node1);
	  parent->children[tempindex] = n;
	  
	  if(node1->isLeaf())
	    {
	      L_LoadedNode *tempLeaf = static_cast<L_LoadedNode*>(node1);
	      tempLeaf->xc.advancePos(i - node1->s);
	    }

	  if(node2->isLeaf())
	    {
	      L_LoadedNode *tempLeaf = static_cast<L_LoadedNode*>(node2);
	      tempLeaf->xc.advancePos(i-node1->s); 
	    }

	  node1->s = i;
	  node1->p = n;
	  node2->s = j;
	  node2->p = n;
	  n->p = parent;
	  n->addChild(node1, charI);
	  n->addChild(node2, charJ);
	  
	  break;
	}
    }
}
 
template <class STRING_T>
void  
MergeTree<STRING_T>::mergeTreeRoots(I_LoadedNode *root1, I_LoadedNode *root2)
{ 
  assert(root1 != NULL);
  assert(root2 != NULL);

  for(UI4Bytes i = NUMCHAR+1; i--; )  
    {
      if(root2->children[i] != NULL)
	{
	  if(root1->children[i] == NULL)
	    {
	      root2->children[i]->p = root1;
	      root1->children[i] = root2->children[i];
	      root2->children[i] = NULL; 
	    }
	  else
	    mergeEdge(root1->children[i], root2->children[i], i);
	}
    }

  ++invalid;
  I_LoadedNodeArray[((intptr_t)root2-((intptr_t)&I_LoadedNodeArray[0]))/sizeof(I_LoadedNode)].~I_LoadedNode();
  valid[((intptr_t)root2-((intptr_t)&I_LoadedNodeArray[0]))/sizeof(I_LoadedNode)] = false;
}

} /* namespace libednai_trellis_plus */

#endif
