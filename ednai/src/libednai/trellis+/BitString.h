#ifndef BIT_STR_H
#define BIT_STR_H

#include <iostream>
#include <string>
#include <fstream>

namespace libednai_trellis_plus {
using namespace std;

typedef unsigned int UI4Bytes;

class BitString
{
public:
  BitString(){}
  
  BitString(const char* fname){ init(fname);}

  void init (const char* fname, unsigned int max = 0)
  {
    f.open(fname);
    f.seekg(0, ios::end);
    fileSize = f.tellg();
    cout << "fileSize = " << fileSize << endl;
    f.seekg(0);

    if(max > 0)
      {
       	fileSize = max;
	cout << "Setting fileSize to " << fileSize << " and " << fileSize/16 << endl;
      }

    arraySize = ((fileSize%16 == 0) ? fileSize/16 : (fileSize/16)+1);
    cout << "Init bitArray with size = " << arraySize << "Bytes" << endl;
    bitArray = new UI4Bytes[arraySize];
    for(int j = 16; j--;)
      {
	shiftAmt[j] = 30-(j<<1);
      }
    
    scan();
    f.close();
  }
  
  
  ~BitString()
  {
    delete [] bitArray;
  }

  char operator[] (const UI4Bytes & i) const
  {
    return (char)(((bitArray[i/16] >> shiftAmt[i%16]) & 3) + 49);
  }

  UI4Bytes size() const { return fileSize; }

  string substr(const UI4Bytes & i, const UI4Bytes & len)
    {
      string s;
      for(UI4Bytes j = i; j < i+len; ++j)
	s += (*this)[j];
      return s;
    } 

private:
  UI4Bytes *bitArray, shiftAmt[16];
  UI4Bytes arraySize, fileSize;
  ifstream f;

  void scan()
  {
    cout << "start scanning" << endl;
    UI4Bytes i = 0, ci = 0, k=0, endI=0;
    char c;
    
    while(i < fileSize)
      {
	endI = i+16;
	for(; i < endI; ++i)
	  {
	    f.get(c); 

	    ci = ci | (UI4Bytes)((c-49) << shiftAmt[i%16]); 
	    if(i >= fileSize)
	      break;
	  }
	bitArray[k++] = ci;
	ci = 0;
      }
  }
};

} /* namespace libednai_trellis_plus */
#endif
