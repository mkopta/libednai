#ifndef STREE_H
#define STREE_H

#include <vector>
#include <fstream>
#include "Node.h"
#include "TimeTracker.h"
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include "SmallSuffixTree.h"
#include "Range.h"

namespace libednai_trellis_plus {

#define KBYTE 1024
#define BUFSIZE KBYTE 
#define SHORT_SIZE sizeof(short)
#define CHAR_SIZE sizeof(char)
#define NODE_SIZE (SHORT_SIZE + CHAR_SIZE + 3*UI4SIZE)

UI4Bytes iFound = 0, lFound = 0;

class SuffixTree
{
 public:

  SuffixTree(){} 
  SuffixTree(bool lastTree);
  ~SuffixTree(){} 
 
  I_Node* getRoot() { return root_; }  
  void setRoot(I_Node* r) { root_ = r; }
    
  void storeSubTrees(UI4Bytes parNo, SmallSuffixTree & sst);
  

  bool match(PREF& q, pair<Node*, UI4Bytes> & p);
    
 private: 
  I_Node *root_, *lastInternalNodeAdded_, *dummyInternalNode_;
  vector<Range> rangeVec;
  
  UI4Bytes match(PREF* s_ptr, Node* n, UI4Bytes loc, UI4Bytes begin_pos, pair<Node*, UI4Bytes> & p);
  UI4Bytes SPA(UI4Bytes beginSPA);
  void reach(Node* node, UI4Bytes index, UI4Bytes length, pair<Node*, int> & p, UI4Bytes & childIndex);
  void rule2case1(I_Node* parent, const UI4Bytes & actualStart);
  void rule2case2(I_Node* oldParent, Node* oldChild, const UI4Bytes & stopPos, 
		  const UI4Bytes & beginPos, const UI4Bytes & childIndex);
  void printTreeToFile(fstream & outFile, Node* node, short t, char i, SmallSuffixTree & sst, const UI4Bytes & pc);

  void initialize(I_Node* dummy = NULL);

};

////////////////////////////////////////////////////////////////////////////////////////

SuffixTree::SuffixTree(bool lastTree)
{  
  dummyInternalNode_ = new (&I_NodeArray[I_NodeCount++]) I_Node();

  bufPos = 0;
  cout << "Building suffix Tree " << endl;
  if(lastTree) 
    cout << "This is the last tree" << endl;
  
  TimeTracker tt;
  tt.Start();
  
  initialize();
  currentEnd = 0; 
 
  UI4Bytes beginSPA = 0;
  
    if(!lastTree)
    {
      while(L_NodeCount < candT1)
	{
	  beginSPA = SPA(beginSPA); 
	  currentEnd++;
	} 
    }
  else
    {
      while(currentEnd < s->size())
	{ 
	  beginSPA = SPA(beginSPA); 
	  currentEnd++;
	}
    }

    currentEnd--; 

  if(!lastTree && root_->children[0] != NULL)
    {
      root_->children[0] = NULL;
      --L_NodeCount;
    }

  if(lastInternalNodeAdded_ != NULL && lastInternalNodeAdded_->suffixLink == NULL)
    lastInternalNodeAdded_->suffixLink = root_;
  lastInternalNodeAdded_ = NULL; 
   
  double tstop = tt.Stop();

  cout << "Time Taken = " << tstop/60.0 << " minutes or " << tstop << " seconds." << endl;
}
 
void
SuffixTree::initialize(I_Node* dummy)
{
  if(dummy == NULL)
    root_ = new (&I_NodeArray[I_NodeCount++]) I_Node();
  else
    root_ = dummy;

  lastInternalNodeAdded_ = NULL;
}

UI4Bytes SuffixTree::SPA(UI4Bytes beginSPA)
{  
  I_Node *n_ptr = root_;
  
  Node* found_node_ptr = NULL;
  UI4Bytes length, length_of_B, begin_search_at;

  if(lastInternalNodeAdded_ != NULL && lastInternalNodeAdded_->suffixLink == NULL)
    lastInternalNodeAdded_->suffixLink = root_;
	
  lastInternalNodeAdded_ = NULL; 
  
  if(beginSPA != 0)
      beginSPA++;

  for(UI4Bytes i = beginSPA; i <= currentEnd; ++i)
    { 
      length = currentEnd - i + 1; 
      pair<Node*, int> p;
      p.second = 0;

      if(n_ptr == root_)
	{
	  begin_search_at = i;
	  length_of_B = length-1;
	}
      else 
	{
	  begin_search_at = i + n_ptr->getSuffixLength();
	  length_of_B = (length-1) - n_ptr->getSuffixLength();
	  
	  if(length_of_B < 0)
	    {
	      cerr << "Length of B " << length_of_B << " < 0 " << endl;
	      exit(0);
	    }
	}

      UI4Bytes childIndex = 0;

      if (n_ptr != root_ && length_of_B == 0)
	{
	  UI4Bytes i = (*s)[begin_search_at]-48; 
	  if(n_ptr->children[i]) 
	    { 
	      p.first = n_ptr->children[i];
	      p.second = -3;
	    }
	  else
	    {
	      p.first = n_ptr;
	      p.second = -2;
	    }
	}
      else
	reach((Node*)n_ptr, begin_search_at, length_of_B, p, childIndex);      
	
      found_node_ptr = p.first;
            
      if(p.second == -2) 
	rule2case1(static_cast<I_Node*>(found_node_ptr), i); 
      else if (p.second >= 0) 
	rule2case2(static_cast<I_Node*>(found_node_ptr->parent), found_node_ptr, p.second, i, childIndex);
      else if(p.second == -3) 
	return i-1;
      else if(p.second != -1)
	{
	  cerr << "p.second " << p.second << " does not fit any cases above." <<  endl;
	  exit(1);
	}

      if(i != currentEnd-1)
	{
	  if(p.second == -1 && (static_cast<I_Node*>(found_node_ptr->parent))->suffixLink != NULL)
	    n_ptr = (static_cast<I_Node*>(found_node_ptr->parent))->suffixLink;
	  else if(p.second == -2 && (static_cast<I_Node*>(found_node_ptr))->suffixLink != NULL)
	    n_ptr = (static_cast<I_Node*>(found_node_ptr))->suffixLink;
	  else if(p.second >=0 && (static_cast<I_Node*>(found_node_ptr->parent->parent))->suffixLink != NULL)
	    n_ptr = (static_cast<I_Node*>(found_node_ptr->parent->parent))->suffixLink;
	} 
      else
	n_ptr = root_;

    } 
  return currentEnd;
}

void
SuffixTree::reach(Node* node, UI4Bytes index, UI4Bytes length, 
		  pair<Node*, int> & p, UI4Bytes & childIndex)
{  
  UI4Bytes edge_width, target_length_left = length;
  UI4Bytes siplus1 = (*s)[index+length]-48;

  p.second += target_length_left;

  I_Node* iNode = static_cast<I_Node*>(node);
  UI4Bytes i = (*s)[index] - 48;

  if(iNode->children[i] != NULL)
    {  
      if(index == currentEnd)
	{
	  p.first = iNode->children[i];
	  p.second = -3;
	  
	  if(lastInternalNodeAdded_ != NULL && lastInternalNodeAdded_->suffixLink == NULL)
	    lastInternalNodeAdded_->suffixLink = iNode;
	  return;
	}
      
      edge_width = iNode->children[i]->getEdgeLength();
      
      if(iNode->children[i]->isLeaf)
	edge_width--; 
      
      if(edge_width == target_length_left) 
	{ 
	  if(iNode->children[i]->isLeaf) 
	    {
	      p.first = iNode->children[i];
	      p.second = -1;
	    }
	  else 
	    {
	      UI4Bytes j = siplus1;
	      if(!iNode->children[i]->isLeaf && 
		 static_cast<I_Node*>(iNode->children[i])->children[j] != NULL)
		{
		  p.first = iNode->children[i];
		  if(lastInternalNodeAdded_ != NULL && lastInternalNodeAdded_->suffixLink == NULL)
		    lastInternalNodeAdded_->suffixLink = static_cast<I_Node*>(p.first); 
		  p.second = -3;
		}
	      else
		{
		  p.first = iNode->children[i];
		  p.second = -2;
		}
	    }
	  return;
	}
      
      if(edge_width > target_length_left)
	{
	  UI4Bytes maybe_siplus1_pos = iNode->children[i]->start + target_length_left;
	  
	  if(char(siplus1+48) == (*s)[maybe_siplus1_pos])
	    {
	      p.first = iNode->children[i];
	      p.second = -3;
	      return;
	    }
	  else
	    {
	      p.first = iNode->children[i];
	      p.second = maybe_siplus1_pos;
	      childIndex = i;
	      return;
	    }
	}
      else 
	{
	  target_length_left -= edge_width; 
	  index = index + edge_width; 
	  reach(iNode->children[i], index, target_length_left, p, childIndex);
	}
    }
  else
    {
      p.first = root_;
      p.second = -2;
    }
}

void SuffixTree::rule2case1(I_Node *parent, const UI4Bytes & actualStart)
{      
  if(actualStart >= candT1)
    return;

  L_Node* newLeaf = new (&L_NodeArray[L_NodeCount++]) L_Node();
  newLeaf->actualStart = actualStart;
  newLeaf->start = currentEnd;
  newLeaf->parent = parent;
  parent->addChild(newLeaf, (*s)[currentEnd]-48);
  
  if(lastInternalNodeAdded_ != NULL && lastInternalNodeAdded_->suffixLink == NULL)
    lastInternalNodeAdded_->suffixLink = parent;

}
void SuffixTree::rule2case2(I_Node* oldParent, Node *oldChild, const UI4Bytes & stopPos, 
			    const UI4Bytes & beginPos, const UI4Bytes & childIndex)
{    
  if(beginPos >= candT1)
    return;

  oldParent->removeChild(childIndex); 
  
  I_Node* newParent = new (&I_NodeArray[I_NodeCount++]) I_Node();
  newParent->actualStart = oldChild->actualStart;
  newParent->start = oldChild->start;
  newParent->end = stopPos-1;
  newParent->parent = oldParent;
  oldParent->addChild(newParent, childIndex);

  L_Node* newChild = new (&L_NodeArray[L_NodeCount++]) L_Node();
  newChild->actualStart = beginPos;
  newChild->start = currentEnd;
  newChild->parent = newParent;
  newParent->addChild(newChild, (*s)[newChild->start]-48);
 
  oldChild->start = stopPos;
  oldChild->parent = newParent;
  newParent->addChild(oldChild, (*s)[oldChild->start]-48);
    
  if(lastInternalNodeAdded_ != NULL && lastInternalNodeAdded_->suffixLink == NULL)
    lastInternalNodeAdded_->suffixLink = newParent;

  lastInternalNodeAdded_ = newParent;
  
}
 

bool SuffixTree::match(PREF& q, pair<Node*, UI4Bytes> & p)
{  
  if(root_->children[q[0]-48] == NULL)
    return false;

  Node* n = root_->children[q[0]-48];
  
  UI4Bytes length_matched = match(&q, n, n->start-1, 0, p);
 
  return length_matched == q.size();
}

UI4Bytes SuffixTree::match(PREF* s_ptr, Node* curr_node, UI4Bytes loc, UI4Bytes begin_pos, 
			   pair<Node*, UI4Bytes> & p)
{
  UI4Bytes suffix_length = s_ptr->size() - begin_pos;
  UI4Bytes length_matched = 0;
  p.first = NULL;
  p.second = 0;

  if(curr_node->isLeaf)
    {
      if(loc == curr_node->getEnd()-1)
      {
	return 0;
      }
    }
  else
    {
      if(loc == curr_node->getEnd())
	{      
	  if(!curr_node->isLeaf && (static_cast<I_Node*>(curr_node))->children[(*s_ptr)[begin_pos]-48] != NULL)
	    {   
	      curr_node = (static_cast<I_Node*>(curr_node))->children[(*s_ptr)[begin_pos]-48];
	      loc = curr_node->start; 
	    }
	  else
	    {
	      p.first = curr_node;
	      p.second = loc;
	      return 0;
	    }
	}
      else
	++loc;
    }

  while(length_matched != suffix_length)
    {
      for(UI4Bytes i = loc; i <= curr_node->getEnd(); ++i)
	{  
	  if((*s)[i] != (*s_ptr)[begin_pos])
	    {
	      p.first = curr_node;
	      p.second = i-1;
	      return length_matched;
	    }
	  ++length_matched;
	  ++begin_pos;

	  if(begin_pos == s_ptr->size())
	    {
	      p.first = curr_node;
	      p.second = i;
	      return length_matched;
	    }
	} 
      
      if(curr_node->isLeaf || (static_cast<I_Node*>(curr_node))->children[(*s_ptr)[begin_pos]-48] == NULL)
	{ 
	  p.first = curr_node;
	  p.second = curr_node->getEnd();
	  return length_matched;
	}
      
      curr_node = (static_cast<I_Node*>(curr_node))->children[(*s_ptr)[begin_pos]-48];
      loc = curr_node->start;
    }

  cout << "length_matched = " << length_matched << " suffix_length = " << suffix_length << endl;
  cerr << "Match: Shouldn't be here!!!" << endl;
  exit(1);

  return length_matched;
}

void SuffixTree::storeSubTrees(UI4Bytes parNo, SmallSuffixTree & sst)
{  
  rangeVec.clear();
  
  short x = ENDMARKER;
  
  cout << "storeSubTrees for partition # " << parNo << endl;
  TimeTracker tt;
  tt.Start();
  double tStop;
  
  UI4Bytes pc = 0;
  for(vector<PREF>::iterator prefix = prefVec.begin(); prefix != prefVec.end(); ++prefix)
    { 
      pair<Node*, UI4Bytes> p;
      bool hasMatch = match(*prefix, p);
      if(!hasMatch)
	{
	  ++pc;
	  continue;
	}
      
      I_Node* foundNode = static_cast<I_Node*>(p.first);

      SuffixTree tempTree;
      dummyInternalNode_->start = 0;
      dummyInternalNode_->end = 0;
      for(UI4Bytes i = NUMCHAR+1; i--;)
        dummyInternalNode_->children[i] = NULL;

      tempTree.initialize(dummyInternalNode_);

      (static_cast<I_Node*>(foundNode->parent))->removeChild((*s)[foundNode->start]-48); 
      
      if(p.second < foundNode->getEnd())
        {
	  tempTree.root_->start = 0;
	  tempTree.root_->end = 0;
	  foundNode->start = p.second+1;
	  tempTree.root_->addChild(foundNode, (*s)[foundNode->start]-48);
          foundNode->parent = tempTree.root_;
       } 
      else 
        {
	  foundNode->parent = NULL;
          foundNode->start = 0;
	  foundNode->end = 0;
	  tempTree.root_ = foundNode;
        }
      
      bufPos=0;
      printTreeToFile(tfArray[pc], tempTree.root_, 0, '7', sst, pc);
      
      if(bufPos + SHORT_SIZE >= maxBufSize)
	{
	  tfArray[pc].write(charBuf, bufPos);
	  bufPos = 0;
	}
      memcpy(charBuf+bufPos, &x, SHORT_SIZE); 
      bufPos += SHORT_SIZE;

      gTimer.Start();
      tfArray[pc].write(charBuf, bufPos);
      diskTimeW += gTimer.Stop();

      ++pc;
    }
  
  tStop = tt.Stop();
  cout << "Time taken for storeSubTrees = " << tStop/60.0 << " minutes or " << tStop << " seconds." << endl;

  sort(rangeVec.begin(), rangeVec.end(), RangeLess());
  
  vector<Range> newRangeVec = collapseRange(rangeVec);
  bigRangeVec.insert(bigRangeVec.end(), newRangeVec.begin(), newRangeVec.end());
  
}
 
void SuffixTree::printTreeToFile(fstream & outFile, Node* node, short t, char i, 
				 SmallSuffixTree & sst, const UI4Bytes & pc)
{   
  UI4Bytes start = node->start;
  UI4Bytes end = node->getEnd();
  UI4Bytes actualStart = 0;

  UI4Bytes edgeLength = node->getEdgeLength();
  if(!node->isLeaf)
    {
      pair<Node*, UI4Bytes> p;
      if(sst.match(s, node->start, edgeLength, p)) 
	{ 
	  if(!p.first->isLeaf)
	    {
	      ++iFound;
	      start = p.first->actualStart;
	      end = start + edgeLength - 1;
	      actualStart = offset + node->actualStart;
	    }
	}
      else
	{ 
	  start += offset;
	  actualStart = offset + node->actualStart;
	  if(!node->isLeaf)
	    end += offset;
	  
	  if(end > candT1) 
	    {
	      Range newRange; newRange.beg = start; newRange.end = end; 
	      rangeVec.push_back(newRange);
	    }
	}
    }
  
  else 
    {
      start += offset;
      actualStart = offset + node->actualStart;
      if(!node->isLeaf)
	end += offset;
    }

  UI4Bytes len = (i == '7') ? 0 : end - start + 1;
  if(bufPos + NODE_SIZE >= maxBufSize)
    {
      tfArray[pc].write(charBuf, bufPos);
      bufPos = 0;
    }

  memcpy(charBuf+bufPos, &t, SHORT_SIZE);
  bufPos += SHORT_SIZE;
  memcpy(charBuf+bufPos, &i, CHAR_SIZE);
  bufPos += CHAR_SIZE;
  memcpy(charBuf+bufPos, &start, UI4SIZE);
  bufPos += UI4SIZE;
  memcpy(charBuf+bufPos, &len, UI4SIZE);
  bufPos += UI4SIZE;
  memcpy(charBuf+bufPos, &actualStart, UI4SIZE);
  bufPos += UI4SIZE;

  if(!node->isLeaf)
    {
      I_Node* iNode = static_cast<I_Node*>(node);
      for(UI4Bytes i = NUMCHAR+1; i--;)
	if(iNode->children[i] != NULL)
	  printTreeToFile(outFile, iNode->children[i], t+1, (char)(i+48), sst, pc);
    }
}

} /* namespace libednai_trellis_plus */

#endif
