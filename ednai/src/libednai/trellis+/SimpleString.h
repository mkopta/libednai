#ifndef SIMPLE_STR_H
#define SIMPLE_STR_H

#include <iostream>
#include <string>
#include <cstring>
#include <cstdlib>

namespace libednai_trellis_plus {

typedef unsigned int UI4Bytes;

using namespace std;

class SimpleString
{
 public:
  SimpleString(char *ca, UI4Bytes extraLen)
    { 
      own = true;
      UI4Bytes lenCa = strlen(ca);
      cStr = new char[lenCa+extraLen];
      strcpy(cStr, ca);
      cStr[lenCa] = '\0';
      sSize = lenCa; 
    }

  SimpleString(char *ca)
    {
      own = false;
      cStr = ca;
      sSize = strlen(cStr);
    }

  void removeEndChar() 
    {  
      cStr[sSize-1] = '\0';
            
      sSize = strlen(cStr); 
    }
  
  UI4Bytes size() const { return sSize; }

  void concat(char *cs)
    {
      strcat(cStr, cs);
      sSize = strlen(cStr);
      
    }
  
  char operator[](int i) const 
    {
      return cStr[i];
    }
   
  const char* c_str() const { return cStr; }
  
  int toInt(UI4Bytes i) const 
    { 
      return (int)cStr[i]-48;
    }
  
  string substr(UI4Bytes b, UI4Bytes len) const
    {
      if(b+len > strlen(cStr))
	{
	  cerr << "Substring index out of bound. " << b+len << " > " << strlen(cStr) << endl;
	  exit(0);
	}
      
      string s;
      for(UI4Bytes i = b; i < b+len; ++i)
	s += cStr[i];
      return s;
    }

  friend ostream & operator<< (ostream &os, SimpleString & ss)
    {
      os << ss.cStr;
      return os;
    }
  
  ~SimpleString() 
    { 
      if(own)
	delete [] cStr; 
    }

 private:
  char* cStr;
  UI4Bytes sSize;
  bool own;
};

} /* namespace libednai_trellis_plus */

#endif
