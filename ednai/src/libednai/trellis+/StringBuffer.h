#ifndef STR_BUF_H
#define STR_BUF_H

#include <fstream>
#include <string>
#include <iostream>
#include "Range.h"
#include <vector>

namespace libednai_trellis_plus {
using namespace std;

#define SMALL_BUF_SIZE 1024 

class RangeString
{
 public:
  RangeString(const int & b, const int & e):beg(b), end(e){}
  
  RangeString(const int & b, const int & e, char* s)
    {
      beg = b;
      end = e;
      this->s = s;
    }

  ~RangeString(){}

  unsigned int beg, end;
  string s;
};

class RangeStringLessThan
{
 public:
  bool operator()(const RangeString & rhs, const RangeString & lhs)
    {
      if(rhs.beg != lhs.beg)
	return rhs.beg < lhs.beg;
      else
	return rhs.end < lhs.end;
    }
};

class StringBuffer
{
 private:
  vector<string> name;
  vector<int> count;
  unsigned int fiveMSize;

 public:
  StringBuffer(vector<Range> & rVec, char* inputFileName, const unsigned int & phase1ParSize)
    {
      fiveMSize = phase1ParSize;
      fiveM = new char[fiveMSize];
      curPar = new char[phase1ParSize];

      smallBuf = new char[SMALL_BUF_SIZE];
 
      curParID = -1;
      this->phase1ParSize = phase1ParSize;
      inputFile.open(inputFileName);
      inputFile.seekg(0, ios::end);
      fileSize = inputFile.tellg();
      inputFile.seekg(0);
      inputFile.read(fiveM, fiveMSize);
      curOffset = fiveMSize;

      unsigned int totalSize =   (fiveMSize + phase1ParSize + SMALL_BUF_SIZE) ;
      
      rsVec.reserve(rVec.size());
      for(vector<Range>::iterator it = rVec.begin(); it != rVec.end(); ++it)
	{ 
	  inputFile.seekg((int)(it->beg-curOffset), ios::cur);
	  	
	  totalSize += (it->end - it->beg + 1);
	  char *temp = new char[it->end - it->beg + 1];
	  inputFile.read(temp, it->end - it->beg + 1);
	  curOffset = it->end+ 1;
	  
	  if(curOffset >= fileSize)
	    {
	      cout << "In loading ranges, curOffset " << curOffset << " >= fileSize " << fileSize << endl;
	      exit(0);
	    }
	  
	  RangeString r(it->beg, it->end, temp); 
	  rsVec.push_back(r);
	  delete []temp;
	}

      cout << "Total string buffer size = " << totalSize/1024 << "KB"<<endl;
    }
  
  ~StringBuffer()
    {
      if(inputFile.is_open())
	inputFile.close();

      delete []fiveM;
      delete []curPar;
      delete []smallBuf;
    }

  void loadPartition(const int & parID)
    {  
      if(parID == 0)
	{
	  inputFile.clear();
	  curParID = -1;
	  curOffset = fiveMSize;
	  inputFile.seekg(fiveMSize);
	  return;
	}

      if(parID > 0 && curParID != parID)
	{
	  curParStart = parID * phase1ParSize;
	  inputFile.seekg((int)(curParStart - curOffset), ios::cur);
	  	  
	  inputFile.read(curPar, phase1ParSize);
	  curParID = parID;
	  curOffset = (parID+1) * phase1ParSize;
	  
	  if(curOffset >= fileSize)
	    {
	      curOffset = 0;
	      inputFile.clear();
	      inputFile.seekg(0);
	    }
	}
    }

  char operator[](const unsigned int & i) 
    {
      if(i >= smallBufStart && i < smallBufStart + SMALL_BUF_SIZE)
	{
	  return smallBuf[i - smallBufStart];
	}

      if(curParID != -1 && (i >= curParStart && i < curParStart + phase1ParSize))
	{
	  return curPar[i - curParStart];
	}

      if(i < fiveMSize)
	{ 
	  return fiveM[i];
	}

      inputFile.seekg((int)(i-100-curOffset), ios::cur);
      
      smallBufStart = i-100;
      inputFile.read(smallBuf, SMALL_BUF_SIZE);
      curOffset = (i-100) + SMALL_BUF_SIZE;

      if(curOffset >= fileSize)
	{
	  cout << "In reading from disk, curOffset " << curOffset
	       << " has become >= fileSize " << fileSize << " so we reset it to 0." << endl;
	  curOffset = 0;
	  inputFile.clear();
	  inputFile.seekg(0);
	}


      return smallBuf[100];
    }
  
 private:
  char *fiveM;
  int curParID;
  char* curPar, *smallBuf;
  unsigned int curParStart, smallBufStart, phase1ParSize, curOffset, fileSize;
  vector<RangeString> rsVec; 
  ifstream inputFile;
};

} /* namespace libednai_trellis_plus */

#endif
