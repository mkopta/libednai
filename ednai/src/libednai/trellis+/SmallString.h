#ifndef SMALL_STR_H
#define SMALL_STR_H

#include <string>
#include <iostream>

namespace libednai_trellis_plus {
using namespace std;

class SmallString
{
 public:
  SmallString(const int & initSize)
    {
      this->initSize = initSize;
      for(int i = 0; i != initSize; ++i)
	v.push_back('5');
    }

  void clear()
    {
      for(int i = 0; i != initSize; ++i)
	v[i] = '5';
    }

  void setStart(const unsigned int & i)
    {
      startPos = i;
    }

  void getStart() const { return startPos; }

  void putChar(const char & c, const unsigned int & i)
    {
      assert(i < initSize);
      v[c] = i;
    }

  char operator[] const (const unsigned int & i)
    {
      assert('5' != v[i-startPos]);
      return v[i-startPos];
    }

  ~SmallString(){}
  
 private:
  vector<char> v;
  unsigned int startPos, initSize;
};

} /* namespace libednai_trellis_plus */

#endif
