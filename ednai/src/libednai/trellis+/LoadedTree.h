#include <iostream>
#include <fstream>
#include <cstring>
#include "LoadedNode.h"
#include "BitString.h"

namespace libednai_trellis_plus {

using namespace std;

class LoadedTree
{
 public:
  template <class T>
  I_LoadedNode * load(fstream & treeFile, T* tempStr);
};

template <class T>
I_LoadedNode*
LoadedTree::load(fstream & treeFile, T* tempStr)
{
  I_LoadedNode *root = NULL;
  
  short X = 12345;
  short curTab = 0, lastTab = 0;
  char indexC;
  UI4Bytes s, len, e, as;

  UI4Bytes bufSize = sizeof(short) + sizeof(char) + (3*UI4SIZE); 
  char buf[bufSize];
  UI4Bytes bufPtrOffset = 0;

  gTimer.Start();
  LoadedNode *lastNode = NULL;
  treeFile.read((char*) & curTab, sizeof(short));

  while(curTab != X)
    {
      treeFile.read((char*) &buf, bufSize);
      bufPtrOffset = 0;

      memcpy(&indexC, buf, sizeof(char));

      bufPtrOffset += sizeof(char);
      memcpy(&s, buf+bufPtrOffset, UI4SIZE);
      bufPtrOffset += UI4SIZE;
      memcpy(&len, buf+bufPtrOffset, UI4SIZE);
      bufPtrOffset += UI4SIZE;
      memcpy(&as, buf+bufPtrOffset, UI4SIZE);
      bufPtrOffset += UI4SIZE;

      //////////////////////////// BUILD THE TREE /////////////////////////////////
          
      e = s+len-1;
      
      LoadedNode *n;
      if(e == fileSize) // leaf
	{ 
	  L_LoadedNode *lNode = new (&L_LoadedNodeArray[L_LoadedNodeCount++]) L_LoadedNode(s, e);
	  lNode->as = as;
	  n = lNode;
	}
      else
	{
	  I_LoadedNode *iNode = new (&I_LoadedNodeArray[I_LoadedNodeCount++]) I_LoadedNode(s, e);
	  n = iNode;
	}

      if(curTab == 0) 
	{
	  lastTab = curTab;
	  memcpy(&curTab, buf+bufPtrOffset, sizeof(short));
	  n->p = NULL;
	  root = static_cast<I_LoadedNode*> (n);
	  continue;
	}

      if(curTab == 1)
	{
	  root->addChild(n, indexC-48);
	  n->p = root;
	  lastTab = curTab;
	  lastNode = n;
	}
      else if(curTab == lastTab+1)
	{
	  (static_cast<I_LoadedNode*>(lastNode))->addChild(n, indexC-48);
	  n->p = lastNode;
	  lastTab = curTab;
	  lastNode = n;
	}
      else if(curTab == lastTab)
	{
	  (static_cast<I_LoadedNode*>(lastNode->p))->addChild(n, indexC-48);
	  n->p = lastNode->p;

	  lastNode = n;
	}
      else if(curTab < lastTab)
	{
	  LoadedNode *temp = lastNode;
	  while(curTab != lastTab)
	    {
	      temp = temp->p;
	      --lastTab;
	    }

	  (static_cast<I_LoadedNode*>(temp->p))->addChild(n, indexC-48);
	  n->p = temp->p;
	  lastNode = n;
	  lastTab = curTab;
	}
      //////////////////////////// END OF BUILDING THE TREE //////////////////////////
      
      lastTab = curTab;
      memcpy(&curTab, buf+bufPtrOffset, sizeof(short));
    }

  diskTimeR+=gTimer.Stop();

  return root;
}

} /* namespace libednai_trellis_plus */

