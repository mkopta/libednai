#ifndef LOADED_NODE_H
#define LOADED_NODE_H

#include <iostream>
#include "Global.h"
#include "XChars64.h"

namespace libednai_trellis_plus {
using namespace std;

class LoadedNode 
{
 public:
  LoadedNode(){}
  LoadedNode(const UI4Bytes & ss, const UI4Bytes & ee):s(ss), e(ee){}
  virtual ~LoadedNode(){}
  void print() { cout << "[" << s << ", " << e << "]" << endl; }
  
  virtual bool find(string & ipStr, UI4Bytes beg)=0;
  
  LoadedNode *p;
  UI4Bytes s, e;
 
  friend bool operator==(const LoadedNode& n1, const LoadedNode& n2)
    {
      return n1.s == n2.s && n1.e == n2.e;
    }
  bool isLeaf() const { return e == fileSize; }

};

class I_LoadedNode: public LoadedNode
{ 
 public: 
  I_LoadedNode(){init(); actualIndex = 0;}
  I_LoadedNode(const UI4Bytes & ss, const UI4Bytes & ee) { init(); LoadedNode::s = ss; LoadedNode::e = ee; }
  ~I_LoadedNode() {}
  UI4Bytes actualIndex;

  int countInternal(int & c)
    {
      ++c;
      for(UI4Bytes i = NUMCHAR+1; i--;)
	if(children[i])
	  if(children[i]->isLeaf() == false)
	    (static_cast<I_LoadedNode*>(children[i]))->countInternal(c);
      return c;
    }
    
  bool find(string & ipStr, UI4Bytes beg)
    { 
      cout << "Internal Node: ActualIndex = " << actualIndex << 
	" [" << s << "," << e << "] beg " << beg << " charbeg = " << ipStr[beg] << endl;

      for(UI4Bytes i = s; i <= e; ++i)
	{
	  cout << "edge i = " << i << " char = " << ipStr[i] << " str = " << beg << " char = " << ipStr[beg] << endl; 
	  assert(ipStr[i] == ipStr[beg++]);
	}

      int cIndex = ipStr[beg]-48;
      
      cout << "These are its children" << endl;
      for(int i = 0; i <= NUMCHAR; ++i)
	if(children[i] == NULL)
	  cout << i << ") 0 " << endl;
	else 
	  {
	    cout << i << ") " << flush;
	    if(children[i]->isLeaf())
	      {
		cout << "Leaf: "; 
		children[i]->print();
	      }
	    else
	      cout << "Internal: " << static_cast<I_LoadedNode*>(children[i])->actualIndex << endl;
	  }
      cout << endl;

      cout << "Now Look for beg " << beg << " or char " << ipStr[beg] << "\n" << endl;
      assert(children[cIndex]!=NULL);
      return children[cIndex]->find(ipStr, beg);
    }
   
  bool hasOneChild(UI4Bytes & childIndex) const
    {
      UI4Bytes nc = 0;
      for(UI4Bytes i = NUMCHAR+1; i--;)
	if(children[i])
	  {
	    if(nc == 0)
	      childIndex = i;
	    ++nc;
	  }

      return (nc == 1);
    }

  UI4Bytes getNumChildren() const
    {
      UI4Bytes nc = 0;
      for(UI4Bytes i = NUMCHAR+1; i--;)
	//for(UI4Bytes i = 0; i < NUMCHAR+1; ++i)
	if(children[i])
	  ++nc;
      return nc;
    }

  void addChild(LoadedNode* node, int index)
    {
      if(index == -48)
        index = 0;

      if(children[index] != NULL)
        {
          cout << "Error: ";
          cout << "Trying to add child " << endl;
          node->print();
          cout << " but " << endl;
          print();
          cout << " (sent index) already has a child beginning at index " << index << endl;
	  exit(0);
        }
      children[index] = node;
    }
		 
  LoadedNode *children[NUMCHAR+1];
 private:
  void init()
    {
      for(UI4Bytes i = NUMCHAR+1; i--;)
	//for(UI4Bytes i = 0; i < NUMCHAR+1; ++i)
	children[i] = NULL;
    }
};

// extra 4 bytes
class L_LoadedNode: public LoadedNode
{
 public:
  L_LoadedNode(){}
  L_LoadedNode(const UI4Bytes & ss, const UI4Bytes & ee) { LoadedNode::s = ss; LoadedNode::e = ee; }
  ~L_LoadedNode() {}
  UI4Bytes as;
  //Ninja
  XChars64 xc;

  void print() { LoadedNode::print(); cout << "as " << as << endl; }
  
  bool find(string & ipStr, UI4Bytes beg)    {
      cout << "find Leaf " << s << " " << e << endl;
      for(UI4Bytes i = s; i < e; ++i)
	{
	  //cout << "edge i = " << i << " char = " << ipStr[i] << " str = " << beg << " char = " << ipStr[beg] << endl; 
	  assert(ipStr[i] == ipStr[beg++]);
	}
      return true;
  }
};

} /* namespace libednai_trellis_plus */

#endif
