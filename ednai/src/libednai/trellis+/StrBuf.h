#ifndef STRBUF_H
#define STRBUF_H
#include <fstream>
#include <iostream>

namespace libednai_trellis_plus {
using namespace std;
typedef unsigned int UI4Bytes;

#define DEFAULT_NUM_NC 1024

class StrBuf
{
public:

  StrBuf(char* fileName, const UI4Bytes & numNC, const UI4Bytes & threshold)
  {
    assert(numNC%4 == 0);
    shiftAmt[0] = 6; shiftAmt[1] = 4; shiftAmt[2] = 2; shiftAmt[3] = 0;

    numNC_ = numNC;
    inputFile_.open(fileName);
    buf_ = new char[numNC_>>2]; 
    threshold_ = threshold;
    bufR_ = new char[threshold];
    load(0);
    
    cout << "This buffer will hold " << numNC_ << " NCs using " << numNC_/4 << " bytes" << endl;
  }
  
  char operator[](const UI4Bytes & i)
  {
    if(i >= curStartR_ && i < curStartR_ + threshold)
      return bufR_[i-curStartR_];

    if(i >= curStart_ && i < curStart_ + numNC_)
      {
	char c = buf_[(i-curStart_)/4];
	return (char)(49 + ((c >> shiftAmt[i%4]) & 3));
      }

    load(i);
    return (char)(49 + ((buf_[0] >> 6) & 3));
  }

  void updateR(const UI4Bytes & i)
  {
    curStartR_ = i;
    inputFile_.seekg(i);
    inputFile_.read(bufR_, threshold_);
  }
  
  ~StrBuf()
  {
    if(inputFile_.is_open())
      {
	inputFile_.close();
	delete []buf_;
      }
  }
  
private:
  UI4Bytes numNC_, curStart_, curStartR_, threshold_;
  ifstream inputFile_;
  char* buf_, *bufR_;
  UI4Bytes shiftAmt[4];

    
  char getCharR(const UI4Bytes & i)
  {
    return bufR_[i-curStartR_];
  }


  void load(const UI4Bytes & offset)
  {
    curStart_ = offset;
    inputFile_.seekg(offset);
    UI4Bytes i = 0, endI, curPos = 0;
    char cR, cB;
    while(i != numNC_)
      {
	endI = i+4;
	cB = 0;
	for(; i != endI; ++i)
	  {
	    inputFile_.get(cR);
	    
	    cB = cB | (UI4Bytes)((cR-49) << shiftAmt[i%4]); 
	  }

	buf_[curPos++] = cB;
      }
  }
};

} /* namespace libednai_trellis_plus */

#endif
