#ifndef RANGE_H
#define RANGE_H

#include <vector>

namespace libednai_trellis_plus {
using namespace std;

struct Range
{
  unsigned int beg, end;
};

class RangeLess
{
 public:
  bool operator()(const Range & r1, const Range & r2)
    {
      return r1.beg < r2.beg;
    }
};

bool find(const unsigned int & i, vector<Range> * v)
{
  for(vector<Range>::iterator it = v->begin(); it != v->end(); ++it)
    {
      if(i >= it->beg && i <= it->end)
	{
	  return true;
	}
    }
  return false;
}

vector<Range> collapseRange(const vector<Range> & rVec)
{
  if(rVec.empty())
    return rVec;

  int sum = 0;
  vector<Range> v;
  Range curRange = *rVec.begin();
  for( vector<Range>::const_iterator it = rVec.begin()+1; it != rVec.end(); ++it)
    {
      if(curRange.end > it->beg)
	curRange.end = it->end;
      else
	{
	  sum += (curRange.end - curRange.beg + 1);
	  v.push_back(curRange);
	  curRange = *it;
	}
    }
  return v;
}

} /* namespace libednai_trellis_plus */

#endif
