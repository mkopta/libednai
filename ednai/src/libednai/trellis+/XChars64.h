#ifndef XCHARS64_H
#define XCHARS64_H

#include <fstream>
#include <iostream>
#include "TimeTracker.h"

namespace libednai_trellis_plus {

using namespace std;

class XChars64
{
 public:
  XChars64() 
  {
    myD = 63;
  }

  template <class T>
  void init(T* bitS, const UI4Bytes & b)
    {
      myD = 0;
      for(UI4Bytes i = 29; i--; )
	myD = myD | (((long long)((*bitS)[i+b]-49))<< (64-((i+1)<<1)) );
    }

  bool touched() const
    {
      return (myD != 63);
    }
  
  void showBin() const
  {
    char cArray[8];
    memcpy(cArray, &myD, 8);
    UI4Bytes temp1;
    memcpy(&temp1, cArray, 4);
    string binRep = getBin(temp1);
    cout << binRep << " ";
    
    memcpy(&temp1, cArray+4, 4);
    binRep = getBin(temp1);
    cout << binRep << endl;
  }

  string getBin(UI4Bytes & num) const
    {
      string bin;

      for(UI4Bytes i = 0; i != sizeof(UI4Bytes)*8; ++i)
	{
	  if(i>0)
	    num = (num >> 1);
	  if(num % 2 == 0) 
	    bin = "0" + bin;
	  else
	    bin = "1" + bin;
	  
	  if((1+i)%4 == 0)
	    bin = " " + bin;
	}

      return bin;
    }

  char operator[] (const UI4Bytes & i) const
    {
      return (char) ((3 & (myD >> (62-(i<<1)))) + 49);
    }

  UI4Bytes pos() const
  {
    return ((myD<<58)>>58);
  }

  void advancePos()
  { 
    if(!touched())
      return;

    if(((myD<<58)>>58)<29)
      ++myD;
  }

  void advancePos(const UI4Bytes & len)
  {
    if(!touched())
      return;

    if( ((myD<<58)>>58) + len < 29) 
      myD+=len;
    else
      myD = (((myD >> 6)<<6) | 29);
  }

  bool done() const
  {
    return (((myD<<58)>>58) == 29);
  }

  friend ostream & operator<<(ostream & os, const XChars64 & x)
    {
      for(UI4Bytes i = 0; i < 29; ++i)
	os << x[i] ;
      return os;
    } 

  template <class T>
  void check(unsigned int s, unsigned int e, T* str)
    {
      if(done())
	{
	  cout << "check: return b/c done" << endl;
	  return;
	}

      string s1="", s2="";
      unsigned int actualLen = e - s + 1;
      unsigned int xcLen = 29 - pos();
      unsigned int len = actualLen > xcLen ? xcLen : actualLen;

      for(unsigned int i = s; i != s+len; ++i)
	s1 += (*str)[i];

      for(unsigned int i = s; i != s+len; ++i)
	s2 += (*this)[i - s + pos()];

      if(s1 != s2)
	{
	  cout << "Leaf info: [" << s << ", " << e << "]" << endl;
	  cout << "s1: " << s1 << endl;
	  cout << "xc: " << s2 << endl;
	  if(s1 != s2)
	    {
	      cout << "tempLeaf pos " << pos() << endl;
	      exit(0);
	    }
	}
    }

  ~XChars64(){}

private:
  long long myD;
};

} /* namespace libednai_trellis_plus */

#endif

/*
int main(int argc, char* argv[])
{
  cout << sizeof(double) << endl;
  cout << sizeof(XChars64) << endl;
  UI4Bytes start = atoi(argv[2]);
  TimeTracker tt; tt.Start();
  BitString bitStr(argv[1]);
  cout << "Load took " << tt.Stop() << " sec" << endl;
  XChars64 xc(&bitStr, start);

  cout << "From XChars" << endl;
  xc.showBin();
  cout << "From bitString" << endl;
  string temp =  bitStr.substr(start, 29);

  cout << "simply put " << xc << endl;
  
  for(int i = 0; i != temp.size(); ++i)
    {
      if( i % 2 == 0)
	cout << " ";
      if( i == 16)
	cout << " ";

      if(temp[i] == '1')
	cout << "00";
      else if (temp[i] == '2')
	cout << "01";
      else if (temp[i] == '3')
	cout << "10";
      else 
	cout << "11";
    }
  cout << endl;

  cout << temp << endl;
  
  for(int i = 0; i != 29; ++i)
    cout << xc[i];
  cout << endl;

  char c;
  while(!xc.done())
    {
      cout << "pos = " << xc.pos() << endl;
      if(xc.touched())
	cout << "Touched" << endl;
      else
	cout << "Not touched" << endl;

      xc.showBin();
      xc.advancePos(3);
      if(xc.done())
	cout << "Done!"<<endl;
      else
	cout << "Not done yet." << endl;
      if(cin.get(c))
	continue;
    }
  xc.showBin();
  return 0;
}
*/
