#ifndef XCHARS_H
#define XCHARS_H

#include <fstream>
#include <iostream>
#include "BitString.h"

namespace libednai_trellis_plus {

using namespace std;

class XChars
{
 public:
  XChars()
  {
    myByte = 0; 
  }

  XChars(BitString* bitS, const UI4Bytes & b)
    {
      init(bitS, b); 
    }

  void init(BitString* bitS, const UI4Bytes & b)
    {
      myByte = 0;
      for(UI4Bytes i = b; i != b + 14; ++i) 	  
	myByte = myByte | (((UI4Bytes)((*bitS)[i]-49))<<(32-((i-b+1)<<1)));
    }

  void showBin()
    {
      UI4Bytes num = myByte;
      string bin;

      for(UI4Bytes i = 0; i != sizeof(UI4Bytes)*8; ++i)
	{
	  if(i>0)
	    num = (num >> 1);
	  if(num % 2 == 0) 
	    bin = "0" + bin;
	  else
	    bin = "1" + bin;
	  
	  if((1+i)%4 == 0)
	    bin = " " + bin;
	}
      
      cout << bin << endl;
    }

  char operator[] (const UI4Bytes & i) const
    {
      return (char) ((3 & (myByte >> (30-(i<<1)))) + 49);
    }
  
  UI4Bytes pos() const
  {
    return ((myByte<<28)>>28);
  }

  void advancePos()
  {
    if(((myByte<<28)>>28)<14)
      ++myByte;
  }

  void advancePos(const UI4Bytes & len)
  {
    if( ((myByte<<28)>>28) + len < 14) 
      myByte+=len;
    else
      myByte = (((myByte >> 4)<<4) | 14);
  }

  bool done() const
  {
    return (((myByte<<28)>>28) == 14);
  }

  friend ostream & operator<<(ostream & os, const XChars & x)
    {
      for(UI4Bytes i = 0; i < 14; ++i)
	os << x[i] ;
      return os;
    } 
 
  ~XChars(){}
private:
  UI4Bytes myByte;
};

} /* namespace libednai_trellis_plus */

#endif
