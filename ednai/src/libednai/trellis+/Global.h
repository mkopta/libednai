
#ifndef GLOBAL_H
#define GLOBAL_H
 
#include <vector>
#include <fstream>
#include <iostream>
#include <string>
#include <map>
#include "math.h"
#include "Range.h" 
#include "TimeTracker.h"
#include "SimpleString.h"
#include "BitString.h"
#include <assert.h>

namespace libednai_trellis_plus {
 
#define NUMCHAR 4
#define ELEN 200

using namespace std;

vector<Range> bigRangeVec;
vector<vector<short> > noPrefVec;

bool* valid;

TimeTracker gTimer;
double diskTimeW=0, diskTimeR=0;

UI4Bytes maxBufSize=0;
char *charBuf=NULL;
UI4Bytes bufPos=0;

UI4Bytes invalid = 0, newInternal = 0;

//TEMP
UI4Bytes spanSize = 0;
const UI4Bytes UI4SIZE = sizeof(UI4Bytes);
map<UI4Bytes, UI4Bytes> spanMap;

UI4Bytes lBufSize = 0, iBufSize = 0, lPos = 0, iPos = 0;
char *lBuf, *iBuf;

/////////////STUFF FOR PLACEMENT NEW////////////////////////
double factor_phase1 = 0, factor_phase2 = 0;

class I_Node;
void *rawMemory0 = NULL;
I_Node *I_NodeArray = NULL;
UI4Bytes I_NodeMax = 0;
UI4Bytes I_NodeCount = 0;

class L_Node;
void *rawMemory1 = NULL;
L_Node *L_NodeArray = NULL;
UI4Bytes L_NodeMax = 0;
UI4Bytes L_NodeCount = 0;

class I_LoadedNode;
void *rawMemory3 = NULL;
I_LoadedNode *I_LoadedNodeArray = NULL;
UI4Bytes I_LoadedNodeMax = 0;
UI4Bytes I_LoadedNodeCount = 0;

class L_LoadedNode;
void *rawMemory4 = NULL;
L_LoadedNode *L_LoadedNodeArray = NULL;
UI4Bytes L_LoadedNodeMax = 0;
UI4Bytes L_LoadedNodeCount = 0;

/////////////END OF STUFF FOR PLACEMENT NEW/////////////////

//////////////// STUFF FOR 1 FILE PER PARTITION ////////////
#define ENDMARKER 12345
ifstream *fArray;
fstream *tfArray;
//////////////// END OF STUFF FOR 1 FILE PER PARTITION /////

UI4Bytes SIZET_8 = 8 * sizeof(UI4Bytes); 
int nsSize = (sizeof(UI4Bytes) * 4) + (sizeof(streamoff)*2);
 
char* inputFileName;
char dirPath[150], tempDirPath[150];
char prefixFileName[150];
 
typedef SimpleString SEQ;
typedef string PREF;


UI4Bytes candT1 = 0, candT2 = 0;
UI4Bytes threshold = 0, offset = 0;
UI4Bytes fileSize = 0, extraLen = 0;
UI4Bytes maxPrefLen = 0;
vector<vector<pair<PREF, UI4Bytes> > > prefInfo;
vector<PREF> prefVec;
vector<UI4Bytes> freqVec;
SEQ* s;
string* s1;

double nodeCreated = 0, nodeDestroyed = 0;
UI4Bytes treeCreated = 0, treeDestroyed = 0;
UI4Bytes nMergeCreated = 0;
UI4Bytes currentEnd = 0;

char i2c(int i)
{
  return (char)(i + 48);
}

char c2i(char c)
{
  return (int)c - 48;
}

int getPrefCode(const PREF & pf)
{
  int pcode = 0;

  for(UI4Bytes i = 0; i < pf.size(); ++i)
    pcode += ((c2i(pf[i])-1) * (int)pow((double)NUMCHAR, (int)i));

  return pcode;
}

int getPrefCode(UI4Bytes b, UI4Bytes e)
{
  int pcode = 0;

  for(UI4Bytes i = b; i <= e; ++i)
    pcode += ( ((*s)[i]-1) * (int)pow((double)NUMCHAR, (int)(i-b)) );
    
  return pcode;
}

} /* namespace libednai_trellis_plus */

#endif
