//////////////////////////////////////////////////////////////////////////////
//  TRELLIS+ (without String Buffer): A disk-based suffix tree construction
//  for genome-scale DNA sequence indexing
//
//  Copyright (C) 2007  Benjarath Phoophakdee
//
//  This file is a part of TRELLIS+.
//
//  TRELLIS+ is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  any later version.
//
//  TRELLIS+ is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.
//////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <fstream>
#include <string>

namespace libednai_trellis {

using namespace std;

int ReadLeaf_main(int argc, char* argv[])
{
  ifstream f(argv[1]);
  unsigned int s, as;
  
  while(!f.eof())
    {
      f.read((char*) & s, sizeof(unsigned int));
      f.read((char*) & as, sizeof(unsigned int));
      cout << "Suffix ID " << as << ", Start Index " << s << endl;
    }
  return 0;
}

} /* namespace libednai_trellis */
