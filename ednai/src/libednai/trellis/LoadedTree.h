//////////////////////////////////////////////////////////////////////////////
//  TRELLIS+ (without String Buffer): A disk-based suffix tree construction
//  for genome-scale DNA sequence indexing
//
//  Copyright (C) 2007  Benjarath Phoophakdee
//
//  This file is a part of TRELLIS+.
//
//  TRELLIS+ is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  any later version.
//
//  TRELLIS+ is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.
//////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <fstream>
#include "LoadedNode.h"
#include "BitString.h"

namespace libednai_trellis {

using namespace std;

class LoadedTree
{
 public:
  LoadedTree(){}
  ~LoadedTree(){}
  I_LoadedNode * load(fstream & treeFile, BitString* tempStr);
}; 

I_LoadedNode*
LoadedTree::load(fstream & treeFile, BitString* tempStr)
{
  I_LoadedNode *root = NULL;
  
  short X = 12345;
  short curTab = 0, lastTab = 0;
  char indexC;
  UI4Bytes s, len, e, as;
  
  UI4Bytes bufSize = sizeof(short) + sizeof(char) + (3*UI4SIZE); 
  char buf[bufSize];
  UI4Bytes bufPtrOffset = 0;

  gTimer.Start();
  LoadedNode *lastNode = NULL;
  treeFile.read((char*) & curTab, sizeof(short));
  while(curTab != X)
    {
      treeFile.read((char*) &buf, bufSize);
      bufPtrOffset = 0;

      memcpy(&indexC, buf, sizeof(char));
      bufPtrOffset += sizeof(char);
      memcpy(&s, buf+bufPtrOffset, UI4SIZE);
      bufPtrOffset += UI4SIZE;
      memcpy(&len, buf+bufPtrOffset, UI4SIZE);
      bufPtrOffset += UI4SIZE;
      memcpy(&as, buf+bufPtrOffset, UI4SIZE);
      bufPtrOffset += UI4SIZE;
                
      e = s+len-1;
      
      LoadedNode *n;
      if(e == fileSize) 
	{ 
	  if(L_LoadedNodeCount >= L_LoadedNodeMax)
	    {
	      cout << "L_LoadedNodeCount " << L_LoadedNodeCount << " >= L_LoadedNodeMax " << L_LoadedNodeMax << endl;
	      exit(0);
	    }

	  L_LoadedNode *lNode = new (&L_LoadedNodeArray[L_LoadedNodeCount++]) L_LoadedNode(s, e);
	  lNode->as = as;
	  n = lNode;
	}
      else
	{
	  if(I_LoadedNodeCount >= I_LoadedNodeMax)
	    {
	      cout << "I_LoadedNodeCount " << I_LoadedNodeCount << " >= I_LoadedNodeMax " << I_LoadedNodeMax << endl;
	      exit(0);
	    }

	  I_LoadedNode *iNode = new (&I_LoadedNodeArray[I_LoadedNodeCount++]) I_LoadedNode(s, e);
	  n = iNode;
	}

      if(curTab == 0) 
	{
	  lastTab = curTab;
	  memcpy(&curTab, buf+bufPtrOffset, sizeof(short));
	  n->p = NULL;
	  root = static_cast<I_LoadedNode*> (n);
	  continue;
	}

      if(curTab == 1)
	{
	  root->addChild(n, indexC-48);
	  n->p = root;
	  lastTab = curTab;
	  lastNode = n;
	}
      else if(curTab == lastTab+1)
	{
	  (static_cast<I_LoadedNode*>(lastNode))->addChild(n, indexC-48);
	  n->p = lastNode;
	  lastTab = curTab;
	  lastNode = n;
	}
      else if(curTab == lastTab)
	{
	  (static_cast<I_LoadedNode*>(lastNode->p))->addChild(n, indexC-48);
	  n->p = lastNode->p;

	  lastNode = n;
	}
 else if(curTab < lastTab)
	{
	  LoadedNode *temp = lastNode;
	  while(curTab != lastTab)
	    {
	      temp = temp->p;
	      --lastTab;
	    }

	  (static_cast<I_LoadedNode*>(temp->p))->addChild(n, indexC-48);
	  n->p = temp->p;
	  lastNode = n;
	  lastTab = curTab;
	}
      
      lastTab = curTab;
      memcpy(&curTab, buf+bufPtrOffset, sizeof(short));
    }

  diskTimeR+=gTimer.Stop();
  
  return root;
}

} /* namespace libednai_trellis */
