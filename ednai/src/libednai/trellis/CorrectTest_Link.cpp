#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <sstream>
#include <map>
#include <algorithm>
#include <cstring>
#include <cassert>
#include "BitString.h"
#include "TimeTracker.h"

namespace libednai_trellis {

using namespace std;
 
typedef BitString STRING;
//typedef string STRING;
 
//UI4Bytes ISIZE = 32, LSIZE = 8;
#define ISIZE 32
#define LSIZE 8
map<string, int> pmap;
vector<string> correcttest_link_prefVec;
STRING  inputString;
typedef unsigned int UI4Bytes;
UI4Bytes correcttest_link_fileSize, correcttest_link_threshold;
char leafBuf[LSIZE], internalBuf[ISIZE];
bool allPos = false;

void getPrefix(char* pfileName)
{
  ifstream pfile(pfileName);
  string prefix;
  while(getline(pfile, prefix))
    {
      //cout << prefix << " = " << correcttest_link_prefVec.size() << endl;
      pmap[prefix] = correcttest_link_prefVec.size();
      correcttest_link_prefVec.push_back(prefix);
      getline(pfile, prefix);
    }
  sort(correcttest_link_prefVec.begin(), correcttest_link_prefVec.end());
  cout << "There are " << correcttest_link_prefVec.size() << " prefixes." << endl;
  pfile.close();
}

string getPrefix(size_t sfId)
{
  /*
  if(correcttest_link_prefVec.empty())
    {
      cout << "No prefix to search" << endl;
      exit(0);
    }
  */

  TimeTracker tt;
  tt.Start();
  string pref;

  int c = 1;
  while(true)
    {
      pref = inputString.substr(sfId, c++);
      //cout << "search for " << pref << " sfId = " << sfId << endl;
      bool r = binary_search(correcttest_link_prefVec.begin(), correcttest_link_prefVec.end(), pref);
      if(r)
        return pref;
    }
  cout << "Got prefix " << pref << " in " << tt.Stop() << " seconds." << endl;

  return pref;
}

void popLeaf(ifstream & leafFile, UI4Bytes offset, 
	     UI4Bytes & s, UI4Bytes & e, UI4Bytes & as)
{
  if(!leafFile.good())
    leafFile.clear();

  leafFile.seekg(offset);
  
  //leafFile.read((char*) & s, 4);
  //leafFile.read((char*) & as, 4);
  //e = correcttest_link_fileSize;

  leafFile.read((char*) &leafBuf, 8);
  memcpy(&s, leafBuf, 4);
  memcpy(&as, leafBuf+4, 4);
  e = correcttest_link_fileSize;
}

void popIn(ifstream & iFile, UI4Bytes offset,
	   UI4Bytes & s, UI4Bytes & e, UI4Bytes * children, UI4Bytes & sfID)
{
  if(!iFile.good())
    iFile.clear();
    
  iFile.seekg(offset);
  //cout << "Get Internal at cIndex " << iFile.tellg()/ISIZE<< endl;

  /*
  iFile.read((char*) & s, 4);
  iFile.read((char*) & e, 4);

  for(int i = 0; i <5; ++i)
    iFile.read((char*) &children[i], 4);
  */

  iFile.read((char*)&internalBuf, ISIZE);
  memcpy(&s, internalBuf, 4);
  memcpy(&e, internalBuf+4, 4);
  int x = 24;
  for(int i = 5; i--;)
    {
      //cout << i << " " << x << endl;
      memcpy(&children[i], internalBuf+x, 4);
      x-=4;
    }
  memcpy(&sfID, internalBuf+28, 4); //read suffix link ID
  
}

void search(ifstream & ifile, ifstream & lfile, unsigned int beg, const unsigned int & end, 
	    int sid, unsigned int & sfId, unsigned int & pLen)
{
  UI4Bytes s, e, as = 0, *c = new UI4Bytes[5], lastSfId=0;
  
  //cout << "pLen = " << pLen << endl;
  UI4Bytes cIndex;
  popIn(ifile, 0, s, e, c, sfId);
  bool leaf = false;

  //cout << "Root: pop result " << s << " " << e << endl;
  /*
  for(int i = 0; i < 5; ++i)
    {
      if(c[i] > correcttest_link_threshold+1)
	cout << i << ") " << c[i]-(correcttest_link_threshold+1) << " or " << c[i] << endl;
      else
	cout << i << ") " << c[i] << endl;
    }
  */
  while(beg <= end)
    {
      //get the edge you wanna match
      
      cIndex = c[inputString[beg]-48];
      
      //cout << "\nSearch for string index " << beg << " or char " << inputString[beg] 
      //   << " at cIndex = " << cIndex << endl;

      /*
      if(cIndex == 0)
	{
	  cout << "Suffix " << sid << " NOT Found." << endl;
	  cout << "Looking for char " << inputString[beg] << " but cIndex = 0" << endl;
	  exit(0);
	  return;
	}
      
      assert(cIndex != 0);
      */

      if(cIndex >= (correcttest_link_threshold+1)) // internal
	{ 
	  //cout << "I: " << cIndex << " or offset " << ISIZE*(cIndex-(1+correcttest_link_threshold)) << endl;
	  //  cout << "internal seek at  " << cIndex-(1+correcttest_link_threshold) << endl;
	  leaf = false;
	  //s = 0; e = 0; 
	  lastSfId = sfId;
	  popIn(ifile, ISIZE*(cIndex-(1+correcttest_link_threshold)),s, e, c, sfId);
	  ///pLen += (e-s+1);
	  //cout << "sfId " << sfId << " s " << s << " e " << e << endl;
		  
	  //cout << "pop internal result [" << s << ", " << e << "]" << endl;
	  /*
	  for(int i = 0; i < 5; ++i)
	    {
	      if(c[i] > correcttest_link_threshold+1)
		cout << c[i]-(1+correcttest_link_threshold) << " ";
	      else
		cout << c[i] << " ";
	    }
	    cout << endl;
	    //*/
	}
      else
	{  
	  //cout << "L: " << cIndex << " or offset " << LSIZE*(cIndex-1) << endl;
	  leaf = true;
	  popLeaf(lfile, LSIZE*(cIndex-1), s, e, as);
	  //cout << "pop leaf result [" << s << ", " << e << "]" << endl;
	}	
	
      //match
      UI4Bytes i = s;
      for(; i <= e; ++i)
	{
	  if(beg > end)
	    break;
	  
	  assert(inputString[i]==inputString[beg++]);
	}
      
      if(cIndex >= (correcttest_link_threshold+1)) //internal
	{
	  if(i == e+1) //used up the entire edge
	    pLen += (e-s+1);
	  else
	    sfId = lastSfId;
	}

      if(leaf && beg==correcttest_link_fileSize+1)
	{
	  cout << "Suffix " << as << " Found." <<  endl;
	  break;
	}
      else if(beg > end)
	{
	  cout << "String s[" << sid << ", " << end << "] found pLen = " << pLen <<  endl;
	  break;
	}
      
    }
}

  
void searchViaLink(ifstream & ifile, ifstream & lfile, unsigned int beg, const unsigned int & end, int sid,
		   const unsigned int & psfId, const unsigned int & pLen)
{ 
  //cout << "searchViaLink: beg = " << beg << " end = " << end << " sid = " << sid << " psfId = " << psfId <<
  //" pLen = " << pLen << endl;
  UI4Bytes s, e, as = 0, *c = new UI4Bytes[5], sfId;
  
  UI4Bytes cIndex;
  beg += pLen;
  //cout << "now offset beg to " << beg << endl;
  popIn(ifile, ISIZE*(psfId-(1+correcttest_link_threshold)), s, e, c, sfId); //start at the sf(p(n)) node
  bool leaf = false;

  //cout << "Root: pop result " << s << " " << e << endl;
  /*
  for(int i = 0; i < 5; ++i)
    {
      if(c[i] > correcttest_link_threshold+1)
	cout << i << ") " << c[i]-(correcttest_link_threshold+1) << " or " << c[i] << endl;
      else
	cout << i << ") " << c[i] << endl;
    }
  //*/

  while(true)
    {
      //get the edge you wanna match
      
      cIndex = c[inputString[beg]-48];
      
      //cout << "\nSearch for string index " << beg << " or char " << inputString[beg] 
      //   << " at cIndex = " << cIndex << endl;

      /*
      if(cIndex == 0)
	{
	  cout << "Suffix " << sid << " NOT Found." << endl;
	  cout << "Looking for char " << inputString[beg] << " but cIndex = 0" << endl;
	  exit(0);
	  return;
	}
      
      assert(cIndex != 0);
      */

      if(cIndex >= (correcttest_link_threshold+1)) // internal
	{ 
	  //cout << "I: " << cIndex << " or offset " << ISIZE*(cIndex-(1+correcttest_link_threshold)) << endl;
	  //  cout << "internal seek at  " << cIndex-(1+correcttest_link_threshold) << endl;
	  leaf = false;
	  //s = 0; e = 0;  
	  popIn(ifile, ISIZE*(cIndex-(1+correcttest_link_threshold)),s, e, c, sfId);
	  //cout << "pop internal result [" << s << ", " << e << "]" << endl;
	  /*for(int i = 0; i < 5; ++i)
	    {
	      if(c[i] > correcttest_link_threshold+1)
		cout << c[i]-(1+correcttest_link_threshold) << " ";
	      else
		cout << c[i] << " ";
	    }
	    cout << endl;*/
	}
      else
	{
	  //cout << "L: " << cIndex << " or offset " << LSIZE*(cIndex-1) << endl;
	  leaf = true;
	  popLeaf(lfile, LSIZE*(cIndex-1), s, e, as);
	  //cout << "pop leaf result [" << s << ", " << e << "]" << endl;
	}

      //match
      //don't need to match all characters, simply skip-and-count
      int x = beg;
      if(x + (e-s+1) <= end)
	x+=(e-s+1);
      else 
	x = end+1;

      //use this for loop just to test if all characters matched
      //*
      for(UI4Bytes i = s; i <= e; ++i)
	{
	  if(beg > end)
	    break;
	  ++beg;

	  //assert(inputString[i]==inputString[beg++]);
	}
      //*/

      //assert(x == beg);

      if(leaf && beg==correcttest_link_fileSize+1)
	{
	  //cout << "Suffix " << sid << " Found." << endl;
	  cout << "Suffix " << as << " Found." << endl;
	  break;
	}
      else if(beg > end)
	{
	  cout << "String s[" << sid << ", " << end << "] found " << endl;
	  break;
	}
    }
}

 
//use SingleQuery.cpp if want to use qBegin file
//this is for consecutive (with and without suffix links) only
int CorrectTest_Link_main(int argc, char* argv[])
{ 
  if(argc != 11 || (argv[9][0] != 'y' && argv[9][0] != 'n'))
    {
      cout << "Usage: " << argv[0] << " prefixFile ipFile iPath lPath threshold start_suffix_ID qLen qNum withLink shift" << endl;
      exit(0);
    }

  getPrefix(argv[1]);
  //ifstream ipFile(argv[2]);
  correcttest_link_threshold = atoi(argv[5]);
  //getline(ipFile, inputString);
  inputString.init(argv[2]);
  //inputString += "0";
  correcttest_link_fileSize = inputString.size();
  //ipFile.close();
  unsigned int lenSearch = atoi(argv[7]), qNum = atoi(argv[8]), shift = atoi(argv[10]);
   
  bool withLink = argv[9][0] == 'y' ? true : false;

  TimeTracker tt;
  string p, ipath, lpath;
  UI4Bytes sfId, pLen;
  unsigned int startHere = atoi(argv[6]);
  startHere += shift;
  for(unsigned int i = startHere; i < startHere + qNum; ++i) //atoi(argv[6]); i < inputString.size()-1; ++i)
    {
      cout << endl;
      tt.Start();
      //cout << "\nSuffix#" << i << ": " << inputString.substr(i, 5) << "..." << endl;
      ipath = argv[3];
      stringstream ss;
      p = getPrefix(i);
      ss << pmap[p];
      ipath += ss.str();
      cout << "Prefix: " << getPrefix(i) << "  " << ipath << endl;
      ifstream ifile(ipath.c_str());
      
      lpath = argv[4];
      lpath += "L_";
      lpath += p;
      //cout << lpath << endl;
      ifstream lfile(lpath.c_str());
      TimeTracker tt1;  tt1.Start();
      pLen = p.size();
      
      //search(ifile, lfile, begin search at, end search at, suffix ID, sfLinkId, pLen);
      if(lenSearch>0)
	search(ifile, lfile, i+p.size(), i+lenSearch-1, i, sfId, pLen);
      else
	search(ifile, lfile, i+p.size(), inputString.size(), i, sfId, pLen);

      cout << "Search: " << tt1.Stop() << " seconds." << endl;
      ifile.close();
      lfile.close();
      //cout << "Total: " << tt.Stop() << " seconds." << endl;

      if(!withLink)
	continue;

      ///////////////////////////////////////////////////////////////////

      cout << "Searching for the next suffix via suffix link..." << endl;
      ipath = argv[3];
      stringstream ss1;
      p = getPrefix(i+1);
      ss1 << pmap[p];
      ipath += ss1.str();
      ifstream ifile1(ipath.c_str());
      //cout << "File " << ipath << endl;
      //cout << "string " << inputString.substr(i+1, 10) << endl;
      
      lpath = argv[4];
      lpath += "L_";
      lpath += p;
      ifstream lfile1(lpath.c_str());
      tt1.Start();
      //cout << "sfId " << sfId << " pLen " << pLen << endl;
      --pLen;

      //searchViaLink(ifstream & ifile, ifstream & lfile, unsigned int beg, const unsigned int & end, int sid,
      //	   const unsigned int & psfId, const unsigned int & pLen)
      if(lenSearch>0)
	searchViaLink(ifile1, lfile1, i+1, i+lenSearch, i+1, sfId, pLen);
      else
	searchViaLink(ifile1, lfile1, i+1, inputString.size(), i+1, sfId, pLen);
      
      cout << "Search: " << tt1.Stop() << " seconds." << endl;
      ifile1.close();
      lfile1.close();

      cout << "Total: " << tt.Stop() << " seconds." << endl;
    }
  
  return 0;
}

} /* namespace libednai_trellis */
