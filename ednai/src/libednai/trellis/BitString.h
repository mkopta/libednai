//////////////////////////////////////////////////////////////////////////////
//  TRELLIS+ (without String Buffer): A disk-based suffix tree construction
//  for genome-scale DNA sequence indexing
//
//  Copyright (C) 2007  Benjarath Phoophakdee
//
//  This file is a part of TRELLIS+.
//
//  TRELLIS+ is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  any later version.
//
//  TRELLIS+ is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.
//////////////////////////////////////////////////////////////////////////////

#ifndef BIT_STR_H
#define BIT_STR_H

#include <iostream>
#include <string>
#include <fstream>

namespace libednai_trellis {

using namespace std;

typedef unsigned int UI4Bytes;

class BitString
{
public:
  BitString(){}
  
  BitString(const char* fname){ init(fname);}
  
  void init (const char* fname)
  {
    f.open(fname);
    f.seekg(0, ios::end);
    fileSize = f.tellg();
    cout << "fileSize = " << fileSize << endl;
    f.seekg(0);
    arraySize = ((fileSize%16 == 0) ? fileSize/16 : (fileSize/16)+1);
    bitArray = new UI4Bytes[arraySize];
    for(int j = 16; j--;)
      {
	shiftAmt[j] = 30-(j<<1);
      }
    
    scan();
    f.close();
  }
  
  
  ~BitString()
  {
    delete [] bitArray;
  }

  char operator[] (const UI4Bytes & i) const
  {
    /*
    UI4Bytes bigIndex = i / 32, smallIndex = i % 32;
    UI4Bytes j = bitArray[bigIndex];
    cout << "bigIndex = " << bigIndex << " smallIndex = " << smallIndex << endl;
    cout << "shiftback for " << shiftAmt[smallIndex] << endl;
    UI4Bytes answer = (j >> shiftAmt[smallIndex]);
    cout << answer << endl;
    return (answer & 3)+1;
    */
    
    return (char)(((bitArray[i/16] >> shiftAmt[i%16]) & 3) + 49);
  }

  UI4Bytes size() const { return fileSize; }

  string substr(const UI4Bytes & i, const UI4Bytes & len)
    {
      string s;
      for(UI4Bytes j = i; j < i+len; ++j)
	s += (*this)[j];
      return s;
    } 

private:
  UI4Bytes *bitArray, shiftAmt[16];
  UI4Bytes arraySize, fileSize;
  ifstream f;

  void scan()
  {
    UI4Bytes i = 0, ci = 0, k=0, endI=0;
    char c;
    
    while(i != fileSize-1)
      {
	endI = i+16;
	for(; i < endI; ++i)
	  {
	    f.get(c); 
	    
	    ci = ci | (UI4Bytes)((c-49) << shiftAmt[i%16]); 
	    if(i == fileSize-1)
	      break;
	  }
	bitArray[k++] = ci;
	ci = 0;
      }
  }
};

} /* namespace libednai_trellis */

#endif
