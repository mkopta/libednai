#include <cstdio>
#include <cstdlib>

namespace libednai_trellis {

using namespace std;

int fast2num_mypst_main(const char *inputfile, const char *outputfile,
    int chars)
{
  char c;
  int skipline = 0;
  FILE *fasta = fopen(inputfile, "r");
  FILE *num   = fopen(outputfile, "w");
  if (!fasta) {
    perror("Unable to open input FASTA file: ");
    return 1;
  }
  if (!num) {
    perror("Unable to create output NUM file: ");
    return 1;
  }
  while ((c = fgetc(fasta)) != EOF) {
    if (skipline) {
      if (c == '\n') skipline = 0;
      continue;
    } 
    switch (c) {
      case '>':
        skipline = 1;
        break;
      case 'a': case 'A':
        fputc((int) '1', num);
        break;
      case 'c': case 'C':
        fputc((int) '2', num);
        break;
      case 'g': case 'G':
        fputc((int) '3', num);
        break;
      case 't': case 'T':
        fputc((int) '4', num);
        break;
      default:
        break;
    }
  }
  fflush(num);
  fclose(num);
  fclose(fasta);
  return 0;
}

} /* namespace libednai_trellis */

