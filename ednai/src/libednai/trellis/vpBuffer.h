#ifndef VP_BUFFER_H
#define VP_BUFFER_H

#include <iostream>
#include <fstream>

namespace libednai_trellis {

using namespace std;

#define MEG 1024 * 1024
#define BUFFER_SIZE 1 * MEG

class VPBuffer
{
 public: 
  VPBuffer(char* fName)
    {
      fileName = fName;
      startIndex = 0;

      f.open(fileName);
      cout << "VPBuffer constructor opens " << fileName << endl;
      cout << "current position = " << f.tellg() << endl;
      f.seekg(0, ios::end);
      fileSize_ = f.tellg();
      --fileSize_;
      f.seekg(0, ios::beg);
   
      f.read(leftBuf, BUFFER_SIZE);
      f.read(rightBuf, BUFFER_SIZE);
    }

  void reset()
    {
      cout << "fileSize_ " << fileSize_ << endl;
      f.clear();
      f.seekg(0, ios::beg);
      
      cout << "resetting vpBuffer, now file.tellg = " << f.tellg() << endl;

      f.read(leftBuf, BUFFER_SIZE);
      f.read(rightBuf, BUFFER_SIZE);
      startIndex = 0;
    }

  int operator[] (UI4Bytes i)
    {
      if(i == 0)
	reset();

      if(i > fileSize_)
	{
	  cout << i << " is out of range " << fileSize_ << endl;
	  exit(0);
	}

      if(i < startIndex || i >= startIndex + (3*BUFFER_SIZE) )
	{
	  cout << "i(" << i << ") must be in the range [" << startIndex << ", " <<
	    startIndex + (3*BUFFER_SIZE) << ")" << endl;
	  exit(0);
	}
      
      if(i < startIndex + BUFFER_SIZE) 
	return leftBuf[i-startIndex]-48;
      else if(i < startIndex + (2*BUFFER_SIZE)) 
	return rightBuf[i-(startIndex+BUFFER_SIZE)]-48;
      else 
	{
	  strcpy(leftBuf, rightBuf);
	  f.read(rightBuf, BUFFER_SIZE);
	  startIndex += BUFFER_SIZE;
	  return rightBuf[i-(startIndex+BUFFER_SIZE)]-48;
	}
    }

  UI4Bytes size() { return fileSize_; }

  ~VPBuffer()
    { 
      if(f.is_open())
	f.close();
    }

 private:
  char leftBuf[BUFFER_SIZE], rightBuf[BUFFER_SIZE];
  UI4Bytes startIndex;
  UI4Bytes fileSize_;
  char *fileName;
  ifstream f;
};

} /* namespace libednai_trellis */

#endif
