//////////////////////////////////////////////////////////////////////////////
//  TRELLIS+ (without String Buffer): A disk-based suffix tree construction
//  for genome-scale DNA sequence indexing
//
//  Copyright (C) 2007  Benjarath Phoophakdee
//
//  This file is a part of TRELLIS+.
//
//  TRELLIS+ is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  any later version.
//
//  TRELLIS+ is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.
//////////////////////////////////////////////////////////////////////////////

#ifndef NODE_H
#define NODE_H

#include <iostream>
#include "Global.h"

namespace libednai_trellis {

using namespace std;

class I_Node;
class L_Node;

class Node
{
 public:
  I_Node* parent;
  UI4Bytes actualStart, start;
  bool isLeaf;
  
  Node()
    {
      parent = NULL;
      actualStart = 0;
      start = 0;
      isLeaf = false;
    }
  virtual ~Node(){}
  
  bool isRoot() const { return parent == NULL; }
  virtual UI4Bytes getEnd() const = 0;
  virtual UI4Bytes getEdgeLength() const = 0;
  virtual UI4Bytes getSuffixLength() const = 0;
  virtual void print() const = 0;
};

class I_Node: public Node
{
 public:
  I_Node* suffixLink;
  Node* children[NUMCHAR+1]; 
  UI4Bytes end;
  
  I_Node()
    {
      suffixLink = NULL;
      for(UI4Bytes i = NUMCHAR+1; i--;)
	children[i] = NULL;
    }
  ~I_Node(){}

  UI4Bytes getEnd() const { return end; }
  UI4Bytes getEdgeLength() const { return end - start + 1;}
  UI4Bytes getSuffixLength() const { return end - actualStart + 1;}  
  void print() const { cout << "Internal: " << actualStart << "[" << start << ", " << getEnd() << "]" << endl; }
  
  void printChildren() const
    {
      cout << "Children of: ";
      print();
      for(UI4Bytes i = NUMCHAR+1; i--;)
	if(children[i])
	  {
	    cout << "i = " << i << " ";
	    children[i]->print();
	  }
	    
    }

  void addChild(Node* node, int index)
    {  
      if(index == -48)
	index = 0;

      if(children[index] != NULL)
	{
	  cout << "Error: ";
	  cout << "Trying to add child ";
	  node->print();
	  cout << " but ";
	  print();
	  cout << " already has a child beginning at index " << index << endl;
	  exit(0);
	}
      
      children[index] = node;
    }

  void removeChild(const UI4Bytes & i)
    {
      if(children[i] == NULL)
	{
	  cout << "Error: ";
	  cout << "Trying to remove and empty child at index " << i << endl;
	  exit(0);
	}
      children[i] = NULL;
    }
     
  Node* hasOneChild() const
    {
      Node* child = NULL;
      int c = 0;
      for(UI4Bytes i = NUMCHAR+1; --i;)
	if(children[i]!=NULL)
	  {
	    child = children[i];
	    ++c;
	    if(c > 1)
	      return false;
	  }

      return child;
    }  
};

class L_Node: public Node
{
 public:
  L_Node(){ isLeaf = true; }
  ~L_Node(){}

  UI4Bytes getEdgeLength() const { return currentEnd - start + 1;}
  UI4Bytes getSuffixLength() const { return currentEnd - actualStart + 1;}  
  UI4Bytes getEnd() const { return currentEnd; }
  void print() const { cout << "Leaf: " << actualStart << "[" << start << ", " << getEnd() << "]" << endl; }
};

} /* namespace libednai_trellis */

#endif

