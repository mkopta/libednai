//////////////////////////////////////////////////////////////////////////////
//  TRELLIS+ (without String Buffer): A disk-based suffix tree construction 
//  for genome-scale DNA sequence indexing
//
//  Copyright (C) 2007  Benjarath Phoophakdee
//
//  This file is a part of TRELLIS+.
//
//  TRELLIS+ is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  any later version.
//
//  TRELLIS+ is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.
//////////////////////////////////////////////////////////////////////////////


#include <fstream>
#include <iostream>
#include <vector>
#include <string>
#include <map>
#include <sstream>
#include <algorithm>
#include <cassert>
#include <cstdlib>
#include <cstring>
#include "BitString.h"
#include "TimeTracker.h"
#include <deque>

namespace libednai_trellis {

using namespace std;

#define ISIZE_NOLINK 28
#define ISIZE_LINK 32
#define NUMCHAR 4
#define UI4SIZE 4

typedef unsigned int UI4Bytes;

UI4Bytes addsflinks_threshold=0;
vector<string> addsflinks_prefVec;
map<string, int> addsflinks_pmap;
ifstream leafFile;
BitString inputStr;
string sfFilePath, leafFilePath;
string curFileName = "";
UI4Bytes mainBufSize = 0, z, nz, call=0;
char *mainBuf = NULL, *sfBuf = NULL;

void popPrefVec(char* pfileName)
{
  ifstream pfile(pfileName);
  string prefix;
  while(getline(pfile, prefix))
    {
      addsflinks_pmap[prefix] = addsflinks_prefVec.size();
      addsflinks_prefVec.push_back(prefix);
      getline(pfile, prefix);
    }
  sort(addsflinks_prefVec.begin(), addsflinks_prefVec.end());
  cout << "There are " << addsflinks_prefVec.size() << " prefixes." << endl;
  pfile.close();
}
 
void addZeroBuf(ifstream & f)
{
  f.seekg(0, ios::end);
  UI4Bytes fSize = f.tellg();
  f.seekg(0);
    
  if(mainBuf != NULL)
    delete [] mainBuf;
     
  assert(fSize%ISIZE_NOLINK == 0); 
  UI4Bytes numInternal = fSize/ISIZE_NOLINK, nc = 0, pos=0; 
  mainBufSize = numInternal*ISIZE_LINK;
  mainBuf = new char[mainBufSize]; 
  char smallBuf[ISIZE_NOLINK]; 
  UI4Bytes zero = 0;
  while(nc < numInternal)
    {
      f.read(smallBuf, ISIZE_NOLINK); 
      memcpy(mainBuf+pos, smallBuf, ISIZE_NOLINK);
      memcpy(mainBuf+pos+ISIZE_NOLINK, &zero, UI4SIZE);
      pos+=ISIZE_LINK;
      ++nc;
    }
  assert(pos == (numInternal*ISIZE_LINK));
}

void popIn(char* treeBuf, const UI4Bytes & ID, UI4Bytes & s, UI4Bytes & e, 
	   UI4Bytes * children, const UI4Bytes & nodeSize)
{
  assert(nodeSize == ISIZE_NOLINK || nodeSize == ISIZE_LINK);

  UI4Bytes pos = (ID == 0 ? 0 : (ID-(addsflinks_threshold+1))*nodeSize);
  
  memcpy(&s, treeBuf+pos, UI4SIZE);
  pos+=UI4SIZE;
  memcpy(&e, treeBuf+pos, UI4SIZE);
  pos+=UI4SIZE;
  for(UI4Bytes i = 0; i <= NUMCHAR; ++i)
    {
      memcpy(&children[i], treeBuf+pos, UI4SIZE);
      pos+=UI4SIZE;
    }
}

void popLeaf(ifstream & lFile, const UI4Bytes & ID, UI4Bytes & s, UI4Bytes & as)
{
  lFile.seekg((ID-1)*2*UI4SIZE);
  lFile.read((char*) & s, UI4SIZE);
  lFile.read((char*) & as, UI4SIZE);
}

void printLeaf(const UI4Bytes & ID, UI4Bytes & s, UI4Bytes & as)
{
  cout << "Leaf at ID " << ID << " as " << as << " s " << s << endl;
}
  
UI4Bytes getSfID(const UI4Bytes & sfpID, const UI4Bytes & nStart, 
		 const UI4Bytes & nEnd, const UI4Bytes & nID)
{   
  UI4Bytes id = sfpID, s, e, c[NUMCHAR+1];
  UI4Bytes tempStart = nStart;

  popIn(sfBuf, id, s, e, c, ISIZE_NOLINK);
  
  while(true)
    {
      id = c[inputStr[tempStart]-48];
      
      if(id>addsflinks_threshold) 
	{
	  popIn(sfBuf, id, s, e, c, ISIZE_NOLINK); 

	  tempStart += (e-s+1); 
	  if(tempStart > nEnd)
	    { 
	      assert(tempStart == nEnd+1); 
	      break;
	    }
	}
      else 
	{
	  UI4Bytes as1, s1;
	  popLeaf(leafFile, id, s1, as1);
	  cout << "Found Leaf at ID " << id << " as " << as1 << " [" << s1 << ", " << inputStr.size() << "]" << endl;
	  cout << curFileName << endl;
	  popIn(sfBuf, nID, s, e, c, ISIZE_NOLINK);
	  for(UI4Bytes k = 0; k <= NUMCHAR; ++k)
	    {
	      assert(c[k] <= addsflinks_threshold);
	    }
	  id = 0;
	  break;
	}
    }

  return id;
}

UI4Bytes search(const string & nString, const UI4Bytes & sfPrefSize)
{
  UI4Bytes i = sfPrefSize, s=0, e=0, c[NUMCHAR+1];

  popIn(sfBuf, 0, s, e, c, ISIZE_NOLINK);

  UI4Bytes lastID=0;

  while(i<nString.size())
    {
      if(c[nString[i]-48] <= addsflinks_threshold)
	{
	  cout << c[nString[i]-48] << " > " << addsflinks_threshold << endl;
	  exit(0);
	}
      
      lastID = c[nString[i]-48];
      popIn(sfBuf, lastID, s, e, c, ISIZE_NOLINK);

      for(UI4Bytes j = s; j <= e; ++j)
	{
	  assert(inputStr[j] == nString[i]);

	  ++i;
	  if(i == nString.size())
	    {
	      if(j!=e)
		{
		  cout << j << " != " << e << endl;
		  exit(0);
		}
	      
	      break;
	    }
	}
    }

  return lastID;
}

void addSuffixLink(const UI4Bytes & nID, const UI4Bytes & sfpID, string pString)
{ 
  ++call;

  UI4Bytes s, e, c[NUMCHAR+4], sfnID;
   
  popIn(mainBuf, nID, s, e, c, ISIZE_LINK); 

  if(sfpID == 0) 
    {
      bool found = false;
      string sfPrefix;

      for(UI4Bytes i = 1; i <= pString.size()-1; ++i)
	{
	  sfPrefix = pString.substr(1, i);
	  if(binary_search(addsflinks_prefVec.begin(), addsflinks_prefVec.end(), sfPrefix))
	    {
	      found = true;
	      break;
	    }
	}
      
      if(!found)
	{
	  sfPrefix = pString.substr(1, pString.size()-1);
	  for(UI4Bytes i = s; i <= e; ++i)
	    {
	      sfPrefix += inputStr[i];
	      if(binary_search(addsflinks_prefVec.begin(), addsflinks_prefVec.end(), sfPrefix))
		{
		  found = true;
		  break;
		}
	    }
	}

      if(found)
	{	
	  string sfFileName = sfFilePath;
	  stringstream ss;
	  ss << addsflinks_pmap[sfPrefix];
	  sfFileName += ss.str();
	   
	  string leafFileName = leafFilePath;
	  leafFileName += "L_";
	  leafFileName += sfPrefix;

	  if(curFileName != sfFileName)
	    {
	      curFileName = sfFileName;
	      cout << "Suffix: " << sfPrefix << " or file " << sfFileName << endl;
	      ifstream sfFile1(sfFileName.c_str());
	      TimeTracker tt; tt.Start();
	      sfFile1.seekg(0, ios::end);
	      UI4Bytes sfFileSize = sfFile1.tellg();
	      sfFile1.seekg(0);
	      if(sfBuf != NULL)
		delete []sfBuf;
	      sfBuf = new char[sfFileSize]; 

	      sfFile1.read(sfBuf, sfFileSize);  

	      sfFile1.close(); 
	      
	      if(leafFile.is_open())
		leafFile.close(); 
	      leafFile.open(leafFileName.c_str()); 
	    }

	  string nString = pString.substr(1, pString.size()-1);
	  nString += inputStr.substr(s, e-s+1);

	  if(sfPrefix.size() == nString.size())
	    sfnID = 0;
	  else
	    sfnID = search(nString, sfPrefix.size());
	}
      else
	sfnID = 0;
    }
  else
    sfnID = getSfID(sfpID, s, e, nID);
    
  if(sfnID > 0)
    {
      ++nz;
      
      UI4Bytes shift = 0;
      if(nID > 0)
	shift = ((nID-addsflinks_threshold-1)*ISIZE_LINK)+28;
      
      memcpy(mainBuf+shift, &sfnID, UI4SIZE);
    }
  else
    ++z;

  if(sfnID == 0)
    pString += inputStr.substr(s, e-s+1);
  
  string origPString = pString;
  
  for(int i = 1; i <= NUMCHAR; ++i)
    {
      pString = origPString;
      if(c[i] > addsflinks_threshold) 
	{ 
	  if(sfnID != 0)
	    {
	      string dumbo;
	      addSuffixLink(c[i], sfnID, dumbo); 
	    }
	  else
	    addSuffixLink(c[i], sfnID, pString);
	}

    }
}

int AddSFLinks_main(const char *tempDirPath, const char *prefixFileName,
    unsigned threshold, const char *inputfile)
{ 
  UI4Bytes totalZ = 0;

  TimeTracker totalTime;
  totalTime.Start();
  
  string pFileName(tempDirPath);
  popPrefVec(const_cast<char *> (prefixFileName));
  addsflinks_threshold = threshold;
  
  TimeTracker loadTime; loadTime.Start();
  cout << "Loading the input string... " << flush;
  inputStr.init(inputfile);
  cout << "Finish! Time taken = " << loadTime.Stop() << " seconds." << endl;

  if(pFileName[pFileName.size()-1] != '/')
    pFileName += 'L';
  else
    pFileName[pFileName.size()-1] = 'L';
  
  UI4Bytes s, e, c[NUMCHAR+1]; 
  string cc = "rm -rf " + pFileName;
  if (system(cc.c_str())) abort();
  pFileName = "mkdir " + pFileName;
  if (system(pFileName.c_str())) abort();
  
  TimeTracker tt;

  for(UI4Bytes i = 0; i < addsflinks_prefVec.size(); ++i)
    {
      tt.Start(); 
      cout << "------------ Prefix " << addsflinks_prefVec[i] << " ---------------- " << endl;
      string pFileName(tempDirPath), newPFileName(tempDirPath);

      if(pFileName[pFileName.size()-1] != '/')
      pFileName += '/';

      if(newPFileName[newPFileName.size()-1] == '/')
	{
	  newPFileName[newPFileName.size()-1] = 'L';
	  newPFileName += '/';
	}
      else
	newPFileName += "L/";

      if(sfFilePath.empty())
	sfFilePath = pFileName;

      stringstream ss;
      ss << addsflinks_pmap[addsflinks_prefVec[i]];
     
      pFileName += ss.str();
      newPFileName += ss.str();

      cout << "read from " << pFileName << " and write to " << newPFileName << endl;

      ifstream inF(pFileName.c_str(), fstream::binary);
      addZeroBuf(inF);
      inF.close();
 
      popIn(mainBuf, 0, s, e, c, ISIZE_LINK);
      call = 0;
      nz = 0; z=0;
      for(UI4Bytes j = 1; j <= NUMCHAR; ++j)
	{
	  if(c[j] > addsflinks_threshold)
	    {
	      string pString = addsflinks_prefVec[i]; 
	      addSuffixLink(c[j], 0, pString);
	      //cout << "nz " << nz << " and " << z << endl;
	      //cout << "call " << call << endl;
	      totalZ += z;
	    }
	}

      ofstream outF(newPFileName.c_str()); 
      outF.write(mainBuf, mainBufSize);
      outF.close();
      
      cout << "Time taken = " << tt.Stop() << " seconds." << endl; 
    }

  double tStop = totalTime.Stop();
  cout << "Total time for adding suffix links = " << tStop << " seconds OR " << tStop/60.0 << " minutes." << endl;
  return 0;  
}

} /* namespace libednai_trellis */
