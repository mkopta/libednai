//////////////////////////////////////////////////////////////////////////////
//  TRELLIS+ (without String Buffer): A disk-based suffix tree construction
//  for genome-scale DNA sequence indexing
//
//  Copyright (C) 2007  Benjarath Phoophakdee
//
//  This file is a part of TRELLIS+.
//
//  TRELLIS+ is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  any later version.
//
//  TRELLIS+ is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.
//////////////////////////////////////////////////////////////////////////////

#ifndef PREFIX_GEN_H
#define PREFIX_GEN_H

#include <vector>
#include <iostream>
#include <fstream>
#include <math.h>
#include <map>
#include <algorithm>

#include "Global.h"
#include "vpBuffer.h"

namespace libednai_trellis {
  
using namespace std;

VPBuffer *vpb;

void showPrefInfo()
{
  ofstream freqF("freqF");
  ofstream prefF("prefF");

  cout << " ******** Show Prefix Info ******** " << endl;
  UI4Bytes more, sum;
  int i = 1, c;
  for(vector<vector<pair<PREF, UI4Bytes> > >::iterator len = prefInfo.begin();
      len != prefInfo.end(); ++len)
    {  
      cout << "\nPrefix Length = " << i << endl;
      freqF << "\nPrefix Length = " << i << endl;
      prefF << "\nPrefix Length = " << i << endl;
      more = 0;
      sum = 0;
      c = 1;
      for(vector<pair<string, UI4Bytes> >::iterator p = len->begin(); p != len->end(); ++p)
        {
	  sum += p->second;
	  prefF << p->first << endl;
	  freqF << p->second << endl;
          
	  if(p->second > threshold)
	    {
	      ++more;
	      cout << c << ") " << p->first << "\t" << p->second << "  *****" << endl;
	    }
	  else
	    cout << c << ") " << p->first << "\t" << p->second << endl;
	  ++c;
	}
      cout << "Total = " << sum << endl;
      cout << more << " out of " << len->size() << " prefixes of length " << i << " > " << threshold << endl;
      
      ++i;
    }

}
 
void showPrefInfo(const vector<vector<pair<string, UI4Bytes> > > & pInfo)
{
  cout << " ******** Show Prefix Info ******** " << endl;
  cout << "there are " << pInfo.size() << " levels" << endl;
  UI4Bytes more;
  int c; 
  for(vector<vector<pair<PREF, UI4Bytes> > >::const_iterator len = pInfo.begin();
      len != pInfo.end(); ++len)
    {  
      cout << "\nPrefix Length = " << len->front().first.size() << endl;
      more = 0;
      c = 1;
      for(vector<pair<string, UI4Bytes> >::const_iterator p = len->begin(); p != len->end(); ++p)
        {
	  if(p->second > threshold)
	    {
	      ++more;
	      cout << c << ") " << p->first << "\t" << p->second << "  *****" << endl;
	    }
	  else
	    cout << c << ") " << p->first << "\t" << p->second << endl;
	  ++c;
	}
      cout << more << " out of " << len->size() << " prefixes of length " <<
	len->front().first.size() << " > " << threshold << endl;
    }
}

void showPrefStat()
{
  ofstream fFile("fFile");
  ofstream aFile("aFile");
  ofstream cFile("cFile");
  ofstream gFile("gFile");
  ofstream tFile("tFile");

  int aCount, cCount, gCount, tCount;
  for(vector<vector<pair<PREF, UI4Bytes> > >::iterator len = prefInfo.begin();
      len != prefInfo.end(); ++len)
    { 
      for(vector<pair<string, UI4Bytes> >::iterator p = len->begin(); p != len->end(); ++p)
        {
          if(p->first.size() == maxPrefLen)
	    {
	      aCount = 0; cCount = 0; gCount = 0; tCount = 0;

	      fFile << p->second << endl;
	      for(UI4Bytes i = 0; i < p->first.size(); ++i)
		{
		  if(p->first[i] == '1') ++aCount;
		  else if(p->first[i] == '2') ++cCount;
		  else if(p->first[i] == '3') ++gCount;
		  else ++tCount;
		}
	      aFile << (double)aCount/(double)p->first.size() << endl;
	      cFile << (double)cCount/(double)p->first.size() << endl;
	      gFile << (double)gCount/(double)p->first.size() << endl;
	      tFile << (double)tCount/(double)p->first.size() << endl;
	    }
	}
    }
  
  fFile.close();
  aFile.close();
  cFile.close();
  gFile.close();
  tFile.close();
}

bool nextPref(string & s)
{
  int j = (int)s.size()-1;
  while(j >= 0)
    {
      if(s[j] < 52) 
        {
          s[j] += 1;
          return true;
        }
      else 
        s[j] = '1';
      --j;
    }
  return false;
}

void genPrefVec(int len)
{ 
  cout << "Entering genPrefVec " << endl;
  TimeTracker tt;
  double tStop;
  tt.Start();

  vector<PREF> start;
  PREF s = "";
  for(int i = 0; i < len; ++i)
    {
      s += '1';
      start.push_back(s);
    }
  
  PREF pref;
  
  for(int i = 0; i < len; ++i)
    {
      pref = start[i];
      vector<pair<string, UI4Bytes> > v;
      prefInfo.push_back(v);
      prefInfo[i].push_back(make_pair(pref, 0));

      while(nextPref(pref))
        prefInfo[i].push_back(make_pair(pref, 0));
    }
   
  tStop = tt.Stop();
  cout << "Time taken for genPrefVec = " << tStop/60.0 << " minutes or " << tStop << " seconds.\n" << endl;
}
  
void genPrefVec(const vector<PREF> & pv, UI4Bytes maxLen, vector<vector<pair<PREF, UI4Bytes> > > & tempPrefInfo)
{
  cout << "Entering genPrefVec next levels" << endl;
  double tStop;
  TimeTracker tt;
  tt.Start();

  UI4Bytes lenDone = pv.front().size();

  if(pv.empty() || lenDone >= maxLen)
    return;
  
  cout << "lenDone = " << lenDone << " maxLen = " << maxLen << " we need " << maxLen - lenDone << " more levels" << endl;
  while(lenDone < maxLen)
    {
      ++lenDone;
      vector<pair<PREF, UI4Bytes> > v;
      tempPrefInfo.push_back(v);
    }

  cout << "We got " << tempPrefInfo.size() << " levels" << endl;
  cout << "Extending first level" << endl;
  
  for(vector<PREF>::const_iterator pit = pv.begin(); pit != pv.end(); ++pit)
    for(int i = 1; i <= NUMCHAR; ++i)
      {
	tempPrefInfo[0].push_back(make_pair(*pit + i2c(i), 0)); 
      }

  lenDone = pv.front().size() + 1;
  
  UI4Bytes j = 0;
  int c = 0;
  while(lenDone < maxLen)
    {
      c = 0;
      ++lenDone;
      cout << "Now extending level " << lenDone <<  " maxLen = " << maxLen << flush;
      for(vector<pair<PREF, UI4Bytes> >::iterator lit = tempPrefInfo[j].begin();
	  lit != tempPrefInfo[j].end(); ++lit)
	for(int i = 1; i <= NUMCHAR; ++i)
	  {
	    ++c;
	    tempPrefInfo[j+1].push_back(make_pair(lit->first + i2c(i), 0));
	  }
      cout << " #pushed back = " << c << endl;
      ++j;
    }
  
  tStop = tt.Stop();
  cout << "Time taken for genPrefVec = " << tStop/60.0 << " minutes or " << tStop << " seconds.\n" << endl;
}
 
void countPrefFreq()
{
  cout << "Entering countPrefFreq" << endl;
  double tStop;
  TimeTracker tt;
  tt.Start();

  UI4Bytes sSize = vpb->size();
  UI4Bytes levI;
  UI4Bytes lastPos = sSize - maxPrefLen;
  for(UI4Bytes i = 0; i <= lastPos; ++i)
    {
      /*
      if(i % 10000000 == 0 && i != 0)
	{ 
	  UI4Bytes totalSize = 0;
	  cout << " i = " << i/10000000 << " millions\t" << flush; 
	}
      */
      levI = (*vpb)[i]-1;
      ++prefInfo[0][levI].second;
      
      for(UI4Bytes j = i+1; j < i+maxPrefLen; ++j)
	{
	  levI = (levI << 2) + ((*vpb)[j]-1);
	  ++prefInfo[j-i][levI].second;
	}
    }
  
  tStop = tt.Stop();
  cout << "Time taken for countPrefFreq = " << tStop/60.0 << " minutes or " << tStop << " seconds.\n" << endl;
}
 
void expand(vector<vector<pair<PREF, UI4Bytes> > > * pInfo, UI4Bytes i, UI4Bytes j)
{
  ++i;
  j = (j << 2);

  for(int k = 0; k < NUMCHAR; ++k)
    {
      if((*pInfo)[i][j+k].second <= threshold)
	{
	  prefVec.push_back((*pInfo)[i][j+k].first);
	  freqVec.push_back((*pInfo)[i][j+k].second);
	}
      else if((i+1) != pInfo->size())
	expand(pInfo, i, j+k);
    }
}

void generateDynamicPrefixes(vector<vector<pair<PREF, UI4Bytes> > > * pInfo)
{
  cout << "Entering generateDynamicPrefixes ptr" << endl;
  double tStop;
  TimeTracker tt;
  tt.Start();

  int firstLen = pInfo->front().back().first.size();
  int lastLen = pInfo->back().back().first.size();
  
  cout << "Generating variable-length prefixes from length " << firstLen << " to " << lastLen << endl;

  for(UI4Bytes i = 0; i < (*pInfo)[0].size(); ++i)
    {
      if((*pInfo)[0][i].second <= threshold)
	{
	  prefVec.push_back((*pInfo)[0][i].first);
	  freqVec.push_back((*pInfo)[0][i].second);
	}
      else
	expand(pInfo, 0, i);
    }

  tStop = tt.Stop();
  cout << "Time taken for generateDynamicPrefixes = " << tStop/60.0 << " minutes or " << tStop << " seconds.\n" << endl;
}
 
class pairLess
{
 public: 
  bool operator()(const pair<int, UI4Bytes> & p1, const pair<int, UI4Bytes> & p2)
    {
      return p1.first < p2.first;
    }
};

class prefLess
{
 public:
  bool operator()(const PREF & p1, const PREF & p2)
    {
      if(p1.size() < p2.size())
	return true;

      if(p1.size() > p2.size())
	return false;

      return p1 < p2;
    }
};

bool extendPrefixLevels(UI4Bytes toLen, vector<vector<pair<PREF, UI4Bytes> > > * lastPInfo,
			vector<vector<pair<PREF, UI4Bytes> > > * newPInfo)
{
  cout << "Entering extendPrefixLevels to length " << toLen << endl;
  double tStop;
  TimeTracker tt;
  tt.Start();

  UI4Bytes lastLen = lastPInfo->back().back().first.size();
  if(lastLen >= toLen)
    return false;

  vector<pair<int, UI4Bytes> > npCodeVec; 
  vector<PREF> npStrVec;

  int ci = 0; 
  for(UI4Bytes i = 0; i < lastPInfo->back().size(); ++i)
    {
      if((lastPInfo->back())[i].second > threshold)
      { 
	npCodeVec.push_back(make_pair(getPrefCode(lastPInfo->back()[i].first), ci));
	npStrVec.push_back( (lastPInfo->back())[i].first);
	++ci;
      }
    } 

  sort(npCodeVec.begin(), npCodeVec.end(), pairLess()); 
  cout << npCodeVec.size() << " prefixes didn't pass" << endl;
  
  tStop = tt.Stop();
  cout << "After sort, time taken = " << tStop/60.0 << " mins or " << tStop << " secs." << endl;

  tt.Start();
  genPrefVec(npStrVec, toLen, *newPInfo); 
     
  cout << "npCodeVec is " << endl;
  for(vector<pair<int, UI4Bytes> >::iterator ii = npCodeVec.begin(); ii != npCodeVec.end(); ++ii)
    cout << ii->first << ", " << ii->second << endl;
  cout << "---------------------------------" << endl;
  
  cout << "npStrVec is " << endl;
  for(vector<PREF>::iterator ii = npStrVec.begin(); ii != npStrVec.end(); ++ii)
    cout << *ii << endl;
  cout << "---------------------------------" << endl;
 
  npStrVec.clear(); 

  UI4Bytes sSize = vpb->size(), diff = sSize - toLen;
  int pcode = 0, levI, prevVal;
  pair<vector<pair<int, UI4Bytes> >::iterator, vector<pair<int, UI4Bytes> >::iterator> rp;
  
  prevVal = (*vpb)[0]-1;
  for(UI4Bytes j = 0; j < lastLen; ++j)
    pcode += ((*vpb)[j]-1) * (int)pow((double)NUMCHAR, (int)j);
  
  for(UI4Bytes i = 1; i <= diff; ++i)
    {  
      if(i % 10000000 == 0 && i != 0)
	cout << " i = " << i/10000000 << " 10MB\t" << flush;
  
      pcode = ((pcode - prevVal)/4) + (((*vpb)[i+lastLen-1]-1) * (int)pow((double)NUMCHAR, (int)lastLen-1));
      prevVal = (*vpb)[i]-1;

      rp = equal_range(npCodeVec.begin(), npCodeVec.end(), make_pair(pcode, 0), pairLess());
 
      if(npCodeVec[rp.first - npCodeVec.begin()].first != pcode)
	continue;
     
      levI = ((npCodeVec[rp.first - npCodeVec.begin()].second) * 4 ) + (*vpb)[i+lastLen] - 1;
      ++(*newPInfo)[0][levI].second;
      
      for(UI4Bytes j = 1; j <= toLen - lastLen - 1; ++j)
	{ 
	  levI = (levI * 4 ) + (*vpb)[i+lastLen+j] - 1;
	  ++(*newPInfo)[j][levI].second;
	}
    }

  tStop = tt.Stop();
  cout << "Time taken for looping in extendPrefLevels = " << tStop/60.0 << " minutes or " << tStop << " seconds" << endl;
   
  for(vector<pair<PREF, UI4Bytes> >::iterator fit = newPInfo->back().begin();
      fit != newPInfo->back().end(); ++fit)
    { 
      if(fit->second > threshold) 
	return true;
    }

  return false;
}

void generateAllDynamicPrefixes()
{
  vpb = new VPBuffer(inputFileName);

  int jump = 4;
  cout << "Entering generateAllDynamicPrefixes" << endl;

  TimeTracker tt;
  double tStop;
  tt.Start();

  cout << "Computing the starting values..." << endl;
  genPrefVec(maxPrefLen); 
  countPrefFreq(); 
  
  generateDynamicPrefixes(&prefInfo);
  cout << "Finished computing the starting values" << endl;

  vector<vector<pair<PREF, UI4Bytes> > > *lastPrefInfo = &prefInfo;
  vector<vector<pair<PREF, UI4Bytes> > > *newPrefInfo = new vector<vector<pair<PREF, UI4Bytes> > >;
  int toLen = maxPrefLen + jump; 

  bool c = false;
  for(UI4Bytes i = 0; i < prefInfo.back().size(); ++i)
    if((prefInfo.back())[i].second > threshold)
      c = true;

  if(c)
    {
      cout << " ======================= Extend 1 ========================= " << endl;
      c = extendPrefixLevels(toLen, lastPrefInfo, newPrefInfo);
      generateDynamicPrefixes(newPrefInfo);
      prefInfo.clear();
      toLen += jump;
      
      while(c)
	{
	  lastPrefInfo = newPrefInfo; 
	  newPrefInfo = new vector<vector<pair<PREF, UI4Bytes> > >;
	  	  
	  cout << " ******************* Extend more to len " << toLen << " *********************** " << endl;
	  
	  c = extendPrefixLevels(toLen, lastPrefInfo, newPrefInfo);
	  generateDynamicPrefixes(newPrefInfo);
	  toLen += jump;

	  delete lastPrefInfo;
	}
      delete newPrefInfo;
    }
  
  delete vpb;
  tStop = tt.Stop();
  cout << "Time taken for generateAllDynamicPrefixes = " << tStop/60.0 << " minutes or " << tStop << " seconds" << endl;

  ofstream pfile(prefixFileName);
  cout << "The variable-length prefixes are" << endl;
  sort(prefVec.begin(), prefVec.end(), prefLess());
  for(int i = 0; i < (int)prefVec.size(); ++i)
    { 
      pfile << prefVec[i] << endl;
      pfile << freqVec[i] << endl;

      cout << i+1 << ") " << prefVec[i];
      if(i%5 == 4)
        cout << endl;
      else
        cout << "  ";
    }
  cout << endl;
  
}

void readPrefixes(char* fileName)
{
  ifstream f(fileName);
  string line;
     
  while(getline(f, line))
    {
      prefVec.push_back(line);
      getline(f, line);
      freqVec.push_back(atoi(line.c_str()));
    }

  cout << prefVec.size() << " prefixes read from file " << fileName << "." << endl;
}

} /* namespace libednai_trellis */

#endif
