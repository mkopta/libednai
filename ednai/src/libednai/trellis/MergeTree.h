//////////////////////////////////////////////////////////////////////////////
//  TRELLIS+ (without String Buffer): A disk-based suffix tree construction
//  for genome-scale DNA sequence indexing
//
//  Copyright (C) 2007  Benjarath Phoophakdee
//
//  This file is a part of TRELLIS+.
//
//  TRELLIS+ is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  any later version.
//
//  TRELLIS+ is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.
//////////////////////////////////////////////////////////////////////////////

#ifndef MERGETREE_H
#define MERGETREE_H

#include <fstream>
#include <iostream>
#include <vector>
#include <string>
#include "BitString.h"
#include "LoadedNode.h"
#include "TimeTracker.h"
  
namespace libednai_trellis {

using namespace std;

typedef LoadedNode MNODE;
bool printOn = false;

template <class STRING_T>
class MergeTree
{
 public:
  MergeTree(STRING_T *s):myStr(s){}

  void mergeTreeRoots(I_LoadedNode *root1, I_LoadedNode *root2);
  ~MergeTree() {}
  
 private:
  STRING_T *myStr; 
    
  int charAt(UI4Bytes i, MNODE* n)
    {
      if(i==fileSize)
        return 0;

      return (*myStr)[i]-48;
    }

  void check(MNODE *n);
  void cleanUpChildren(I_LoadedNode *node, const UI4Bytes & seqNo);
  void mergeEdge(MNODE *node1, MNODE *node2);

  int getIndex(I_LoadedNode*parent, LoadedNode *node)
    {
      for(UI4Bytes i = NUMCHAR+1; i--; )  
	if(parent->children[i] && ((&(*(parent->children[i]))) == (&(*node))))
	  return i;
      return -1;
    }
};

template <class STRING_T>
void 
MergeTree<STRING_T>::check(MNODE *n)
{
  assert(n->s <= n->e);
}

template <class STRING_T>
void 
MergeTree<STRING_T>::cleanUpChildren(I_LoadedNode *node, const UI4Bytes & seqNo)
{  
  if(node->children[0] == NULL)
    return;

  int index = getIndex(node, node->children[0]); 

  if(index != 0) 
    {  
      if(node->children[index] != NULL)
	mergeEdge(node->children[index], node->children[0]);
      else 
	{
	  node->children[index] = node->children[0];
	  node->children[0] = NULL;
	}
    }
}

template <class STRING_T>
void 
MergeTree<STRING_T>::mergeEdge(MNODE *node1, MNODE *node2)
{  
  UI4Bytes i = node1->s;
  UI4Bytes j = node2->s;
  UI4Bytes iEnd = node1->e;
  UI4Bytes jEnd = node2->e;
 
  assert(i<=iEnd);
  assert(j<=jEnd);
  assert(charAt(i, node1) == charAt(j, node2));
  
  while(true)
    {
      if(i == iEnd && j == jEnd)
	{   
	  I_LoadedNode *node1T = static_cast<I_LoadedNode*>(node1);
	  I_LoadedNode *node2T = static_cast<I_LoadedNode*>(node2);
	  
	  cleanUpChildren(node1T, 0);
	  cleanUpChildren(node2T, 1);
	  
	  for(UI4Bytes c = NUMCHAR+1; c--; )  
	    {   
	      if(node2T->children[c] != NULL)
		{
		  if(node1T->children[c] == NULL)
		    {
		      node1T->children[c] = node2T->children[c];
		      node1T->children[c]->p = node1T; 
		      node2T->children[c] = NULL;
		    }
		  else
		    mergeEdge(node1T->children[c], node2T->children[c]);
		}
	    }
	  assert(node1->s <= node1->e);
	  		  
	  ++invalid;
	  I_LoadedNodeArray[((intptr_t)node2-((intptr_t)&I_LoadedNodeArray[0]))/sizeof(I_LoadedNode)].~I_LoadedNode();
	  valid[((intptr_t)node2-((intptr_t)&I_LoadedNodeArray[0]))/sizeof(I_LoadedNode)] = false;
	  break;
	}

      else if(i < iEnd && j == jEnd)
	{    
	  if(I_LoadedNodeCount == I_LoadedNodeMax)
	    {
	      cout << "I_LoadedNodeCount " << I_LoadedNodeCount << " == I_LoadedNodeMax " 
		   << I_LoadedNodeMax << endl;
	      exit(0);
	    }

	  I_LoadedNode *n = new (&I_LoadedNodeArray[I_LoadedNodeCount++]) I_LoadedNode(node1->s, i); 
	  ++nMergeCreated;
	  
	  I_LoadedNode *parent = static_cast<I_LoadedNode*> (node1->p);
	  	   
	  int tempindex = getIndex(parent, node1);
	  parent->children[tempindex] = NULL;
	  parent->addChild(n, tempindex);
	  n->p = parent;

	  tempindex = charAt(i+1, node1); 
	  node1->s = i+1;

	  n->addChild(node1, tempindex);
	  node1->p = n;
	  
	  I_LoadedNode* node2T = static_cast<I_LoadedNode*> (node2);
	  cleanUpChildren(node2T, 1);
	  
	  for(UI4Bytes c = NUMCHAR+1; c--; )  	  
	    {  
	      if(node2T->children[c] != NULL) 
		{ 
		  if(n->children[c] == NULL) 
		    {
		      n->children[c] = node2T->children[c];
		      n->children[c]->p = n;
		      node2T->children[c] = NULL;
		    }
		  else  
		    mergeEdge(n->children[c], node2T->children[c]);
		}
	    }
	  
	  if(!node1->isLeaf())
	    {
	      I_LoadedNode *node1T = static_cast<I_LoadedNode*>(node1);
	      if(node1T->getNumChildren() == 1)
		{
		  MNODE* child = NULL;
		  for(UI4Bytes i = NUMCHAR+1; i--; )  
		    if(node1T->children[i] != NULL)
		      {
			child = node1T->children[i];
			break;
		      }
		  
		  child->s = node1T->s;
		  I_LoadedNode *parent = static_cast<I_LoadedNode*>(node1T->p);
		  parent->children[tempindex] = NULL;
		  parent->addChild(child, tempindex);
		  child->p = parent;
		  
		  ++invalid;
		  I_LoadedNodeArray[((intptr_t)node1-((intptr_t)&I_LoadedNodeArray[0]))/sizeof(I_LoadedNode)].~I_LoadedNode();
		  valid[((intptr_t)node1-((intptr_t)&I_LoadedNodeArray[0]))/sizeof(I_LoadedNode)] = false;
		}
	    }
	   
	  ++invalid;
	  I_LoadedNodeArray[((intptr_t)node2-((intptr_t)&I_LoadedNodeArray[0]))/sizeof(I_LoadedNode)].~I_LoadedNode();
	  valid[((intptr_t)node2-((intptr_t)&I_LoadedNodeArray[0]))/sizeof(I_LoadedNode)] = false;
	 
	  break;
	}
      else if(i == iEnd && j < jEnd)
	{    
	  node2->p = NULL;
	  node2->s = j+1; 
	  int index = charAt(j+1, node2);
		  
	  I_LoadedNode *node1T = static_cast<I_LoadedNode*>(node1);
	  cleanUpChildren(node1T, 1);
	  	  	   
	  if(node1T->children[index] != NULL)
	    {
	      mergeEdge(node1T->children[index], node2);
	    }
	  else
	    {
	      node2->p = node1T;
	      node1T->addChild(node2, index);
	    }

	  UI4Bytes childIndex = 0;
	  if(node1T->hasOneChild(childIndex))
	    {
	      MNODE* child = node1T->children[childIndex];
	      
	      child->s = node1T->s;
	      I_LoadedNode *parent = static_cast<I_LoadedNode*>(node1T->p);
		    
	      int tempindex = getIndex(parent, node1T); 
	      parent->children[tempindex] = NULL;
	      parent->addChild(child, tempindex);
	      child->p = parent;
	      
	      ++invalid;
	      I_LoadedNodeArray[((intptr_t)node1-((intptr_t)&I_LoadedNodeArray[0]))/sizeof(I_LoadedNode)].~I_LoadedNode();
	      valid[((intptr_t)node1-((intptr_t)&I_LoadedNodeArray[0]))/sizeof(I_LoadedNode)] = false;
	    }  
	  
	  assert(node2->s <= node2->e);
	  
	  break;
	}

      ++i;
      ++j;

      int charI = charAt(i, node1);
      int charJ = charAt(j, node2);
      if(charI != charJ)
	{
	  if(I_LoadedNodeCount == I_LoadedNodeMax)
	    {
	      cout << "I_LoadedNodeCount " << I_LoadedNodeCount << " == I_LoadedNodeMax " 
		   << I_LoadedNodeMax << endl;
	      exit(0);
	    }
	  
	  I_LoadedNode *n = new (&I_LoadedNodeArray[I_LoadedNodeCount++]) I_LoadedNode(node1->s, i-1); 
	  ++nMergeCreated;
	
	  I_LoadedNode *parent = static_cast<I_LoadedNode*> (node1->p);
	  UI4Bytes tempindex = getIndex(parent, node1);
	  parent->children[tempindex] = n;
	  
	  node1->s = i;
	  node1->p = n;
	  node2->s = j;
	  node2->p = n;
	  n->p = parent;
	  n->addChild(node1, charI);
	  n->addChild(node2, charJ);
	  
	  assert(node1->s <= node1->e);
	  assert(node2->s <= node2->e);
	  assert(n->s <= n->e);
	  check(node1); check(node2); check(n);
	  
	  break;
	}
    }
}
 
template <class STRING_T>
void  
MergeTree<STRING_T>::mergeTreeRoots(I_LoadedNode *root1, I_LoadedNode *root2)
{ 
  assert(root1 != NULL);
  assert(root2 != NULL);

  for(UI4Bytes i = NUMCHAR+1; i--; )  
    {
      if(root2->children[i] != NULL)
	{
	  if(root1->children[i] == NULL)
	    {
	      root2->children[i]->p = root1;
	      root1->children[i] = root2->children[i];
	      root2->children[i] = NULL; 
	    }
	  else
	    mergeEdge(root1->children[i], root2->children[i]);
	}
    }

  ++invalid;
  I_LoadedNodeArray[((intptr_t)root2-((intptr_t)&I_LoadedNodeArray[0]))/sizeof(I_LoadedNode)].~I_LoadedNode();
  valid[((intptr_t)root2-((intptr_t)&I_LoadedNodeArray[0]))/sizeof(I_LoadedNode)] = false;
}

} /* namespace libednai_trellis */

#endif
