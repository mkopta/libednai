//////////////////////////////////////////////////////////////////////////////
//  TRELLIS+ (without String Buffer): A disk-based suffix tree construction
//  for genome-scale DNA sequence indexing
//
//  Copyright (C) 2007  Benjarath Phoophakdee
//
//  This file is a part of TRELLIS+.
//
//  TRELLIS+ is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  any later version.
//
//  TRELLIS+ is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.
//////////////////////////////////////////////////////////////////////////////

#ifndef SIMPLE_STR_H
#define SIMPLE_STR_H

#include <iostream>
#include <string>
#include <cstring>
#include <cstdlib>

namespace libednai_trellis {

typedef unsigned int UI4Bytes;

using namespace std;

class SimpleString
{
 public:
  SimpleString(char *ca, UI4Bytes extraLen)
    { 
      own = true;
      UI4Bytes lenCa = strlen(ca);
      cStr = new char[lenCa+extraLen];
      strcpy(cStr, ca);
      cStr[lenCa] = '\0';
      sSize = lenCa; 
    }

  SimpleString(char *ca)
    {
      own = false;
      cStr = ca;
      sSize = strlen(cStr);
    }

  void removeEndChar() 
    {  
      cStr[sSize-1] = '\0';
      sSize = strlen(cStr); 
    }
  
  UI4Bytes size() const { return sSize; }

  void concat(char *cs)
    {
      strcat(cStr, cs);
      sSize = strlen(cStr);
    }
  
  char operator[](int i) const 
    {
      /*
      if(i > strlen(cStr)-1)
	{
	  cerr << "String index out of bound. " << i << " > " << strlen(cStr)-1 << endl;
	  exit(0);
	}
      */

      return cStr[i];
    }
   
  const char* c_str() const { return cStr; }
  
  int toInt(UI4Bytes i) const 
    { 
      return (int)cStr[i]-48;
    }
  
  string substr(UI4Bytes b, UI4Bytes len) const
    {
      if(b+len > strlen(cStr))
	{
	  cerr << "Substring index out of bound. " << b+len << " > " << strlen(cStr) << endl;
	  exit(0);
	}
      
      string s;
      for(UI4Bytes i = b; i < b+len; ++i)
	s += cStr[i];
      return s;
    }

  friend ostream & operator<< (ostream &os, SimpleString & ss)
    {
      os << ss.cStr;
      return os;
    }
  
  ~SimpleString() 
    { 
      if(own)
	delete [] cStr; 
    }

 private:
  char* cStr;
  UI4Bytes sSize;
  bool own;
};

} /* namespace libednai_trellis */

#endif
