/* Copyright (c) 2011, Martin Kopta <martin@kopta.eu>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#ifndef __trellis_h_6564545632686859847353__
#define __trellis_h_6564545632686859847353__

namespace libednai_trellis {
  int fast2num_mypst_main(const char *inputfile, const char *outputfile,
      int chars);
  int Main_main(const char *inputfile, const char *outputdir,
      const char *tmpdir, int memory /*MB*/, int preflen);
  int AddSFLinks_main(const char *tempDirPath, const char *prefixFileName,
      unsigned threshold, const char *inputfile);
  char *trellis_global_dirPath(void);
  char *trellis_global_tempDirPath(void);
  char *trellis_global_prefixFileName(void);
  unsigned trellis_global_threshold(void);
} /* namespace libednai_trellis */

#endif /* __trellis_h_6564545632686859847353__ */

