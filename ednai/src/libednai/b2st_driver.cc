/* Copyright (c) 2011, Martin Kopta <martin@kopta.eu>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include "./libednai.h"
#include "./b2st/b2st.h"

namespace libednai {
using namespace libednai_b2st;

int b2st_driver::create_index(string inputfile, string outputdir,
  string tmpdir, int memory /*MB*/, int maxstringsize,
  int memforinbuffers /*MB*/, int memforouttree /*MB*/) {

  int retval = 0;
  int memforinputbuffers = memforinbuffers * 1024 * 1024; /* bytes */
  int memoryforoutputtree = memforouttree * 1024 * 1024; /* bytes */
  const char *prefix = "b2st";

  cout << "+-------------------------------------+" << endl;
  cout << "| Creating index using B2ST algorithm |" << endl;
  cout << "+-------------------------------------+" << endl;

  cout << "* Phase 1: preprocessing input" << endl;
  retval = preprocessing_main(inputfile.c_str(), prefix,
      outputdir.c_str(), tmpdir.c_str(), memory);
  if (retval) return 1;
  cout << "[Phase 1 done]" << endl;

  cout << "* Phase 2: sorting strings" << endl;
  retval = string_suffix_sorting_main(outputdir.c_str(), tmpdir.c_str(),
      prefix);
  if (retval) return 1;
  cout << "[Phase 2 done]" << endl;

  cout << "* Phase 3: sorting pairs" << endl;
  retval = pairwise_sorting_main(outputdir.c_str(), tmpdir.c_str(), prefix,
      memforinputbuffers);
  if (retval) return 1;
  cout << "[Phase 3 done]" << endl;

  cout << "* Phase 4: sorting pairs" << endl;
  retval = final_merge_main(tmpdir.c_str(), prefix, outputdir.c_str(), prefix,
      memforinputbuffers, memoryforoutputtree);
  if (retval) return 1;
  cout << "[Phase 4 done]" << endl;

  return 0;
}

} /* namespace libednai */
