/* Copyright (c) 2011, Martin Kopta <martin@kopta.eu>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#ifndef __args_h_9851723894047738490234857_
#define __args_h_9851723894047738490234857_

#include "./general.h"
#include <getopt.h>
#include <cstdlib>

namespace libednai {

class args {
  private:
    int help;
    int usage;
    int version;
    string action;
    string algorithm;
    string inputfile;
    string outputdir;
    string tmpdir;
    int memory; /* in MB */
    int time;
    int preflen;
    int maxstringsize;
    int memforinbuffers;
    int memforouttree;
    int argc;
    char** argv;

  public:
    args(int argc, char **argv);
    int read();
    int is_help_set();
    int is_usage_set();
    int is_version_set();
    int is_action_set();
    int is_algorithm_set();
    int is_inputfile_set();
    int is_outputdir_set();
    int is_tmpdir_set();
    int is_time_set();
    int is_memory_set();
    int is_preflen_set();
    int is_maxstringsize_set();
    int is_memforinbuffers_set();
    int is_memforouttree_set();
    string get_action();
    string get_algorithm();
    string get_inputfile();
    string get_outputdir();
    string get_tmpdir();
    int get_memory();
    int get_preflen();
    int get_maxstringsize();
    int get_memforinbuffers();
    int get_memforouttree();
};

} /* namespace libednai */

#endif /* __args_h_9851723894047738490234857_ */
