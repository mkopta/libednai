/* Copyright (c) 2011, Martin Kopta <martin@kopta.eu>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE. */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define LINEWIDTH 80

int main(int argc, char **argv) {
  char dna[] = {'a', 'c', 'g', 't'};
  long long int length = 0;
  int linesize = 0, break_at = 0, break_counter = 0;

  if (argc < 2) {
    printf("Usage: %s <length> [<break>]\n", *argv);
    return 0;
  }
  length = atoll(*++argv);
  if (length < 1) {
    printf("Length has to be > 0\n");
    return 1;
  }
  if (argc == 3) {
    break_at = atoi(*++argv);
    break_counter = break_at;
  }
  printf("> Random DNA sequence of length %lli\n", length);
  srand((unsigned int) time(NULL));
  while (length --> 0) {
    printf("%c", dna[rand() % 4]);
    if (++linesize >= LINEWIDTH) {
      printf("\n");
      linesize = 0;
    }
    if (break_at > 0 && --break_counter == 0) {
      if (linesize > 0) {
        printf("\n");
        linesize = 0;
      }
      printf("> Random DNA sequence, remaining %lli\n", length);
      break_counter = break_at;
    }
  }
  if (linesize) printf("\n");
  return 0;
}
