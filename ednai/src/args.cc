/* Copyright (c) 2011, Martin Kopta <martin@kopta.eu>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include "./args.h"

namespace libednai {

args::args(int argc, char **argv):
  help(0),
  usage(0),
  version(0),
  action(""),
  algorithm(""),
  inputfile(""),
  outputdir(""),
  tmpdir(""),
  memory(0),
  time(0),
  preflen(0),
  maxstringsize(0),
  memforinbuffers(0),
  memforouttree(0),
  argc(argc),
  argv(argv) {
  args::read();
}

/* Read cmdline and set flags and fill data structures */
int args::read() {
  /* Processing of switches*/
  /* going thru commandline */
  char c;
  while (1) {
    static struct option long_options[] = {
      {"help", no_argument, &this->help, 1},
      {"usage", no_argument, &this->usage, 1},
      {"version", no_argument, &this->version, 1},
      {"action", required_argument, 0, 'a'},
      {"algorithm", required_argument, 0, 'A'},
      {"inputfile",  required_argument, 0, 'i'},
      {"memory",  required_argument, 0, 'm'},
      {"outputdir",  required_argument, 0, 'o'},
      {"tmpdir", required_argument, 0, 't'},
      {"time", no_argument, 0, 'T'},
      {"preflen", required_argument, 0, 'p'},
      {"maxstringsize", required_argument, 0, 'j'},
      {"memforinbuffers", required_argument, 0, 'k'},
      {"memforouttree", required_argument, 0, 'l'},
      {0, 0, 0, 0}
    };

    int option_index = 0;

    /* get next switch */
    c = (char) getopt_long(
        this->argc, this->argv,
        "huva:A:i:m:o:t:Tp:",
        long_options,
        &option_index);

    /* if we are at the end of switches */
    if (c == -1) break;

    /* evaluating of switch */
    switch (c) {
      case 0:
        if (long_options[option_index].flag != 0) {
          break;
        }
        cout << "option " << long_options[option_index].name;
        if ( optarg ) cout << " with arg " << optarg;
        cout << endl;
        break;
      case 'h':
        this->help = true;
        break;
      case 'u':
        this->usage = true;
        break;
      case 'v':
        this->version = 1;
        break;
      case 'a':
        this->action = optarg;
        break;
      case 'A':
        this->algorithm = optarg;
        break;
      case 'i':
        this->inputfile = optarg;
      case 'm':
        this->memory = atoi(optarg);
        break;
      case 'o':
        this->outputdir = optarg;
        break;
      case 't':
        this->tmpdir = optarg;
        break;
      case 'T':
        this->time = true;
        break;
      case 'p':
        this->preflen = atoi(optarg);
        break;
      case 'j':
        this->maxstringsize = atoi(optarg);
        break;
      case 'k':
        this->memforinbuffers = atoi(optarg);
        break;
      case 'l':
        this->memforouttree = atoi(optarg);
        break;
      case '?': /* `getopt_long' prints error. */
        return 1;
      default:
        PRINT_ERROR("Internal error while processing arguments");
        /* some of the allowed options arent catched by this switch */
        return 1;
    }
  }

  /*
  PRINT_DEBUG("Processing of command-line done.");
  PRINT_DEBUG("Action    : " << this->action);
  PRINT_DEBUG("Algorithm : " << this->algorithm);
  PRINT_DEBUG("Input     : " << this->inputfile);
  PRINT_DEBUG("Output dir: " << this->outputdir);
  PRINT_DEBUG("Tmp dir   : " << this->tmpdir);
  PRINT_DEBUG("Memory    : " << this->memory);
  */
  return 0;
}

int args::is_help_set() { return this->help == 1; }
int args::is_usage_set() { return this->usage == 1; }
int args::is_version_set() { return this->version == 1; }

int args::is_action_set() { return !this->action.empty(); }
int args::is_algorithm_set() { return !this->algorithm.empty(); }
int args::is_inputfile_set() { return !this->inputfile.empty(); }
int args::is_outputdir_set() { return !this->outputdir.empty(); }
int args::is_tmpdir_set() { return !this->tmpdir.empty(); }
int args::is_memory_set() { return this->memory != 0; }
int args::is_time_set() { return this->time == 1; }
int args::is_preflen_set() { return this->preflen != 0; }
int args::is_maxstringsize_set() { return this->maxstringsize != 0; }
int args::is_memforinbuffers_set() { return this->memforinbuffers != 0; }
int args::is_memforouttree_set() { return this->memforouttree != 0; }

string args::get_action() { return this->action; }
string args::get_algorithm() { return this->algorithm; }
string args::get_inputfile() { return this->inputfile; }
string args::get_outputdir() { return this->outputdir; }
string args::get_tmpdir() { return this->tmpdir; }
int args::get_memory() { return this->memory; }
int args::get_preflen() { return this->preflen; }
int args::get_maxstringsize() { return this->maxstringsize; }
int args::get_memforinbuffers() { return this->memforinbuffers; }
int args::get_memforouttree() { return this->memforouttree; }


} /* namespace libednai */
