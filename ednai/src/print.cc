/* Copyright (c) 2011, Martin Kopta <martin@kopta.eu>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include <iostream>
#include "./general.h"
#include "./libednai/libednai.h"

namespace libednai {

using std::cout;
using std::endl;

void print_version() {
  cout << "libednai v" << LIBEDNAI_VERSION << endl;
}

void print_usage_hint() {
  cout << "Use -u to show usage" << endl;
}

void print_usage() {
  cout << "Usage: ednai [args]" << endl;
  cout << "  -v                             show version" << endl;
  cout << "  -u                             show usage" << endl;
  cout << "  -h                             show help (same as -u)" << endl;
  cout << "  -a/--action           <action> action to do" << endl;
  cout << "  -A/--algorithm     <algorithm> algorithm to use" << endl;
  cout << "  -i/--inputfile          <file> FASTA input to index" << endl;
  cout << "  -o/--outputdir           <dir> output directory" << endl;
  cout << "  -t/--tmpdir              <dir> temp directory [" << DEFAULT_TMPDIR << "]" << endl;
  cout << "  -m/--memory (MB)         <num> available memory [" << DEFAULT_MEMORY << "]" << endl;
  cout << "  -T/--time                      measure time" << endl;
  cout << "  -p/--preflen             <num> prefix length (TRELLIS only) [" << DEFAULT_PREFLEN << "]" << endl;
  cout << "  -j/--maxstringsize       <num> maximum string size (B2ST only) [0.1 * memory]" << endl;
  cout << "  -k/--memforinbuffers (MB)<num> memory for input buffers (B2ST only) [0.7 * memory]" << endl;
  cout << "  -l/--memforouttree (MB)  <num> memory for output tree (B2ST only) [10]" << endl;
  cout << "Use 'help' in place of algorithm or action to list availible"
    << endl;
}

void print_available_actions() {
  cout << "Available action:" << endl;
  cout << "  index" << endl;
}

void print_available_algorithms() {
  cout << "Available algorithms:" << endl;
  cout << "  dummy" << endl;
  cout << "  trellis" << endl;
  cout << "  trellis+" << endl;
  cout << "  digest" << endl;
  cout << "  b2st" << endl;
}

} /* namespace libednai */
