/* Copyright (c) 2011, Martin Kopta <martin@kopta.eu>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include "./action.h"
#include "./print.h"
#include "./libednai/libednai.h"

namespace libednai {

int action_index(args *options, string tmpdir, int memory,
    int preflen, int maxstringsize, int memforinbuffers, int memforouttree) {

  string algorithm = options->get_algorithm();

  if (!options->is_inputfile_set()) {
    PRINT_ERROR("Input file not set!");
    print_usage_hint();
    return 1;
  }
  if (!options->is_outputdir_set()) {
    PRINT_ERROR("Output dir not set!");
    print_usage_hint();
    return 1;
  }

  /* routing */
  if (algorithm == "dummy") {
    return dummy_driver::create_index(
        options->get_inputfile(), options->get_outputdir(), tmpdir, memory);
  } else if (algorithm == "trellis") {
    return trellis_driver::create_index(
        options->get_inputfile(), options->get_outputdir(), tmpdir, memory, preflen);
  } else if (algorithm == "trellis+") {
    return trellis_plus_driver::create_index(
        options->get_inputfile(), options->get_outputdir(), tmpdir, memory);
  } else if (algorithm == "digest") {
    return digest_driver::create_index(
        options->get_inputfile(), options->get_outputdir(), tmpdir, memory);
  } else if (algorithm == "b2st") {
    return b2st_driver::create_index(
        options->get_inputfile(), options->get_outputdir(), tmpdir, memory,
        maxstringsize, memforinbuffers, memforouttree);
  } else {
    PRINT_ERROR("Unable to create index using algorithm '"
        << algorithm << "'");
    return 1;
  }

  return 0;
}

} /* namespace libednai */
