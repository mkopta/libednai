/* Copyright (c) 2011, Martin Kopta <martin@kopta.eu>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include "./print.h"
#include "./args.h"
#include "./action.h"
#include <cstdio>
#include <ctime>
#include <unistd.h>

int main(int argc, char **argv) {

  using namespace libednai;

  /* parse command line */
  args options(argc, argv);

  if (options.is_help_set() or options.is_usage_set()) {
    print_version();
    print_usage();
    return 0;
  } else if (options.is_version_set()) {
    print_version();
    return 0;
  }

  if (options.get_action() == "help") {
    print_available_actions();
    return 0;
  }

  if (options.get_algorithm() == "help") {
    print_available_algorithms();
    return 0;
  }

  if (!options.is_action_set()) {
    PRINT_ERROR("No action selected!");
    print_usage_hint();
    return 1;
  } else if (!options.is_algorithm_set()) {
    PRINT_ERROR("No algorithm selected!");
    print_usage_hint();
    return 1;
  }

  /* default memory? */
  int memory;
  if (!options.is_memory_set()) {
    memory = (int) DEFAULT_MEMORY;
  } else {
    memory = options.get_memory();
  }

  /* default tmpdir? */
  string tmpdir;
  if (!options.is_tmpdir_set()) {
    tmpdir = string(DEFAULT_TMPDIR);
  } else {
    tmpdir = options.get_tmpdir();
  }

  /* default preflen? */
  int preflen;
  if (!options.is_preflen_set()) {
    preflen = (int) DEFAULT_PREFLEN;
  } else {
    preflen = options.get_preflen();
  }

  /* default maxstringsize? */
  int maxstringsize;
  if (!options.is_maxstringsize_set()) {
    maxstringsize = (int) (memory * DEFAULT_MAXSTRINGSIZE_MULTIPLIER);
  } else {
    maxstringsize = options.get_maxstringsize();
  }

  /* default memforinbuffers? */
  int memforinbuffers;
  if (!options.is_memforinbuffers_set()) {
    memforinbuffers = (int) (memory * DEFAULT_MEMFORINBUFFERS_MULTIPLER);
  } else {
    memforinbuffers = options.get_memforinbuffers();
  }

  /* default memforouttree? */
  int memforouttree;
  if (!options.is_memforouttree_set()) {
    memforouttree = DEFAULT_MEMFOROUTTREE;
  } else {
    memforouttree = options.get_memforouttree();
  }

  int retval = 0;
  /* run wanted action with given params */
  clock_t start = clock();
  if (options.get_action() == "index") {
    retval = action_index(&options, tmpdir, memory, preflen, maxstringsize,
        memforinbuffers, memforouttree);
  } else {
    PRINT_ERROR("Unknown action '" << options.get_action() << "'");
    cout << "Usage '-a help' to get list of available actions" << endl;
    return 1;
  }
  clock_t stop = clock();

  if (options.is_time_set()) {
    printf("Time: %.4f seconds.\n", (((double) (stop - start)) / (double)CLOCKS_PER_SEC));
  }

  return retval;
}

