\chapter{Efektivní indexování DNA}

V předchozích kapitolách jsme prošli základy genetiky, vyhledávání v~textu
a~indexování. Nyní se tedy můžeme zaměřit konkrétně na indexování DNA. Jak jsme
se přesvědčili, klasické vyhledávací algoritmy se v~oblasti genomů příliš
neuplatní kvůli lineární hranici asymptotické složitosti vůči délce textu
(genomu). Tuto hranici boří indexování textu pomocí předzpracování do různých
datových struktur. Uvedeny byly nejzákladnější indexovací struktury -- sufix
tree, suffix array a~sufixový automat (DAWG). Kromě uvedených samozřejmě existuje
množství různých dalších indexovacích struktur jako například invertovaný index
\cite{inverted_index}, sufix kaktusy \cite{suffix_cactus}, sufixové binární
vyhledávací stromy \cite{suffix_binary_search_tree}, prefix index, string
B-tree \cite{string-b-tree}, {\em q}-gramy \cite{q-gram}, wavelet tree či FM-index
\cite{fm-index}.  Pro indexování biologických dat vznikly také specializované
indexovací techniky. Představení možných indexovacích technik biologických dat
(především DNA) bude v~této kapitole následováno vybráním
těch nejefektivnějších z~nich.

\section{Paměťové nároky naivní implementace}

Abychom se nepouštěli do zbytečné optimalizace, ověříme si nejprve
její potřebu. Aplikujeme teoreticky suffix tree, suffix array a~sufixový automat
(DAWG) na lidský genom. Jak bylo zmíněno v~předchozí kapitole, prvním problémem
indexování je paměť. Doba konstrukce indexovací struktury je pak problémem
druhým. Zaměřme se tedy na přesnější určení paměťové náročnosti suffix tree.
Suffix tree je datová indexovací struktura složená z~uzlů a~hran (respektive
jejich datových struktur). Místo reprezentace hran samostatnými datovými
strukturami je můžeme implementovat jako ukazatele (reference na jiné uzly) uvnitř
datových struktur reprezentující uzly. Víme,
že suffix tree
má stejně listů \footnote[1]{Platí v případě použití terminálního symbolu
rozšiřujícího původní text $T$, jako například `\$'. Bez použití terminálního
symbolu je počet listů $\leq n$.}, jako je sufixů textu $T$,
jichž je přesně $|T|$, tedy $n$.
Každý uzel zabírá sám o~sobě nějakou paměť. Velmi minimalistická struktura uzlu
obsahuje pouze sufix link (ukazatel na jiný uzel), pole nebo seznam ukazatelů
na potomky (o velikosti maximálně rovné aritě stromu, která je daná velikostí
abecedy) a~číslo udávající počátek sufixu v~textu $T$ (index). Abychom
mohli referovat ze suffix tree do textu $T$, musíme mít také $T$. Tedy celkem
se dá paměťová asymptotická složitost indexování textu pomocí suffix tree
základním neoptimalizovaným způsobem vyjádřit jako
$${\cal O}(n \cdot \log_2|\Sigma| + (\frac{|\Sigma| \cdot n - 1}{|\Sigma| - 1}) \cdot \alpha).$$
První polovina výrazu je paměťová náročnost
uložení textu $T$~o~délce $n$. Pomocí $\log_2|\Sigma|$, kde $\Sigma$ je abeceda
textu $T$ zjistíme nejmenší možný počet bitů pro uložení jednoho symbolu z~$T$.
Pro uložení textu $T$ délky $n$ tedy budeme potřebovat $n \cdot \log_2|\Sigma|$
bitů. Druhá polovina výrazu udává paměťovou náročnost uložení suffix tree.
Konstanta $\alpha$ udává velikost struktury jednoho uzlu v~bitech
a~$\frac{|\Sigma|\cdot n - 1}{|\Sigma| - 1}$ je počet všech uzlů úplného
$|\Sigma|$-árního suffix tree mající $n$ listů.
(úplný $k$-nární strom o~$n$ listech má $\frac{k \cdot n - 1}{k - 1}$ uzlů). $\alpha$ je
daná jako $(|\Sigma| + 1) \cdot \beta + \log_2n$. $|\Sigma|$ je počet ukazatelů
vedoucí na potomky ($|\Sigma|$-ární strom) plus jeden sufix link. $\beta$
udává velikost ukazatele na strukturu uzlu a~$\log_2n$ je minimální počet bitů
potřebný pro adresaci v~$T$ délky $n$. Dosadíme-li tedy za $\beta$
experimentálně ověřenou hodnotu 24 bitů a~za $n$ dosadíme $3,2 \cdot 10^9$ párů
nukleotických bází lidského genomu a~za $\Sigma$ dosadíme $\{a, c, g, t\}$,
vyjde nám nejmenší možná velikost indexovaného lidského genomu přibližně 60
GB.

Nyní se pokusíme určit paměťovou náročnost suffix array. Ta je jednodušší. Opět
je složena ze dvou částí -- referovaného textu $T$ a~samotné struktury. Pokud
budeme uvažovat velmi primitivní suffix array obsahující pouze pozice
sufixů v~$T$ seřazené podle abecedního pořadí získáme složitost
${\cal O}(\log_2|\Sigma| \cdot n + n \cdot \log_2n)$. První polovina udává paměťové
nároky uložení textu $T$ a~druhá polovina je paměťová náročnost uložení celkem
$n$ sufixů, které jsou uložené pouze jako pozice jejich začátků v~$T$
(indexy). Pro adresaci v~$T$ délky $n$ potřebujeme $\log_2n$ bitů, takže pro
adresy všech $n$ sufixů potřebujeme $n \cdot \log_2n $ bitů. Dosadíme-li opět za
$n$ a~$\Sigma$ jako v~předchozím případě, získáme paměťovou náročnost velmi
triviálního suffix array přibližně 12 GB. Při doplnění struktury o~LCP (longest
common prefix) a~další vhodné informace paměťová náročnost několikrát vzroste.

Nakonec určíme ještě paměťovou náročnost základní varianty sufixového automatu
(DAWG). Situaci si výrazně ulehčíme nereálnou podmínkou počtu stavů DAWG.
Nejmenší možný počet stavů DAWG je roven $n$ a~ten budeme uvažovat. DAWG musí
mít alespoň páteř tvořenou nejdelším sufixem (celým $T$), nicméně velikost
abecedy neumožňuje, aby bylo stavů právě $n$. Při velikosti abecedy $|\{a, c,
g, t\}|$ je nemožné, aby nedošlo k~nutnosti vytvoření nových stavů mimo páteř
kvůli zajištění determinističnosti DAWG. Paměťová náročnost DAWG může být
$\Omega(n \cdot \beta)$, kde $\beta$ je paměťová náročnost jednoho uzlu sufix
automatu. Uzel obsahuje ukazatele na následovníky a~ke každému z~těchto
ukazatelů také symbol, při kterém k~přechodu dojde. Díky ukládání symbolů přímo
do DAWG není potřeba původního textu $T$. Dosadíme-li v~předchozích odstavcích
použité hodnoty do uvedeného výrazu, dostaneme paměťovou náročnost přibližně 30
GB.

\subsection{Shrnutí}

Použití naivních implementací datových indexovacích struktur pro indexování
velmi velkých textů jako je například kompletní lidský genom klade příliš
vysoké paměťové nároky. Ze zkoumání paměťových složitostí plynou dva závěry.
Zaprvé, indexování velmi velkých textů již nelze provádět v~hlavní paměti
(uvažujeme-li počítačové zpracování). Kapacita dnešních operačních pamětí je
v~řádu jednotek gigabajtů u~osobních počítačů, desítek gigabajtů u~serverů
a~stovek až tisíců gigabajtů u~superpočítačů (clustery a~podobně). Takzvané
„in-memory“ indexování lze provádět jen pro malé vstupy. Pro vstupy (texty)
větší je potřeba použít principu „on-disk“, při kterém je indexovací struktura
trvale uložena na pevném disku. Princip „on-disk“ je nutno také použít
v~případě, že se do operační paměti nevejde ani samotný vstup. Uložení
indexovacích struktur a~vstupů na disk, tedy úložiště s~řádově horší
přístupovou dobou, si automaticky vynucuje použití postupů zohledňující skryté
paměti (cache) a~prostorovou lokalitu dat.

Druhým poznatkem je potřeba optimalizovat indexovací struktury a~dobu jejich
tvorby (počet I/O operací). Rozhodně je potřeba snížit paměťové nároky buď
zavedením komprese \cite{GroVit00} nebo efektivnější architekturou indexovacích
struktur a~zavedením efektivního postupu indexování.

\section{Suffix stromy na disku a~řešení I/O}

\subsection{TDD, 2004}
% vyuziti cache a prostorove lokality, complete Human Genome in 30 hours

Klasický Ukkonenův algoritmus konstrukce suffix tree \cite{Ukk95} má
asymptotickou složitost ${\cal O}(n)$, kde $n$ je velikost indexovaného textu. Tento
algoritmus má ale zásadní problém při velkých vstupech. Implicitně předpokládá,
že pro konstrukci bude k~dispozici dostatečné množství paměti. Spíše než
praktickým algoritmem je tak důkazem existence optimálního algoritmu konstrukce
suffix tree. V~roce 2004 se pokusili Sandeep Tata, Richard Hankins a~Jignesh
Patel vytvořit „disk-based“ techniku konstrukce suffix tree
\cite{TatHanPat04} zaměřující se na efektivní využití I/O operací a~skrytých
pamětí tak, aby bylo možné indexovat velmi velké vstupy. Kapacity dnešních
disků jsou běžně stovky gigabajtů až jednotky terabajtů u~osobních počítačů
a~desítky terabajtů u~středně velkých diskových polí serverů. To umožňuje
bezproblémové ukládání indexovacích struktur velkých textů. Dá se také
předpokládat, že každá smysluplná databáze biologických dat je uložena bezpečně
na pevném disku. Takzvaným úzkým hrdlem je však doba náhodného přístupu na
disk. Naivní implementace suffix tree při vytváření i~následném vyhledávání
generuje velké množství čtení původního textu na zcela nesouvisejících,
prakticky náhodných, adresách (pozicích).  Při efektivnějším využití diskových
vyrovnávacích pamětí a~skrytých pamětí v~hierarchii pamětí lze dosáhnout
signifikantně lepšího výkonu algoritmů.

TDD („Top Down Disk-based“) technika má asymptotickou složitost ${\cal O}(n^2)$,
nicméně díky efektivnímu využití paměťových operací prakticky překonává
asymptoticky lepší Ukkonenův algoritmus a~to dokonce i~v případě „in-memory“
konstrukce (bez odkládání struktur na disk). Ten totiž sice díky sufix linkům
(odkazům na kratší sufixy uvnitř stromu) lze provést v~${\cal O}(n)$, ale právě ony
sufix linky způsobují množství náhodných přístupů do již vytvořeného stromu
a~indexovaného textu a~tím algoritmus v~praxi velmi zpomalují. Před představením
TDD bylo vytvořeno několik algoritmů zeefektňující Ukkonenův algoritmus právě
lepším použitím paměti \cite{HunAtkIrv01, BedHar04}, buď odstraněním
sufix linků, lepším využitím vyrovnávacích pamětí, líné konstrukce (části
stromu jsou konstruovány při prvních požadavcích) či konstrukce podle prefixů.

TDD se skládá z~konstrukčního algoritmu PWOTD (Partition and Write Only Top
Down) a~správy vyrovnávacích pamětí. Samotný suffix tree je reprezentován
lineárním polem \cite{GieKurSto99}, kde jeden indexovaný symbol obsazuje
přibližně 8,5 bajtu.  Algoritmus PWOTD má dvě fáze. V~první rozdělí vstupní
text na oblasti podle prefixů a~v druhé fázi tyto oblasti vloží do suffix tree.
Rozdělení vstupního textu na oblasti umožňuje konstruovat nezávislé podstromy
v~paměti. Algoritmus PWOTD je založen na algoritmu {\em wotdeager}
\cite{GieKurSto99}. Základní trik TDD je záměna náhodného přístupu k~suffix
tree za náhodný přístup ke vstupnímu textu. Ukkonenův algoritmus tomu činí obráceně.
Vstupní text zpracovává sekvenčně a~k vytvářenému suffix tree přistupuje
náhodně. Tato záměna se může zdát zbytečná, ale po přihlédnutí k~poměru
paměťovým nárokům suffix tree a~vstupního textu je odůvodnění jasné. Suffix
tree je řádově paměťově náročnější než samotný vstupní text a~tedy náhodný
přístup k~textu není tak velkým omezením jako potřeba náhodného přístupu
k~budovanému suffix tree.

\begin{figure}[t]
\begin{center}
\includegraphics[width=70mm]{figures/tdd_vs_ukkonen_cache.pdf}
\caption{Doba výkonu TDD a Ukkonen a zobrazení cache-misses}
\label{tdd_vs_ukkonen_cache}
\end{center}
\end{figure}

Efektivní využití prostorové lokality TDD se uplatní nejen v~případě diskových
operací, ale i~v případě čistě „in-memory“ konstrukce, díky procesorovým
skrytým pamětím. TDD významně omezuje cache-misses (obrázek
\ref{tdd_vs_ukkonen_cache}, převzato z~\cite{TatHanPat04}) a~je tak schopen
překonat asymptoticky lepší Ukkonenův algoritmus. Jednou slabinou TDD je však
velikost abecedy. Pro malé abecedy TDD provádí výpočty podstromů, které
Ukkonenův algoritmus přeskakuje. I~pro DNA abecedu je však TDD přibližně
dvakrát až třikrát rychlejší (obrázek \ref{tdd_vs_ukkonen_cache}, {\em unif4}
a~{\em dmelano}), nicméně pro abecedy větší ($|A| = 40$) již TDD vykazuje
přibližně desetkrát nižší čas než Ukkonen. Další podstatnou nevýhodou TDD je
absence sufix linků, které některé algoritmy pro práci se sufix stromy
vyžadují \cite{PhoZak07}.

TDD byla první technika schopná indexovat kompletní lidský genom na
jednoprocesorovém počítači za méně než 30 hodin.

\subsection{dmsuffix}
% paralelní TDD, C++, pomale jako prase
Pokusů o~optimalizaci konstrukce suffix tree se od sedmdesátých let objevilo
mnoho a~některé z~nich byly založené na paralelním zpracování. Jeden z~takových
pokusů je algoritmus {\em dmsuffix} jež vychází z~TDD techniky. Autoři TDD ve
svém článku navrhli možnost paralelizace TDD, která je možná díky rozdělení
textu na nezávislé oddíly podle prefixů. Tohoto návrhu se ujali Arthur Dardia
a~Suzanne Matthews v~práci nazvané {\em Suffix Tree Construction} \cite{DarMatt}.
Jejich práce však skončila neúspěchem. Bez dostupné implementace TDD,
porovnáním se silně optimalizovaným Ukkonenovým algoritmem v~jazyce C, bez
kvalitní správy vyrovnávacích pamětí a~použitím jazyka C++ se autorům {\em
dmsuffix} nepodařilo naimplementovat dostatečně kvalitně na to, aby jej bylo
možné s~Ukkonenem a~TDD smysluplně porovnat.

\subsection{STTD64, 2007}
Halachev, Shiri a~Thamildurai publikovali v~roce 2007 \cite{HalShiTha07} způsob
uložení suffix tree nazvaný STTD64, přičemž se zaměřili nejenom na rozumnou
dobu konstrukce a~použitelné paměťové nároky, ale i~na složitost vyhledávání.
Vycházeli z~implementace suffix tree tak jak ji navrhl Giegerich, Kurtz a~Stoye
v roce 1999 \cite{GieKurSto99}, kterou označili jako STTD32 (Suffix Tree,
Top Down, 32 bitů). Ta v~nejhorším případě vyžaduje pro jeden symbol vstupního
textu dvanáct bajtů v~suffix tree, v~průměru však osm a~půl bajtu. STTD32 má
omezení na velikost vstupního textu jeden gigabajt. Uzly STTD32 mají 32 bitů,
přičemž 2~bity jsou použity na orientaci ve stromě. To nechává pouze 30 bitů na
adresaci do textu, který může být tedy maximálně jeden gigabajt velký. Toto
omezení obchází technika TDD tvorbou speciálních bitmap obsahující právě ty dva
kontrolní bity pro každý uzel. Tím uvolňuje plných 32 bitů pro každý uzel
stromu a~umožňuje adresaci až čtyř gigabajtů vstupního textu (velikost lidského
genomu je přibližně 3~GB a~méně při efektivnějším způsobu ukládání). Nicméně
zavedením dodatečných datových struktur (bitových map) se zvyšuje I/O vytížení
a tím se snižuje výkon algoritmu konstrukce a~vyhledávání. STTD64 řeší tento
gigabajtový problém zavedením 64 bitů místo 32 bitů. Uvolňuje tak 32 bitů pro
adresaci, 2~bity pro orientaci a~zbývajících 30 bitů pro adresaci prvního
potomka v~případě vnitřních uzlů stromu a~pro zaznamenávání hloubky v~případě
listu.

Použitím 64 bitů pro každý uzel suffix tree se znatelně zvyšuje paměťová
náročnost. Navíc pro naplnění listů informace o~jejich hloubce je potřeba
provést další průchod v~čase ${\cal O}(n)$. Uchovávání hloubky listů však zlepšuje
výkon vyhledávání, protože poté je čtení z~disku pro každou shodu sekvenční.
Nepoužitím speciálních bitmap jako v~případě TDD se výkon STTD64 zvyšuje.
Podle měření v~\cite{HalShiTha07} je STTD64 vhodné pro středně velké a~velké
vstupní texty (gigabajt a~více). STTD64 vykazuje velmi dobré výsledky při
vyhledávání, času indexování i~paměťové náročnosti a~dá se použít i~pro velmi
velké vstupy.

\subsection{TRELLIS, 2007}
% complete human genome in 4 hours with 2 GB
Trellis je disk-based indexovací algoritmus, který popsali v~roce 2007
Benjarath a~Zaki \cite{PhoZak07}. Je zaměřený především na indexování velkých
vstupů, konkrétně DNA. Asymptotická složitost konstrukce je ${\cal O}(n^2)$ a~paměťová
náročnost je ${\cal O}(n)$. Algoritmus zohledňuje charakteristiku DNA vstupu, jež má
nerovnoměrné rozložení. Při dělení podle prefixů tak jak tomu činí TDD se tak
snadno může stát, že některé podstromy prefixů můžou být nečekaně velké.
Trellis se skládá ze čtyř fází. V~první fázi sestavuje prefixy, aby se předešlo
nerovnoměrnému vytížení paměti při tvorbě podstromů. Ve druhé fázi vytváří
oddíly textu podobně jako TDD a~vytváří podstromy pomocí klasického in-memory
Ukkonenova lineárního algoritmu. Ve třetí fázi tyto podstromy spojuje
dohromady. Tato fáze má jako jediná asymptotickou složitost ${\cal O}(n^2)$.  Ve
čtvrté fázi se vypočítají sufix linky, přičemž tato fáze není povinná.

V porovnání Trellis s~TDD v~\cite{PhoZak07} vítězí jasně Trellis dobou
konstrukce, ale vykazuje lehce vyšší paměťové nároky což je způsobeno
kompresními technikami TDD. Podstatně rychlejší je Trellis i~při dotazování
právě díky sufix linkům.

V roce 2008 pak autoři TRELLIS publikovali vylepšenou verzi algoritmu nazvaný
TRELLIS+ \cite{PhoZak08}, který je postaven na stejných principech jako původní
verze, ale snižuje počet oddílů textu při tvorbě suffix tree, čímž snižuje
počet vytvořených podstromů, které se poté musí spojit. Protože fáze spojování
podstromů je asymptoticky nejhorší v~algoritmu TRELLIS, omezení počtu do tohoto
algoritmu vstupujících podstromů zvyšuje výkon celého algoritmu.

\subsection{DiGeST, 2008}
% vhodne pro velke vstupy, velice rychle
Algoritmus DiGeST (Disk-based Genomic Suffix Tree) navržený v~roce 2008 autory
Barsky, Stege, Thomo a~Upton \cite{BarSteThoUpt08} se zaměřuje na tvorbu suffix
tree pro velmi velké DNA sekvence. V~případě porovnávání více genomů je
praktické je indexovat do jednoho suffix tree (zobecněného suffix tree), čímž
se odhalí shodné a~unikátní části. Indexování více genomů najednou však
implikuje velikost vstupu přesahující čtyři gigabajty (záleží na typu uložení
genomu a~velikosti genomu v~párech nukleotických bází), což algoritmy jako TDD
či TRELLIS nezvládají (zčásti kvůli 32 bitovému adresování). Autoři DiGeST se
rozhodli nerozšiřovat TDD či TRELLIS o~možnost indexovat větší vstupy, protože
pro velmi velké vstupy by tyto algoritmy nemuseli být dostatečně výkonné.
Především proto, že vykonávají příliš mnoho náhodných I/O čtení. V~případě TDD
jsou to častá náhodná čtení vstupu a~případě TRELLIS jsou to náhodná čtení
suffix tree při dereferenci sufix linků.

Algoritmus DiGeST má tři fáze. V~první je vstup (vstupy) rozdělen na stejně
velké soubory o~velikosti $k$ a~symboly DNA abecedy jsou kódovány dvěma bity.
Ve druhé fázi jsou nad soubory vystavěny suffix array (tedy sufixy řetězců
v~souborech jsou seřazeny). Ve třetí fázi je na základě těchto suffix arrays
sestaven suffix tree postupným slučováním.

DiGeST díky rozdělení vstupu na menší soubory dokáže indexovat prakticky velmi
velké vstupy (přibližně jeden až osm gigabajtů). Doba běhu konstrukce je díky
způsobu využití I/O operací také velmi dobrá a~pro vstupy větší než jeden
gigabajt překonává TRELLIS+. DiGeST čte vždy sekvenčně a~co lze čte pouze
jednou jedinkrát a~stejně tak i~zapisuje. Dle měření v~\cite{BarSteThoUpt08}
DiGeST zvládne indexovat kompletní lidský genom za jeden a~půl hodiny.


\subsection{B$^2$ST, 2009}
% state of the art, obrovske vstupy velice rychle
Stejný tým, který publikoval algoritmus DiGeST, publikoval v~roce 2009 nový
algoritmus nazvaný $B^2ST$ \cite{BarSteThoUpt09}. Cílem bylo ještě snížit
náhodné přístupy do vstupního textu a~zobecnit algoritmus tak, aby byl schopen
indexovat vstupy, které se nevejdou do hlavní paměti. Základní myšlenkou
$B^2ST$ je opět stejně jako v~případě DiGeST budování suffix tree ze suffix
array doplněného o~LCP informace.

$B^2ST$ algoritmus má tři fáze. V~první rozdělí vstup na části, které lze
načíst do paměti. Ve druhé části seřadí sufixy v~každé části zvlášť a~ve třetí
fázi z~nich postupně postaví výsledný suffix tree. Asymptotická složitost
konstrukce je lineární.

Podle experimentálních výsledků v~\cite{BarSteThoUpt09} zvládá $B^2ST$
indexovat kompletní lidský genom během tří hodin. Velmi zajímavým výsledkem je
schopnost indexovat velmi velké vstupy. Během testování byl indexováno 12
gigabajtů reálných DNA sekvencí do jednoho suffix tree, což trvalo přibližně 25
hodin, přičemž dostupná operační paměť byla omezena na dva gigabajty. Výsledný
suffix tree uložený na disku zabíral přibližně 250 gigabajtů. Při použití čtyř
gigabajtů operační paměti a~jednoho terabajtu diskového prostoru je $B^2ST$
schopný indexovat přibližně 60 gigabajtů (!) vstupních DNA dat do jednoho
suffix tree.

\subsection{String B-tree}
Počet I/O lze omezit také využitím {\em string b-tree} \cite{string-b-tree},
který se zvláště hodí pro externí datové úložiště (disk). Jde o~kombinaci
klasického B-tree a~patricia tree (suffix tree).

\section{Řešení paměťové náročnosti indexovacích struktur}

Jak už jsme zjistili, indexování je velmi náročné na dostupnou paměť. V~případě
větších vstupů už nedostačuje operační paměť a~je nutno použít on-disk přístup.
S tím vzroste doba operací s~indexovanou strukturou (vytváření, vyhledávání)
kvůli nadměrnému a~náhodnému čtení z~pomalé externí paměti. Výše uvedené
algoritmy řeší tento problém efektivním řízením čtení a~zápisů s~ohledem na
skryté paměti a~celkovou hierarchii pamětí. Umožňují nám tak indexovat velké
vstupy v~rozumném čase. Nicméně, v~případě ještě větších vstupů nám brzy
přestane stačit i~externí paměť. Při indexování 12 GB dat pomocí
{\em state of the art} algoritmu $B^2ST$ bylo obsazeno výsledným sufix stromem
přibližně 250 GB. Velikost vstupu 12 GB odpovídá přibližně čtyřem kompletním
lidským genomům. Uvážíme-li potřebu indexovat genomy většího množství lidí,
například deseti miliónů, dostaneme se velice rychle mimo dostupnou kapacitu
pamětí.

V roce 1989 navrhl Jacobson \cite{succinct} datovou strukturu {\em succinct}.
Tato datová struktura je obecná a~lze ji použít pro ukládání vektorů, stromů,
grafů a~dalších jiných struktur. Succinct se vyznačuje paměťovou náročností,
která je velmi blízko teoretickému minimu potřebné paměti pro uložení dané
informace. Na rozdíl od kompresních algoritmů je však možné succinct strukturu
používat bez nutnosti dekomprese. V~pracích zabývající se komprimovanými
datovými strukturami se někdy succinct používá jako horní mez paměťové
náročnosti. Komprimovanou datovou strukturou se pak rozumí taková datová
struktura, která umožňuje provádět potřebné operace v~přibližně stejném čase
jako její nekomprimovaný ekvivalent, ale na rozdíl od něj zabírá signifikantně
méně paměti.

Připomeňme si paměťovou náročnost suffix array a~suffix tree.  Obě datové
indexovací struktury uchovávají celkem $n$ sufixů textu $T$ délky $n$. Pro
adresaci počátku sufixu v~$T$ je potřeba adresa o~minimální velikosti $\log_2n$
bitů. Celkem suffix tree i~suffix array uchovává $n$ sufixů pomocí adres
o~velikosti $\log_2n$ bitů,  tedy dohromady ${\cal O}(n \cdot \log_2n)$ bitů. Pro uchování
samotného textu však stačí pouze ${\cal O}(n \cdot \log_2|\Sigma|)$ bitů.  Indexovací
struktura je paměťově podstatně náročnější než samotný text, který se navíc dá
komprimovat jako $n \cdot H_k(T)$ bitů (kde $H_k(T)$ je empirická entropie
textu $T$ {\em k}-tého řádu).

Prolomení paměťové hranice indexu ${\cal O}(n \cdot \log_2n)$ rozebíral Kärkkäinen
\cite{Kar99}. Použil Lempel-Ziv kompresi k~vytvoření indexu s~paměťovou
náročností ${\cal O}(n \cdot H_k(T)) + o(n \cdot~\log_2|\Sigma|)$ bitů. Čas vyhledávání
se však změnil z~lineárního na kvadratický. Další práce se pak věnovaly
zrychlení vyhledávání.

\subsection{Compressed suffix array}
Komprimované pole přípon (CSA) představené v~roce 2000 Grossim a~Vitterem
\cite{GroVit00} je se svojí paměťovou náročností nejenom pod bariérou $n\cdot
\log_2n$, ale má (téměř) lineární čas vyhledávání. Použitým trikem je
vypočítávání některých informací z~okolních hodnot až v~případě potřeby.
Strukturu pak rozšířil Sadakane \cite{Sad00, Sad03} na {\em self-indexing} čímž
dále snížil paměťové nároky. Pomocí wavelet stromů odstraňuje CSA redundanci
v~sekvencí a~dosahuje tak kvalitní komprese.

\subsection{FM-index}

FM-index je {\em self-index} datová struktura využívající Burrows-Wheeler
transformace pro dosažení méně než lineární paměťové náročnosti a~přitom
umožňuje vyhledávání v~lineárním čase \cite{Lak08}. Navrhli jej v~roce 2000
Ferragina a~Manzini \cite{fm-index}.


\subsection{Kompromis}
Problém s~FM-index i~CSA je v~jejich potřebě náhodných čtení, což způsobuje
mnoho I/O operací, které v~případě uložení FM-indexu či CSA na disku významně
omezuje jejich použití \cite{HonShaVit10}. Takže si můžeme buď vybrat velmi
rychlou indexovací techniku jako například $B^2ST$ a~obětovat desítky bajtů za
každý indexovaný symbol, nebo budeme ukládat index do self-indexing
komprimované struktury, ale kvůli I/O operacím se vyhledávání řádově protáhne
v~čase. Kompromis nabízí Cánovas a~Navarro v~\cite{CanNav10}.

Alternativní řešení pak publikoval Mäkinen, Navarro, Sirén a~Välimäki
v~\cite{MakNavSirVal09}, kde je řešena otázka indexování řádově tisíců genomů.
Podle autorů je potřeba nejenom aplikovat I/O efektivní, komprimované
indexovací datové struktury, ale zaměřit se i~na fakt, že genomické sekvence
obsahují mnoho opakujících se vzorů, a~že genomy jsou si vždy velmi podobné. Ve
svojí práci navrhují novou rodinu self-indexing technik pro ukládání řetězců
s~opakujícími se vzory a~vhodný suffix tree.

\section{Efektivní indexování DNA}
Ideální technika pro indexování DNA by měla splňovat následující požadavky:
\begin{enumerate}
\item je schopná lineární konstrukce vůči délce vstupu,
\item je schopná lineárního vyhledávání,
\item pracuje s~indexem (a případně i~se vstupem) stylem on-disk,
\item efektivně využívá I/O operací,
\item používá self-indexing strukturu,
\item má sublineární paměťovou náročnost uložení indexu,
\item zvládá indexovat vstupy o~velikosti mnoha kompletních genomů (homo
sapiens),
\item je elegantní a~jednoduchá na implementaci.
\end{enumerate}
Žádná z~popsaných technik nesplňuje každý požadavek, dohromady však pokrývají
všechny. Nejblíže popsaným vlastnostem je technika TRELLIS (TRELLIS+), DiGeST
a~$B^2ST$. Ty splňují body 2, 3~a 4, technika $B^2ST$ navíc i~bod 7. Bod 1~je
diskutabilní, protože asymptoticky lineární konstrukce může být překonána
asymptoticky horším algoritmem, který je však lépe naimplementován (jak je
vidět na TDD).

Efektivní algoritmy využívající suffix tree zpravidla používají stejný vzor
práce. Nejprve rozdělí vstup na oddíly, které samostatně zpracují jako suffix
array. Z~nich pak sestaví les sufix stromů, které spojí dohromady.

Problémem, který je ještě potřeba řešit je spojení bodu 5~a 6~spolu s~bodem 
3 a~4. Dostupné techniky indexování se sublineární paměťovou náročnosti nejsou
momentálně schopné provádět efektivní přístup on-disk \cite{CanNav10}.
