\chapter{Knihovna}

V rámci této práce byla vypracována softwarová knihovna algoritmů pro efektivní
indexování DNA. Cílem bylo vybrat algoritmy vhodné pro efektivní indexování DNA
a~shrnout je do jedné kompaktní knihovny za účelem snadného porovnávání. V~této
kapitole je popsán návrh a~implementace této knihovny.

\section{Vybrané algoritmy}

Na základě poznatků z~předchozích kapitol bylo vybráno pár technik pro
efektivní indexování DNA. Jejich výběr byl dán kritériem schopnosti indexovat
on-disk, zpracovávat velké vstupy, rychlé konstrukce a~efektivního přístupu
k~pamětím. Preferované byly techniky využívající suffix tree, protože jde
o~strukturu obecně praktičtější. Vybrány byly algoritmy TRELLIS (a TRELLIS+),
DiGeST a~$B^2ST$. Všechny jsou navržené s~ohledem na efektivní indexování
rozsáhlých DNA řetězců.

\subsection{Vlastnosti vybraných algoritmů}

Algoritmus TRELLIS \cite{PhoZak07} naimplementoval v~roce 2007 Benjarath
Phoophakdee. Implementace je dostupná na webové adrese
\url{http://www.cs.rpi.edu/~zaki/www-new/pmwiki.php/Software/Software#bioseq},
spolu s~ostatními zajímavými programy. TRELLIS je napsaný v~jazyce C++, má
přibližně 4~kLOC (Lines of Code, řádků kódu) a~je uzpůsobený pro použití s~GNU
C++ kompilátorem a~GNU platformou. Algoritmus je složen ze čtyř fází. V~první
se rozdělí vstupní text na oddíly, ve druhé fázi se tyto oddíly samostatně
indexují do suffix trees, které se ve třetí fázi spojují. Čtvrtá fáze přidává
sufix linky pro rychlé vyhledávání a~je volitelná.

Algoritmus DiGeST \cite{BarSteThoUpt08} je dostupný na webové adrese
\url{http://webhome.cs.uvic.ca/~mgbarsky/digestpapercode.html}. Je napsaný
v~jazyce C, má přibližně 4,4 kLOC a~je opět uzpůsoben pro kompilaci GNU
kompilátorem a~použitím na GNU platformě. Obsahuje mimo jiné algoritmus
rychlého řazení sufixů od Jespera Larssona, publikovaný Larssonem a~Sadakane.
Cílem návrhu algoritmu DiGeST je schopnost indexovat více DNA sekvencí do
jednoho suffix tree, což implikuje schopnost indexovat dlouhé DNA sekvence.
Podstatnou vlastností je také malý počet I/O přístupů.

Algoritmus $B^2ST$ je dostupný na adrese
\url{http://webhome.cs.uvic.ca/~mgbarsky/bbstpapercode.html}. Je napsaný
v~jazyce C, má přibližně 4,2 kLOC a~z větší části vychází z~algoritmu DiGeST.
Zásadní vlastností $B^2ST$ je možnost indexovat vstupy větší, než je dostupná
paměť.

\section{Struktura knihovny}

Knihovna nazvaná {\em libednai} (\textbf{lib}rary for \textbf{e}fficient
\textbf{DNA} \textbf{i}ndexing) se skládá se z~několika komponent. Ty jsou
vyobrazeny pomocí UML diagramu komponent na obrázku \ref{komponenty}.

\begin{figure}[h]
\begin{center}
\includegraphics[scale=0.8]{figures/05_komponenty/komponenty.pdf}
\caption{Diagram komponent knihovny {\em libednai}}
\label{komponenty}
\end{center}
\end{figure}

Základem knihovny jsou originální implementace vybraných algoritmů, které jsou
částečně upravené. Tyto algoritmy (jejich funkce a~globální symboly) jsou
využívány odpovídajícími ovladači, které sdružují různorodé rozhraní různých
implementací pod společné rozhraní {\em driver}, které všechny ovladače
implementují. Tento přístup odpovídá návrhovému vzoru {\em fasáda} (někdy zvané
též {\em front controller}). Tato skupina ovladačů spolu s~originálními
algoritmy a~jednotnými rozhraními je knihovna {\em libednai}. Ke knihovně je
připojena komponenta {\em ednai}, poskytující CLI rozhraní umožňující přímou
práci s~knihovnou. Při použití CLI rozhraní je možné zvolit algoritmus a akci
pro daná vstupní data, což připomíná návrhový vzor {\em Strategy}.

Zapouzdření jednotlivých indexačních algoritmů a~jejich zpřístupnění přes
fasádu umožňuje jednoduché rozšiřování knihovny o~další algoritmy. Výhodou je
také jednoduchost návrhu.

\section{Implementace}

Výběr programovacího jazyka pro vytvoření knihovny byl značně omezen
originálními implementacemi indexačních algoritmů. Ty jsou psané v~jazyce C++
či jazyce C. Kompilátorem C++ se dá ve většině případů bez problémů kompilovat
zdrojový kód v~jazyce C, nikoliv však naopak. Volba tedy jasně padla na C++.
Předností C++ je mimo jiné objektového programování, datové struktury vyšší
abstrakce (než v~C) a~podpora jmenných prostorů. Dalo se také uvažovat
o~programovací jazycích jako například Perl, Lisp, Haskell či Python, ale přepis
původních algoritmů do jiného jazyka by byl pouze ztrátou času (a pravděpodobně
efektivity) a~případné propojování komponent psaných v~jiných jazycích by také
mohlo být problematické. Nejvhodnější alternativou by bylo použít jazyk Perl
pro tvorbu knihovny a~frontendu. Podobně tak tomu činí implementace TDD.
Nejkrásnější alternativou by bylo přepsat implementace do Haskellu a~využít
elegantní matematické syntaxe a~vysoce kvalitní automatické funkcionální
optimalizace.

Platforma knihovny je opět dána původními implementacemi. Ta je ve všech
případech shodná~--~GNU/Linux (či GNU kompatibilní). Knihovna je tedy psána
s~ohledem na GNU platformu, ale díky vysoce přenositelnému jazyku C++ není velký
problém přeportovat knihovnu na jinou platformu.

Knihovna je psána zčásti objektovým a~zčásti procedurálním stylem podle toho,
co se pro daný kus kódu zrovna více hodí. Knihovna byla tvořena velice pečlivě
s maximální možnou kvalitou kódu na všech abstraktních úrovních, kde kvalita
znamená čitelnost, srozumitelnost, přehlednost, efektivitu a~flexibilitu.

Knihovna je rozdělena do několika oddělených částí podobně jak je tomu na
diagramu \ref{komponenty}. Zdrojové kódy každé indexační metody jsou
kompilovány jako samostatné objektové soubory (object files), které jsou poté
spojeny v~jeden celek. Tento celek je pak připojen k~odpovídajícímu ovladači
a~všechny ovladače jsou poté opět spojeny do jednoho celku. Ten je nakonec
připojen k~vnějšímu rozhraní. Pro zajištění nekonfliktnosti jednotlivých částí
knihovny byly použity jmenné prostory (namespaces). Každý indexační algoritmus
se nachází ve vlastním jmenném prostoru {\verb libednai_xyz }, kde {\verb xyz }
je název algoritmu. Zbytek knihovny je pak umístěn ve jmenném prostoru
{\verb libednai }. Aby bylo možno zapouzdřit každý indexační algoritmus do jednoho
objektového souboru, bylo potřeba vyřešit konflikty globálních symbolů
a~vícenásobných definic funkcí. Pro vybrané algoritmy byl tento proces proveden
úspěšně, ale v~případě, že by tento proces vyžadoval příliš mnoho času, je
vhodné algoritmus rozdělit do více jmenných prostorů (například
{\verb libednai_xyz_filename }). Přístup do {\verb libednai_xyz } je potřeba povolit
pro ovladač odpovídajícího algoritmu.

\subsection{Úpravy originálních implementací}
Originální implementace algoritmů byly upraveny pro chod v rámci
knihovny. Jejich rozhraní bylo upraveno potřebným způsobem a některé části
kódu byly inovovány (například překladač FASTA formátu do interního NUM
formátu algoritmu TRELLIS). Originálním implementacím chybělo několik kroků k
úspěšné kompilaci, jako například ošetření návratových hodnot systémových
volání, doplnění potřebných importů hlavičkových souborů, korekce nevhodných
přetypování a některé další drobnosti. Navzdory veškeré snaze však originální
implementace převzaté přímo od výzkumných týmu nejsou plně funkční a při
nesmyslných či nesprávných vstupech zcela selhávají. Stabilita algoritmů se dá
částečně ovlivňovat pro každý jednotlivý případ nastavováním skrytých konstant
uvnitř kódu algoritmů. Knihovna samotná je plně funkční a během testování
nebyly zaznamenány žádné problémy.

\subsection{Testování knihovny}
Knihovna byly testována především velmi citlivým nastavením úrovně varování a
chyb kompilátoru. Použit byl také statický analyzátor kódu {\verb cppcheck }.
Samotná funkčnost rozhraní a knihovny byla mnohokráte testována ručním
způsobem vkládáním vstupů a~sledováním výstupů.

\subsection{Náhodný generátor sekvencí}
Součástí distribuce knihovny je náhodný generátor DNA sekvencí ve FASTA
formátu. Při generování lze nastavit délku požadované sekvence a je možno
rozdělit výstup do oddílů dané délky.

\subsection{Rozšiřitelnost}

Máme-li algoritmus v~jazyce C~či C++, který chceme přidat do knihovny libednai,
je potřeba provést celkem 5~(6) kroků.

\begin{description}
\item[Jmenný prostor] Uzavřeme kompletní kód algoritmu (ve všech zdrojových
souborech a~hlavičkových souborech) do jmenného prostoru
{\verb libednai_xyz }, kde {\verb xyz } je název algoritmu.
\item[Kompilace do objektových souborů] Každý zdrojový soubor zkompilujeme do
samostatného objektového souboru.
\item[Sestavení] Všechny připravené objektové soubory sestavíme linkerem do
jednoho výsledného objektového souboru. V~této fázi je potřeba odstranit
veškerý duplicitní kód a~vícenásobné definice globálních proměnných.
\item[Vytvoření ovladače] Pro daný algoritmus vytvoříme ovladač implementující
rozhraní {\verb driver } podobně jako ostatní ovladače v~knihovně. Zároveň bude
potřeba definovat rozhraní algoritmu.
\item[Připojení ovladače] Vytvořený ovladač linkerem připojíme do knihovny
{\verb libednai }.
\item[Úprava ednai]
Podle potřeby přidáme do frontendu {\verb ednai } do souboru
{\verb actions.cc } směrování na náš algoritmus, do {\verb print.cc } přidáme
náš algoritmus do listování a~případně pozměníme rozhraní {\verb ednai }
v~{\verb args.cc }.
\end{description}

\section{Vstupní formát}

DNA řetězce lze uchovávat různými způsoby. Základem je textová reprezentace
nukleotických bází jako samostatných symbolů. Tu definuje organizace
International Union of Pure and Applied Chemistry (IUPAC) a~je popsána
v~tabulce \ref{iupac}. Některým symbolům odpovídá více nukleotických bází.
Například N může být adenin, cytosin, guanin nebo thymin.

\begin{table}[h]
\begin{center}
\begin{tabular}{|c|l|}
\hline
Symbol & význam \\
\hline
A & adenin \\
C & cytosin \\
G & guanin \\
T & thymin \\
U & uracil \\
R & G A \\
Y & T C \\
K & G T \\
M & A C \\
S & G C \\
W & A T \\
B & G T C \\
D & G A T \\
H & A C T \\
V & G C A \\
N & A G C T \\
\hline
\end{tabular}
\caption{Kódy nukleotických bází dle IUPAC}
\label{iupac}
\end{center}
\end{table}

\begin{description}
\item[Plain] DNA řetězec můžeme uložit jako obyčejný textový soubor
v kódování ASCII s~využitím IUPAC kódování. Jde o~plýtvání místem, ale data
jsou lidsky čitelná a~snadno se s~nimi pracuje.
\item[FASTA] Uchovává řetězec podobně jako Plain, ale může jich uchovávat více
v jednom souboru. Na začátku každé sekvence je jeden řádek, který začíná
symbolem $>$ a~za ním následuje popis sekvence. FASTA formát má obyčejně
omezenou délku řádku na dohodnutou konstantu jako 120, 80 či 70 symbolů. To
usnadňuje implementaci programů čtoucích FASTA formát.
\item[GCG, GCG-RSF, GenBank] Velmi podobné formáty ukládající řetězec (řetězce)
určitým vzorem. Symboly jsou zapsané stejně jako v~případě Plain, ale jsou
rozdělené do skupin po deseti symbolech, po šesti skupinách na řádek oddělených
mezerou. Řádky předchází číslování počtu symbolů. Formáty připomínají
hexadecimální výpisy. Na začátku souboru (sekvence) je textová hlavička blíže
popisující další obsah.
\item[IG] Uchovává sekvence stejně jako Plain. Může obsahovat komentáře jako řádky
uvozené středníkem. Před samotnou sekvencí je název sekvence (bez mezer) a~na
konci sekvence je číslo (1) ukončující sekvenci.
\item[Genomatix] Podobný formát jako GenBank, ale s~velmi komplexní hlavičkou.
\end{description}

Kromě formátů založených na ASCII kódování IUPAC kódu lze využít různé binární
reprezentace. Nejběžnější je uložit DNA symboly a, c, g, t~jako dvojice bitů
a~ty poté seskupit do větších celků. Máme-li 32 bitů velký datový objekt pro
ukládání čísel (integer), můžeme do něj uložit celkem šestnáct DNA symbolů.
Toho například využívá TDD či $B^2ST$ algoritmus.

Běžně používaným formátem je FASTA formát a~přijímá jej většina algoritmů. Do
knihovny zahrnuté algoritmy také používají FASTA formát (i přesto, že z~něj
konvertují do interní podoby). Knihovna tedy také používá FASTA formát.

\begin{figure}[h]
\begin{verbatim}
           >chr1
           taaccctaaccctaaccctaaccctaaccctaaccctaaccctaacccta
           accctaaccctaaccctaaccctaaccctaaccctaaccctaaccctaac
           cctaacccaaccctaaccctaaccctaaccctaaccctaaccctaacccc
           taaccctaaccctaaccctaaccctaacctaaccctaaccctaaccctaa
           ccctaaccctaaccctaaccctaaccctaacccctaaccctaaccctaaa
           ccctaaaccctaaccctaaccctaaccctaaccctaaccccaaccccaac
           cccaaccccaaccccaaccccaaccctaacccctaaccctaaccctaacc
           >chr2
           CGTATcccacacaccacacccacacaccacacccacacacacccacaccc
           acacccacacacaccacacccacacaccacacccacacccacacaccaca
           cccacaccacacccacacaccacacaccacacccacacccacacacacca
           cacccacacaccacacccacacacaccctaaccctaacccctaaccccta
           accctaaccctacccgaaccctaaccctaaccctaacccctaaccctaac
           ccctaaccctaaccctaaccgtaaccctaaccctttaccctaacccgaac
           ccctaacccctaacccctaacccttaaccctaacccttaaccctgaccct
           gaccctgaccgtgaccctgaccctaacccgaacccgaacccgaaccccga
           accccgaaccccgaaccccaaccccaaccccaaccccaaccctaacccct
\end{verbatim}
\caption{Ukázka FASTA formátu}
\label{fasta_example}
\end{figure}
